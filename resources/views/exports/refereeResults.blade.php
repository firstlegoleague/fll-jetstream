<table>
    <thead>
    <tr>
        <th>Teamnumber</th>
        <th>Teamname</th>
        <th>Max score</th>
        @foreach($rounds as $round)
            <th>{{$round->name}}</th>
        @endforeach
        @foreach($rounds as $round)
            <th>GP {{$round->name}}</th>
        @endforeach
    </tr>
    </thead>

    <tbody>
        @foreach($teams as $team)
        <tr>
            <td>{{$team->id}}</td>
            <td>{{$team->name}}</td>
            <td>{{$team->getMaxScore()}}</td>
            @foreach($rounds as $round)
                <td>{{ $team->getScoreByRound($round) }}</td>
            @endforeach
            @foreach($rounds as $round)
                <td>{{ $team->getGPByRound($round) }}</td>
            @endforeach
        </tr>
        @endforeach
    </tbody>


</table>
