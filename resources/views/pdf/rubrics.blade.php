<!DOCTYPE html>
<html lang="nl">
<head>
    {{--    <meta charset="utf-8">--}}

    <title>Rubrics OMR</title>

    <style>
        .page-break {
            page-break-after: always;
        }

        @page { margin: 0; }

        body {
            font-family: Helvetica, serif;
        }
    </style>
</head>
<body>
@foreach($teams as $team)
    <div style="width: 100%; height: 100%; position: relative;">
        <img src="https://cdn.flltools.com/judgeForms/nl/2025/innovatie.png" alt="Rubrics" style="height: 100%; width: 100%; position: absolute; top:0;left:0;"/>
        <div style="position: absolute; top: 160px; right: 350px;">
            <div style="position: relative;">
                <img src="{{'data:image/png;base64,' . $team['barcodes']['innovation']}}" style="position: absolute; top: 0; right: 0; height: 70px;">
                <span style="position: absolute; top:85px; right:0; width: 100%; text-align: center; font-family: 'Times New Roman',serif">{{ $team['barcodes']['innovation_str'] }}</span>
            </div>
        </div>
        {{--        <div style="position: absolute; top: 140px; left:350px; width: 100%;">--}}
        {{--            <div style="position: relative; color:red;">--}}
        {{--                <span style="position: absolute; top: 0; left: 0; width: 100%;"><b>Waarschuwing!</b> Niet vouwen of nieten</span>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        @if ($team['team_id'] != '*')
            <span style="position: absolute; top:500px; left:750px;">{{ $team['team_id'] }}</span>
        @endif
        @if ($team['team_name'])
            <span style="position: absolute; top:500px; left:1100px;">{{ $team['team_name'] }}</span>
        @endif
    </div>
    <div class="page-break"></div>
    <div style="width: 100%; height: 100%; position: relative;">
        <img src="https://cdn.flltools.com/judgeForms/nl/2025/robotontwerp.png" alt="Rubrics" style="height: 100%; width: 100%; position: absolute; top:0;left:0;"/>
        <div style="position: absolute; top: 160px; right: 350px;">
            <div style="position: relative;">
                <img src="{{'data:image/png;base64,' . $team['barcodes']['robot']}}" style="position: absolute; top: 0; right: 0; height: 70px;">
                <span style="position: absolute; top:85px; right:0; width: 100%; text-align: center; font-family: 'Times New Roman',serif">{{ $team['barcodes']['robot_str'] }}</span>
            </div>
        </div>
        {{--        <div style="position: absolute; top: 140px; left:350px; width: 100%;">--}}
        {{--            <div style="position: relative; color:red;">--}}
        {{--                <span style="position: absolute; top: 0; left: 0; width: 100%;"><b>Waarschuwing!</b> Niet vouwen of nieten</span>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        @if ($team['team_id'] != '*')
            <span style="position: absolute; top:500px; left:750px;">{{ $team['team_id'] }}</span>
        @endif
        @if ($team['team_name'])
            <span style="position: absolute; top:500px; left:1100px;">{{ $team['team_name'] }}</span>
        @endif
    </div>
    <div class="page-break"></div>
    <div style="width: 100%; height: 100%; position: relative;">
        <img src="https://cdn.flltools.com/judgeForms/nl/2025/feedback.png" alt="Rubrics" style="height: 100%; width: 100%; position: absolute; top:0;left:0;"/>
        <div style="position: absolute; top: 160px; right: 350px;">
            <div style="position: relative;">
                <img src="{{'data:image/png;base64,' . $team['barcodes']['cv']}}" style="position: absolute; top: 0; right: 0; height: 70px;">
                <span style="position: absolute; top:85px; right:0; width: 100%; text-align: center; font-family: 'Times New Roman',serif">{{ $team['barcodes']['cv_str'] }}</span>
            </div>
        </div>
        {{--        <div style="position: absolute; top: 140px; left:350px; width: 100%;">--}}
        {{--            <div style="position: relative; color:red;">--}}
        {{--                <span style="position: absolute; top: 0; left: 0; width: 100%;"><b>Waarschuwing!</b> Niet vouwen of nieten</span>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        @if ($team['team_id'] != '*')
            <span style="position: absolute; top:500px; left:750px;">{{ $team['team_id'] }}</span>
        @endif
        @if ($team['team_name'])
            <span style="position: absolute; top:500px; left:1100px;">{{ $team['team_name'] }}</span>
        @endif
    </div>
    <div class="page-break"></div>
    <div style="width: 100%; height: 100%; position: relative;">
        <img src="https://cdn.flltools.com/judgeForms/nl/2025/juniorjudge.png" alt="Rubrics" style="height: 100%; width: 100%; position: absolute; top:0;left:0;"/>
    </div>
    <div class="page-break"></div>
@endforeach
<div style="width: 100%; height: 100%; position: relative;">
    <div style="position: absolute; top: 800px; left:0; text-align: center; width: 100%;">
        <div style="position: relative;">
            <span>This is the last page</span><br>
            <span>Total of {{ count($teams) - 1 }} teams.</span><br>
            <span>Generated at {{  \Carbon\Carbon::now()->toDateTimeLocalString() }}</span>
        </div>
    </div>
</div>
</body>
</html>
