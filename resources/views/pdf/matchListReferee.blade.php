<!DOCTYPE html>
<html lang="nl">
<head>
    {{--<meta charset="utf-8">--}}

    <title>Judge Sheet</title>

    <style>
        .page-break {
            page-break-after: always;
        }

        @page { margin: 20; }

        body {
            font-family: Helvetica, serif;
        }
    </style>
</head>

{{--<div class="page-break"></div>--}}

<body>
<h1>Match overview</h1>
<div class="page-break"></div>

    <table>
    @foreach($data[1] as $session)
    <tr>
            <td>
                {{$session["start_time"]}}
            </td>
            @foreach($data as $test)
                <td>
                    {{$test[$session["loop"]]["team_id_name"]}}
                </td>
            @endforeach
    <td>
    @endforeach
    </table>


    <div class="page-break"></div>

    <div style="width: 100%; height: 100%; position: relative;">
        <div style="position: absolute; top: 800px; left:0; text-align: center; width: 100%;">
            <div style="position: relative;">
                <span>This is the last page</span><br>
                <span>Total of {{ count($data) }} Judge lanes.</span><br>
                <span>Generated at {{  \Carbon\Carbon::now()->toDateTimeLocalString() }}</span>
            </div>
        </div>
    </div>

</body>
</html>
