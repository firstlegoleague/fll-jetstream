<!DOCTYPE html>
<html lang="nl">
<head>
    {{--    <meta charset="utf-8">--}}

    <title>Teams4Teams</title>

    <style>
        .page-break {
            page-break-after: always;
        }

        @page { margin: 0; }

        body {
            font-family: Helvetica, serif;
        }
    </style>
</head>
<body>
@foreach($teams as $team)
    <div style="width: 100%; height: 100%; position: relative;">
        <img src="https://cdn.flltools.com/exportTemplate/Teams4Teams.png" alt="Rubrics" style="height: 100%; width: 100%; position: absolute; top:0;left:0;"/>

            <span style="font-size: 90px; position: absolute; top:350px; left:250px;">{{ $team['team_id'] }}</span>

            <span style="font-size: 90px; position: absolute; top:350px; left:500px;">{{ $team['team_name'] }}</span>

    </div>
    <div class="page-break"></div>

@endforeach
<div style="width: 100%; height: 100%; position: relative;">
    <div style="position: absolute; top: 800px; left:0; text-align: center; width: 100%;">
        <div style="position: relative;">
            <span>This is the last page</span><br>
            <span>Total of {{ count($teams) }} teams.</span><br>
            <span>Generated at {{  \Carbon\Carbon::now()->toDateTimeLocalString() }}</span>
        </div>
    </div>
</div>
</body>
</html>
