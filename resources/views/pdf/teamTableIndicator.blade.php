<!DOCTYPE html>
<html lang="nl">
<head>
    {{--    <meta charset="utf-8">--}}

    <title>Team Table Indicator</title>

    <style>
        .page-break {
            page-break-after: always;
        }

        @page { margin: 0; size: A4 landscape;}

        body {
            font-family: Helvetica, serif;
        }
    </style>
</head>
<body>
@foreach($teams as $team)
    <div style="width: 100%; height: 100%; position: relative;">
        <img src="https://cdn.flltools.com/exportTemplate/teamTableIndicator.png" alt="Team Table Indicator" style="height: 100%; width: 100%; position: absolute; top:0;left:0;"/>

        <span style="position: absolute; top:300px; left:2000px;"><div style="font-size: 5em">{{ $team['team_id'] }}</div></span>

        <span style="position: absolute; top:900px; left:300px;"><div style="font-size: 6em">{{ $team['team_name'] }}</div></span>

        <span style="position: absolute; top:1800px; left:250px;"><div style="font-size: 4em">{{ $team['team_affiliate'] }}</div></span>

    </div>
    <div class="page-break"></div>
@endforeach

<div style="width: 100%; height: 100%; position: relative;">
    <div style="position: absolute; top: 800px; left:0; text-align: center; width: 100%;">
        <div style="position: relative;">
            <span>This is the last page</span><br>
            <span>Total of {{ count($teams)}} teams.</span><br>
            <span>Generated at {{  \Carbon\Carbon::now()->toDateTimeLocalString() }}</span>
        </div>
    </div>
</div>
</body>
</html>
