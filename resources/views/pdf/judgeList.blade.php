<!DOCTYPE html>
<html lang="nl">
<head>
    {{--<meta charset="utf-8">--}}

    <title>Judge Sheet</title>

    <style>
        .page-break {
            page-break-after: always;
        }

        @page { margin: 20; }

        body {
            font-family: Helvetica, serif;
        }
    </style>
</head>

{{--<div class="page-break"></div>--}}

<body>
<h1>Judge overview</h1>
<div class="page-break"></div>
@foreach ($jury as $lane)
    <h1>{{$lane[0]["jury_name"]}}</h1><br>
    <table>
    @foreach($lane as $session)
        <tr>
            <td>{{$session["start_time"]}}</td>
            <td style="margin-left: 200px; margin-right: 200px"></td>
            <td style="margin-left: 200px; margin-right: 200px"></td>
            <td style="margin-left: 200px; margin-right: 200px"></td>
            <td style="margin-left: 200px; margin-right: 200px"></td>
            <td>{{$session["team_id_name"]}}</td>
        </tr>
    @endforeach
    </table>


    <div class="page-break"></div>
@endforeach

    <div style="width: 100%; height: 100%; position: relative;">
        <div style="position: absolute; top: 800px; left:0; text-align: center; width: 100%;">
            <div style="position: relative;">
                <span>This is the last page</span><br>
                <span>Total of {{ count($jury) }} Judge lanes.</span><br>
                <span>Generated at {{  \Carbon\Carbon::now()->toDateTimeLocalString() }}</span>
            </div>
        </div>
    </div>

</body>
</html>
