export default {
    email: 'E-mail',
    password: 'Wachtwoord',
    forgotPassword: 'Wachtwoord vergeten?',
    login: 'Inloggen',
    rememberMe: 'Onthoud mij'
};