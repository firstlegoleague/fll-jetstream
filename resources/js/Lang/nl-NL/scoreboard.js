export default {
    title: 'Scorebord',
    header: 'Scorebord',
    place: 'Plaats',
    round: 'Ronde',
    score: 'Score',
    admin: { header: 'Administrative Scoreboard' }
};