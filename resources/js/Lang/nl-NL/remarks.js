export default {
    title: 'Opmerkingen',
    madeBy: 'Gemaakt door',
    category: 'Categorie',
    remark: 'Opmerking',
    aboutTeam: 'Opmerking over:',
    value: 'De opmerking',
    coach: 'Coach',
    jury: 'Jury',
    table: 'Tafel',
    team: 'Team',
    parents: 'Ouder',
    others: 'Overige'
};