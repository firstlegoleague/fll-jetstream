import generalMsg from "./scoresheet_general"
import masterpieceMsg from "./masterpiece"
import submergedMsg from "./submerged";

export default {
    general: generalMsg,
    submerged: submergedMsg,
    masterpiece: masterpieceMsg,
};
