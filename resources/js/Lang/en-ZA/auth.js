export default {
    email: 'Email',
    password: 'Password',
    forgotPassword: 'Forgot your password?',
    login: 'Login',
    rememberMe: 'Remember Me'
};