export default {
    title: "Exports",
    rubrics: "Judge Rubrics",
    rubricsExplain: "This export contains all the rubrics sheets, with special marks for digital processing. This uses the Teams as they are in the FLLTools system.",
    ojs: "Offical Judging Sheet Export",
    ojsExplain: "Creates various export files for OJS from the FLLTools website.",
    warningTime: "It can take a few minutes to generate this export"
};
