export default {
    itemsPerPageText: 'Items per pagina:',
    itemsPerPageAll: 'Alles',
    pageText: '{0}-{1} van {2}',
    loadingText: 'Data ophalen...',
    nextPage: 'Volgende pagina',
    lastPage: 'Laatste pagina',
    prevPage: 'Vorige pagina',
    firstPage: 'Eerste pagina'
};