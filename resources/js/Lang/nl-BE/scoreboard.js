export default {
    title: 'Scoreboard',
    header: 'Scoreboard',
    place: 'Rank',
    round: 'Ronde',
    score: 'Score',
    admin: { header: 'Administratief Scorebord' }
};