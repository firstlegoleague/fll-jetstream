export default {
    title: 'Timer',
    start: 'Start',
    stop: 'Stop',
    reset: 'Reset'
};