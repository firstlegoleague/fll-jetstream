export default {
    title: "Teams",
    number: "Nummer",
    name: "Naam",
    numberLong: "Teamnummer",
    nameLong: "Teamnaam",
    id: "ID",
    affiliate: "Organisatie",
    actions: "Acties",
    titleModalAdd: "Team toevoegen",
    creationSuccess: "Team is succesvol aangemaakt!",
    listOfTeams: "Lijst met alle teams"
};
