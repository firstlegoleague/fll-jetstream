import generalMsg from "./scoresheet_general"
import masterpieceMsg from "./masterpiece"


export default {
    general: generalMsg,
    masterpiece: masterpieceMsg,

};
