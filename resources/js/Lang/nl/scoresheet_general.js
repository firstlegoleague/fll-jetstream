export default {
    yes: 'Ja',
    no: 'Nee',
    blue: 'Blauw',
    pink: 'Roze',
    orange: 'Oranje',
    lightBlue: 'Licht blauw',
    darkBlue: 'Donkerblauw',
    yellow: 'Geel',
    green: 'Groen',
    gp_long_name: 'Precisietokens',
    gp_desc: 'Aantal resterende precisietokens:'
};