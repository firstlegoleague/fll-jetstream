export default {
    email: "Email",
    password: "Wachtwoord",
    forgotPassword: "Wachtwoord vergeten?",
    login: "Inloggen",
    rememberMe: "Onthoud mij",
};