export default {
    title: "General",
    add: "Toevoegen",
    close: "Sluiten",
    save: "Opslaan",
    edit: "Aanpassen",
    show: "Bekijken",
    delete: "Verwijderen",
    actions: "Acties",
    active: "Actief",
    search: "Zoeken",
    update: "Updaten",
    language: "Taal",
};
