import dataFooterMsg from "./dataFooter.js";

export default {
    noDataText: "Geen resultaten.",
    dataFooter: dataFooterMsg,
    input: {
        clear: "Legen",
    },
};
