export default {
    title: "Rondes",
    name: "Ronde naam",
    is_practice: "Oefenronde",
    score_hidden: "Verborgen",
    titleModalAdd: "Ronde aanmaken",
    titleModalShow: "Ronde bekijken",
    titleModalEdit: "Ronde aanpassen",
    game_round: "Wedstrijdronde",
    score_visible: "Zichtbaar",
};
