import dataFooterMsg from './dataFooter.js';
export default {
    noDataText: 'Aucun enregistrement.',
    dataFooter: dataFooterMsg,
    input: { clear: 'Nettoyer' },
    open: 'Ouvert'
};