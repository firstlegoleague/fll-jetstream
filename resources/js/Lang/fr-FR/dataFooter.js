export default {
    itemsPerPageText: 'Éléments par page :',
    itemsPerPageAll: 'Tous',
    pageText: '{0}-{1} de {2}',
    loadingText: 'Récupération des données...',
    nextPage: 'Page suivante',
    lastPage: 'Dernière page',
    prevPage: 'Page précédente',
    firstPage: '1ère page'
};