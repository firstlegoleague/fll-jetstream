export default {
    title: 'Exportations',
    rubrics: 'Grilles d\'évaluation',
    rubricsExplain: 'Cet export contient toutes les grilles d\'évaluation avec des marques spéciales pour le traitement numérique. Il utilise les équipes comme elles le sont dans le système FLLTools.',
    ojs: 'Exportation de la fiche de grilles d\'évaluation',
    ojsExplain: 'Crée divers fichiers d\'exportation pour OJS à partir du site Web de FLLTools.',
    warningTime: 'La génération de cette exportation peut prendre quelques minutes'
};