export default {
    title: 'Minuterie',
    start: 'Start',
    stop: 'Stop',
    reset: 'Reset'
};