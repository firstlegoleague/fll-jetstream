export default {
    email: 'E-mail',
    password: 'Mot de passe',
    forgotPassword: 'Mot de passe oublié ?',
    login: 'Connexion',
    rememberMe: 'Mémoriser mon login'
};