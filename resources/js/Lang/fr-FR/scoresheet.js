import generalMsg from './scoresheet_general';
import masterpieceMsg from './scoresheet_general';
import submergedMsg from './submerged';
export default {
    general: generalMsg,
    submerged: submergedMsg,
    masterpiece: masterpieceMsg
};