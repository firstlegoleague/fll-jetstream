export default {
    title: 'Tables',
    number: 'Numéro de table',
    color: 'Couleur ',
    color_code: 'Code de couleur',
    titleModalAdd: 'Créer une paire de tables',
    titleModalShow: 'Voir la table',
    titleModalEdit: 'Modifier la table',
    pair: 'Paire de tables'
};