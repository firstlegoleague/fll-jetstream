export default {
    title: 'Tableau des résultats',
    header: 'Tableau des résultats',
    place: 'Classement',
    round: 'Manche',
    score: 'Score',
    admin: { header: 'Administrative Scoreboard' }
};