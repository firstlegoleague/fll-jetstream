<script setup>
import {
    computed,
    defineProps,
    nextTick,
    onBeforeMount,
    onBeforeUnmount,
    onMounted,
    ref,
    watch,
} from "vue";
import MissionInput from "./Mission/MissionInput.vue";
import { sortByKey } from "../../utility/sorting.js";
import { v4 as uuidv4 } from "uuid";

import { BooleanExpressionEvaluator } from "../../../../app/Utils/ScoreSheet/ConditionalExpression/resources/evaluator.js";

const props = defineProps({
    scoreSheet: {
        required: true,
        type: Object,
    },
    imageEnabled: {
        required: false,
        type: Boolean,
        default: true,
    },
    disabled: {
        required: false,
        default: false,
    },
    initialResult: {
        required: false,
        default: null,
        type: Object,
    },
});

const emit = defineEmits(["update:result"]);

defineExpose({ createEmptyResult, getResult });

const sortedMissions = computed(() => {
    if (props.scoreSheet?.missions) {
        return sortByKey(props.scoreSheet.missions, "priority");
    } else {
        return [];
    }
});

/**
 * Whether initial results are enabled for this score sheet.
 * @type {ComputedRef<*>}
 */
const initialResultsEnabled = computed(() => {
    return props.scoreSheet.initial_results_enabled;
});

/**
 * Get the initial result for an objective.
 * @param objective An objective of a scoresheet.
 * @returns {{valid: boolean, result: *, points: number}|{valid: boolean, result: null, points: number}|{valid: boolean, result, points}}
 */
function getInitialResult(objective) {
    const none = {
        valid: false,
        points: 0,
        result: null,
    };
    if (!initialResultsEnabled.value) {
        return none;
    }
    if (
        objective.type ===
            "App\\Models\\Challenge\\Mission\\Objective\\RadioObjective" &&
        objective.initial_option_uuid
    ) {
        const option = objective.radio_objective_options.find(
            (option) => option.uuid === objective.initial_option_uuid
        );
        return {
            valid: true,
            points: option.points,
            result: option.uuid,
        };
    } else if (
        objective.type ===
            "App\\Models\\Challenge\\Mission\\Objective\\SpinnerObjective" &&
        !isNaN(objective.initial_value)
    ) {
        // TODO We don't take into consideration when min > 0
        return {
            valid: true,
            points: (objective.initial_value ?? 0) * objective.points,
            result: objective.initial_value ?? 0,
        };
    }
    return none;
}

const result = ref({});

function getResult() {
    return result.value;
}

/**
 * Create a new empty result and perform revalidation on it.
 */
function createEmptyResult() {
    result.value = {
        uuid: uuidv4(),
        score_sheet_uuid: props.scoreSheet.uuid,
        points: 0,
        valid: false,
        mission_results: {},
    };

    sortedMissions.value.forEach((mission) => {
        result.value.mission_results[mission.uuid] = {
            mission_uuid: mission.uuid,
            points: 0,
            valid: false,
            mission_objective_results: {},
        };

        mission.objectives.forEach((objective) => {
            const initialResult = getInitialResult(objective);

            result.value.mission_results[
                mission.uuid
            ].mission_objective_results[objective.uuid] = {
                points: initialResult.points,
                valid: initialResult.valid,
                mission_objective_id: objective.uuid,
                mission_objective_type: objective.type,
                result: initialResult.result,
                available: true,
                available_options: objective?.radio_objective_options?.map(
                    (option) => option.uuid
                ),
            };
        });
    });

    resultUpdated();
}

/**
 * Called when an event comes in that updates the value of an objective result.
 * @param resultUpdate The update event.
 */
function handleObjectiveResultUpdate(resultUpdate) {
    result.value.mission_results[
        resultUpdate.mission_uuid
    ].mission_objective_results[resultUpdate.objective_uuid].result =
        resultUpdate.result;
    result.value.mission_results[
        resultUpdate.mission_uuid
    ].mission_objective_results[resultUpdate.objective_uuid].points =
        resultUpdate.points;

    result.value.mission_results[
        resultUpdate.mission_uuid
    ].mission_objective_results[resultUpdate.objective_uuid].valid =
        resultUpdate.result !== null;
    // TODO Validity checks
    // TODO Move valid check into different function of updated lifecycle

    resultUpdated();
}

/**
 * Revalidate conditional objective options and then recalculate the points.
 */
function resultUpdated() {
    revalidateAvailability();
    recalculatePoints();

    emit("update:result", result.value);
}

/**
 * Revalidate the conditional radio objective options.
 * - Checks if an option has a conditional, if not it is included in the available list.
 * - If the option does have a conditional, it is checked against the result. Its value determines whether it is in the available list.
 * - If the current result of the objective is not available anymore, it is replaced by the initial result if that is available.
 *      If the initial result is not available, it is set to null.
 * - The objective is set unavailable if there are 0 options. This also automatically makes it valid since there are no options.
 */
function revalidateAvailability() {
    const evaluator = new BooleanExpressionEvaluator();

    sortedMissions.value.forEach((mission) => {
        mission.objectives.forEach((objective) => {
            if (
                objective.type ===
                "App\\Models\\Challenge\\Mission\\Objective\\RadioObjective"
            ) {
                const objectiveResult =
                    result.value.mission_results[mission.uuid]
                        .mission_objective_results[objective.uuid];

                const availableOptions = objective.radio_objective_options
                    .filter((option) => {
                        if (!option.available_condition_ast) {
                            return true;
                        }

                        return evaluator.evaluate(
                            option.available_condition_ast,
                            result.value
                        );
                    })
                    .map((option) => option.uuid);

                result.value.mission_results[
                    mission.uuid
                ].mission_objective_results[objective.uuid].available_options =
                    availableOptions;

                const available = availableOptions.length > 0;
                result.value.mission_results[
                    mission.uuid
                ].mission_objective_results[objective.uuid].available =
                    available;

                if (!availableOptions.includes(objectiveResult.result)) {
                    // TODO Get default
                    const initialResult = getInitialResult(objective);

                    if (!availableOptions.includes(initialResult.result)) {
                        initialResult.result = null;
                        initialResult.points = 0;
                        initialResult.valid = !available;
                    }

                    result.value.mission_results[
                        mission.uuid
                    ].mission_objective_results[objective.uuid].result =
                        initialResult.result;

                    result.value.mission_results[
                        mission.uuid
                    ].mission_objective_results[objective.uuid].points =
                        initialResult.points;

                    result.value.mission_results[
                        mission.uuid
                    ].mission_objective_results[objective.uuid].valid =
                        initialResult.valid;
                }
            }
        });
    });
}

/**
 * Recalculate points for every mission and objective.
 */
function recalculatePoints() {
    let totalPoints = 0;
    let resultValid = true;

    Object.keys(result.value.mission_results).forEach((mission_uuid) => {
        const missionResult = result.value.mission_results[mission_uuid];
        let missionPoints = 0;
        let missionValid = true;
        // TODO Validity checks

        Object.keys(missionResult.mission_objective_results).forEach(
            (objective_uuid) => {
                const objectiveResult =
                    missionResult.mission_objective_results[objective_uuid];

                const objectivePoints = objectiveResult.points;

                if (objectivePoints) {
                    missionPoints += objectivePoints;
                }
                if (!objectiveResult.valid) {
                    missionValid = false;
                }
            }
        );

        result.value.mission_results[mission_uuid].points = missionPoints;
        result.value.mission_results[mission_uuid].valid = missionValid;
        totalPoints += result.value.mission_results[mission_uuid].points;

        if (!result.value.mission_results[mission_uuid].valid) {
            resultValid = false;
        }
    });

    result.value.points = totalPoints;
    result.value.valid = resultValid;
}

onBeforeMount(() => {
    if (props.initialResult === null) {
        createEmptyResult();
    } else {
        result.value = { ...props.initialResult };
        resultUpdated();
    }
});

const sizeWatcher = ref(0);

onMounted(() => {
    nextTick(() => {
        window.addEventListener("resize", onResize);
    });
});

onBeforeUnmount(() => {
    window.addEventListener("resize", onResize);
});

watch(
    () => props.imageEnabled,
    (first, second) => {
        onResize();
    }
);

function onResize() {
    // TODO Use delayed propagation
    sizeWatcher.value++;
}
</script>

<template>
    <mission-input
        :size-watcher="sizeWatcher"
        :image-enabled="imageEnabled"
        :mission="mission"
        :mission-result="result.mission_results[mission.uuid]"
        :disabled="disabled"
        @objective-result-update="handleObjectiveResultUpdate"
        v-for="mission in sortedMissions"
    ></mission-input>
</template>

<style scoped></style>
