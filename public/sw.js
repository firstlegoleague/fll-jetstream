self.addEventListener("push", async (e) => {
    if (self.Notification && self.Notification.permission === "granted") {
        //notifications are supported and permission  granted!
        if (e.data) {
            const msg = await e.data.json();
            await console.log(msg);
            // e.waitUntil(

            await self.registration.showNotification(msg.title, {
                body: msg.body,
                icon: msg.icon,
                actions: msg.actions,
                silent: false,
                vibrate: [300, 100, 400],
            });
        }
    }
});
