import { sentryVitePlugin } from "@sentry/vite-plugin";
import { defineConfig, BuildOptions } from "vite";
import laravel from "laravel-vite-plugin";
import vue from "@vitejs/plugin-vue";

export default defineConfig({
    plugins: [
        laravel({
            input: "resources/js/app.js",
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
        sentryVitePlugin({
            org: "flltools",
            project: "flltools-frontend",
            url: "https://sentry.fll.tools",

            authToken: process.env.SENTRY_AUTH_TOKEN,
        }),
    ],

    build: {
        sourcemap: true,
    },
});
