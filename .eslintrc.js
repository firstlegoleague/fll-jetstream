module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true,
    },
    extends: ["plugin:vue/vue3-essential", "airbnb-base", "prettier"],
    settings: {
        "import/resolver": {
            alias: {
                map: [["@", "./resources/js"]],
            },
        },
    },
    overrides: [],
    parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
    },
    plugins: ["vue"],
    rules: {
        // "indent": ["error", 4],
        "vue/multi-word-component-names": "off",
    },
    globals: {
        route: "readonly",
        axios: "readonly",
        router: "readonly",
    },
};
