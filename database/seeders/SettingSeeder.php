<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // TODO Register
        // TODO Current Tournament

        $settingFinalName = new \App\Models\Setting();
        $settingFinalName->key = "finalName";
        $settingFinalName->name = "settings.finalName";
        $settingFinalName->type = "TEXT";
        $settingFinalName->category = "General";
        $settingFinalName->priority = 10;
        $settingFinalName->save();

        $settingOpenRegistering = new \App\Models\Setting();
        $settingOpenRegistering->key = "openRegistering";
        $settingOpenRegistering->name = "settings.openRegistration";
        $settingOpenRegistering->type = "BOOLEAN";
        $settingOpenRegistering->value = "0";
        $settingOpenRegistering->category = "General";
        $settingOpenRegistering->priority = 20;
        $settingOpenRegistering->save();

        $settingPublishResults = new \App\Models\Setting();
        $settingPublishResults->key = "publishResults";
        $settingPublishResults->name = "settings.resultsDefaultPublic";
        $settingPublishResults->type = "BOOLEAN";
        $settingPublishResults->value = "0";
        $settingPublishResults->category = "General";
        $settingPublishResults->priority = 30;
        $settingPublishResults->save();

        $settingModuleTable = new \App\Models\Setting();
        $settingModuleTable->key = "ModuleTable";
        $settingModuleTable->name = "settings.modules.moduleTable";
        $settingModuleTable->type = "BOOLEAN";
        $settingModuleTable->value = "0";
        $settingModuleTable->category = "Modules";
        $settingModuleTable->priority = 20;
        $settingModuleTable->save();

        $settingModuleJudge = new \App\Models\Setting();
        $settingModuleJudge->key = "ModuleJudge";
        $settingModuleJudge->name = "settings.modules.moduleJudge";
        $settingModuleJudge->type = "BOOLEAN";
        $settingModuleJudge->value = "0";
        $settingModuleJudge->category = "Modules";
        $settingModuleJudge->priority = 40;
        $settingModuleJudge->save();

        $settingModuleSeasons = new \App\Models\Setting();
        $settingModuleSeasons->key = "ModuleSeason";
        $settingModuleSeasons->name = "settings.modules.moduleSeasons";
        $settingModuleSeasons->type = "BOOLEAN";
        $settingModuleSeasons->value = "0";
        $settingModuleSeasons->category = "Modules";
        $settingModuleSeasons->priority = 50;
        $settingModuleSeasons->save();

        $settingModuleAdvTeams = new \App\Models\Setting();
        $settingModuleAdvTeams->key = "ModuleAdvTeams";
        $settingModuleAdvTeams->name = "settings.modules.moduleAdvTeams";
        $settingModuleAdvTeams->type = "BOOLEAN";
        $settingModuleAdvTeams->value = "0";
        $settingModuleAdvTeams->category = "Modules";
        $settingModuleAdvTeams->priority = 60;
        $settingModuleAdvTeams->save();

        $settingModuleDisplay = new \App\Models\Setting();
        $settingModuleDisplay->key = "ModuleDisplay";
        $settingModuleDisplay->name = "settings.modules.moduleDisplay";
        $settingModuleDisplay->type = "BOOLEAN";
        $settingModuleDisplay->value = "0";
        $settingModuleDisplay->category = "Modules";
        $settingModuleDisplay->priority = 70;
        $settingModuleDisplay->save();

        $settingModuleButtons = new \App\Models\Setting();
        $settingModuleButtons->key = "ModuleButtons";
        $settingModuleButtons->name = "settings.modules.moduleButtons";
        $settingModuleButtons->type = "BOOLEAN";
        $settingModuleButtons->value = "0";
        $settingModuleButtons->category = "Modules";
        $settingModuleButtons->priority = 80;
        $settingModuleButtons->save();

        $settingModuleSchedule = new \App\Models\Setting();
        $settingModuleSchedule->key = "ModuleSchedule";
        $settingModuleSchedule->name = "settings.modules.moduleSchedule";
        $settingModuleSchedule->type = "BOOLEAN";
        $settingModuleSchedule->value = "0";
        $settingModuleSchedule->category = "Modules";
        $settingModuleSchedule->priority = 90;
        $settingModuleSchedule->save();

        $settingModuleScheduleGen = new \App\Models\Setting();
        $settingModuleScheduleGen->key = "ModuleScheduleGen";
        $settingModuleScheduleGen->name = "settings.modules.moduleScheduleGen";
        $settingModuleScheduleGen->type = "BOOLEAN";
        $settingModuleScheduleGen->value = "0";
        $settingModuleScheduleGen->category = "Modules";
        $settingModuleScheduleGen->priority = 100;
        $settingModuleScheduleGen->save();

        $settingModuleLivestream = new \App\Models\Setting();
        $settingModuleLivestream->key = "ModuleLivestream";
        $settingModuleLivestream->name = "settings.modules.moduleLivestream";
        $settingModuleLivestream->type = "BOOLEAN";
        $settingModuleLivestream->value = "0";
        $settingModuleLivestream->category = "Modules";
        $settingModuleLivestream->priority = 110;
        $settingModuleLivestream->save();
    }
}
