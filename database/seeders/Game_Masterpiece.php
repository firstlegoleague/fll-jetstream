<?php
namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Challenge\ScoreSheet;
use App\Models\Challenge\Mission\Mission;
use App\Models\Challenge\Mission\Objective\RadioObjective;
use App\Models\Challenge\Mission\Objective\RadioObjectiveOption;
use App\Models\Challenge\Mission\Objective\SpinnerObjective;

class GameMasterpiece extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $masterpiece = new ScoreSheet();

        $masterpiece->name = "MASTERPIECE";
        $masterpiece->season = "2023 - 2024";
        $masterpiece->save();

        $M00 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "EIB",
            'long_name' => "scoresheet.masterpiece.M00_long_name",
            'description' => "Description test",
            'requirements' => "Requirements test",
            'updates' => "Updates test",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/EIB.png", 
            'priority' => 10
        ]);
        $M00_1 = RadioObjective::create([
            'mission_uuid' => $M00->uuid,
            'name' => "",
            'statement' => 'scoresheet.masterpiece.M00_1_desc',
            'priority' => 10
        ]);
        $M00_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M00_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M00_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M00_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M01 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M01",
            'long_name' => 'scoresheet.masterpiece.M01_long_name',
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M01.png",
            'priority' => 20
        ]);
        $M01_1 = RadioObjective::create([
            'mission_uuid' => $M01->uuid,
            'name' => "",
            'statement' => 'scoresheet.masterpiece.M01_1_desc',
            'priority' => 10
        ]);
        $M01_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M01_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M01_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M01_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M02 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M02",
            'long_name' => "scoresheet.masterpiece.M02_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M02.png",
            'priority' => 30
        ]);
        $M02_1 = RadioObjective::create([
            'mission_uuid' => $M02->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M02_1_desc",
            'priority' => 10
        ]);
        $M02_1_noChange = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M02_1->uuid,
            'value' => "scoresheet.general.noChange",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 5
        ]);
        $M02_1_blue = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M02_1->uuid,
            'value' => "scoresheet.general.blue",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M02_1_pink = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M02_1->uuid,
            'value' => "scoresheet.general.pink",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M02_1_orange = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M02_1->uuid,
            'value' => "scoresheet.general.orange",
            'points' => 30,
            'bold' => false,
            'color' => "primary",
            'priority' => 30
        ]);
        $M02_2 = RadioObjective::create([
            'mission_uuid' => $M02->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M02_2_desc",
            'priority' => 20
        ]);
        $M02_2_noChange = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M02_2->uuid,
            'value' => "scoresheet.general.noChange",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 5,
            'available_condition' => "'/mission_results/{$M02->uuid}/mission_objective_results/{$M02_1->uuid}/result' != null and '/mission_results/{$M02->uuid}/mission_objective_results/{$M02_1->uuid}/result' != '{$M02_1_noChange->uuid}'"
        ]);
        $M02_2_blue = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M02_2->uuid,
            'value' => "scoresheet.general.blue",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
            'available_condition' => "'/mission_results/{$M02->uuid}/mission_objective_results/{$M02_1->uuid}/result' == '{$M02_1_blue->uuid}'"
        ]);
        $M02_2_pink = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M02_2->uuid,
            'value' => "scoresheet.general.pink",
            'points' => 30,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
            'available_condition' => "'/mission_results/{$M02->uuid}/mission_objective_results/{$M02_1->uuid}/result' == '{$M02_1_pink->uuid}'"
        ]);
        $M02_2_orange = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M02_2->uuid,
            'value' => "scoresheet.general.orange",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 30,
            'available_condition' => "'/mission_results/{$M02->uuid}/mission_objective_results/{$M02_1->uuid}/result' == '{$M02_1_orange->uuid}'"
        ]);

        $M03 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M03",
            'long_name' => "scoresheet.masterpiece.M03_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M03.png",
            'priority' => 40
        ]);
        $M03_1 = RadioObjective::create([
            'mission_uuid' => $M03->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M03_1_desc",
            'priority' => 10
        ]);
        $M03_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M03_1->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M03_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M03_1->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);


        $M04 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M04",
            'long_name' => "scoresheet.masterpiece.M04_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M04.png",
            'priority' => 50
        ]);
        $M04_1 = RadioObjective::create([
            'mission_uuid' => $M04->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M04_1_desc",
            'priority' => 10
        ]);
        $M04_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M04_1->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M04_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M04_1->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M04_2 = RadioObjective::create([
            'mission_uuid' => $M04->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M04_2_desc",
            'priority' => 20,
        ]);

        $M04_2_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M04_2->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
            'available_condition' => "'/mission_results/{$M04->uuid}/mission_objective_results/{$M04_1->uuid}/result' == '{$M04_1_yes->uuid}'",
        ]);
        $M04_2_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M04_2->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
            'available_condition' => "'/mission_results/{$M04->uuid}/mission_objective_results/{$M04_1->uuid}/result' == '{$M04_1_yes->uuid}'",
        ]);

        $M05 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M05",
            'long_name' => "scoresheet.masterpiece.M05_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M05.png",
            'priority' => 60
        ]);
        $M05_1 = RadioObjective::create([
            'mission_uuid' => $M05->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M05_1_desc",
            'priority' => 10
        ]);
        $M05_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M05_1->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 30,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M05_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M05_1->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M06 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M06",
            'long_name' => "scoresheet.masterpiece.M06_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M06.png",
            'priority' => 70
        ]);
        $M06_1 = RadioObjective::create([
            'mission_uuid' => $M06->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M06_1_desc",
            'priority' => 10
        ]);
        $M06_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M06_1->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M06_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M06_1->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M06_2 = RadioObjective::create([
            'mission_uuid' => $M06->uuid,
            'name' => "",
            'statement' =>  "scoresheet.masterpiece.M06_2_desc",
            'priority' => 20
        ]);
        $M06_2_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M06_2->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M06_2_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M06_2->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M07 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M07",
            'long_name' => "scoresheet.masterpiece.M07_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M07.png",
            'priority' => 80
        ]);
        $M07_1 = RadioObjective::create([
            'mission_uuid' => $M07->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M07_1_desc",
            'priority' => 10
        ]);
        $M07_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M07_1->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M07_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M07_1->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M08 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M08",
            'long_name' => "scoresheet.masterpiece.M08_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M08.png",
            'priority' => 90
        ]);
        $M08_1 = RadioObjective::create([
            'mission_uuid' => $M08->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M08_1_desc",
            'priority' => 10
        ]);
        $M08_1_noChange = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M08_1->uuid,
            'value' => "scoresheet.general.noChange",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 5
        ]);
        $M08_1_lightBlue = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M08_1->uuid,
            'value' => "scoresheet.general.lightBlue",
            'points' => 30,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M08_1_mediumblue = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M08_1->uuid,
            'value' => "scoresheet.general.blue",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M08_1_darkBlue = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M08_1->uuid,
            'value' => "scoresheet.general.darkBlue",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 30
        ]);

        $M09 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M09",
            'long_name' => "scoresheet.masterpiece.M09_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M09.png",
            'priority' => 100
        ]);
        $M09_1 = RadioObjective::create([
            'mission_uuid' => $M09->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M09_1_desc",
            'priority' => 10
        ]);
        $M09_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M09_1->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M09_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M09_1->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M09_2 = RadioObjective::create([
            'mission_uuid' => $M09->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M09_2_desc",
            'priority' => 20
        ]);
        $M09_2_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M09_2->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M09_2_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M09_2->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M10 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => 'M10',
            'long_name' => "scoresheet.masterpiece.M10_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M10.png",
            'priority' => 110
        ]);
        $M10_1 = SpinnerObjective::create([
            'mission_uuid' => $M10->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M10_1_desc",
            'min'=> 0,
            'max' => 3,
            'points' => '10'
        ]);

        $M11 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M11",
            'long_name' => "scoresheet.masterpiece.M11_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M11.png",
            'priority' => 120
        ]);
        $M11_1 = RadioObjective::create([
            'mission_uuid' => $M11->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M11_1_desc",
            'priority' => 10
        ]);
        $M11_1_noChange = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M11_1->uuid,
            'value' => "scoresheet.general.noChange",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 5
        ]);
        $M11_1_yellow = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M11_1->uuid,
            'value' => "scoresheet.general.yellow",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M11_1_green = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M11_1->uuid,
            'value' => "scoresheet.general.green",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M11_1_blue = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M11_1->uuid,
            'value' => "scoresheet.general.blue",
            'points' => 30,
            'bold' => false,
            'color' => "primary",
            'priority' => 30
        ]);

        $M12 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M12",
            'long_name' => "scoresheet.masterpiece.M12_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M12.png",
            'priority' => 130
        ]);
        $M12_1 = RadioObjective::create([
            'mission_uuid' => $M12->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M12_1_desc",
            'priority' => 10
        ]);
        $M12_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M12_1->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M12_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M12_1->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M12_2 = RadioObjective::create([
            'mission_uuid' => $M12->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M12_2_desc",
            'priority' => 20
        ]);
        $M12_2_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M12_2->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
            'available_condition' => "'/mission_results/{$M12->uuid}/mission_objective_results/{$M12_1->uuid}/result' == '{$M12_1_yes->uuid}'",
        ]);
        $M12_2_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M12_2->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
            'available_condition' => "'/mission_results/{$M12->uuid}/mission_objective_results/{$M12_1->uuid}/result' == '{$M12_1_yes->uuid}'",
        ]);

        $M13 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => "M13",
            'long_name' => "scoresheet.masterpiece.M13_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M13.png",
            'priority' => 140
        ]);
        $M13_1 = RadioObjective::create([
            'mission_uuid' => $M13->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M13_1_desc",
            'priority' => 10
        ]);
        $M13_1_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M13_1->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M13_1_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M13_1->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M13_2 = RadioObjective::create([
            'mission_uuid' => $M13->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M13_2_desc",
            'priority' => 20
        ]);
        $M13_2_yes = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M13_2->uuid,
            'value' => "scoresheet.general.yes",
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M13_2_no = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M13_2->uuid,
            'value' => "scoresheet.general.no",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M14 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => 'M14',
            'long_name' => "scoresheet.masterpiece.M14_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M14.png",
            'priority' => 150
        ]);
        $M14_1 = SpinnerObjective::create([
            'mission_uuid' => $M14->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M14_1_desc",
            'min'=> 0,
            'max' => 7,
            'points' => '5',
            'priority' => 10
        ]);
        $M14_2 = SpinnerObjective::create([
            'mission_uuid' => $M14->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M14_2_desc",
            'min'=> 0,
            'max' => 7,
            'points' => '5',
            'priority' => 20
        ]);

        $M15 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => 'M15',
            'long_name' => "scoresheet.masterpiece.M15_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/M15.png",
            'priority' => 160
        ]);
        $M15_1 = SpinnerObjective::create([
            'mission_uuid' => $M15->uuid,
            'name' => "",
            'statement' => "scoresheet.masterpiece.M15_1_desc",
            'min'=> 0,
            'max' => 5,
            'points' => '10'
        ]);

        $M16 = Mission::create([
            'score_sheet_uuid' => $masterpiece->uuid,
            'short_name' => 'PT',
            'long_name' => "scoresheet.general.gp_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Masterpiece/PT.png",
            'priority' => 170
        ]);
        $M16_1 = RadioObjective::create([
            'mission_uuid' => $M16->uuid,
            'name' => "",
            'statement' => "scoresheet.general.gp_desc",
            'priority' => 10
        ]);

        $M16_1_0 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "0",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M16_1_1 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "1",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M16_1_2 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "2",
            'points' => 15,
            'bold' => false,
            'color' => "primary",
            'priority' => 30
        ]);
        $M16_1_3 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "3",
            'points' => 25,
            'bold' => false,
            'color' => "primary",
            'priority' => 40
        ]);
        $M16_1_4 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "4",
            'points' => 35,
            'bold' => false,
            'color' => "primary",
            'priority' => 50
        ]);
        $M16_1_5 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "5",
            'points' => 50,
            'bold' => false,
            'color' => "primary",
            'priority' => 60
        ]);
        $M16_1_7 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "6",
            'points' => 50,
            'bold' => false,
            'color' => "primary",
            'priority' => 70
        ]);

    }
}
