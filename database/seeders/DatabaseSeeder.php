<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            developmentUsers::class,
            RoleAndAbilitySeeder::class,
            SettingSeeder::class,
//            GameSuperPowered::class,
//            GameMasterpiece::class,
            Game_Submerged::class,
            Tournament::class,
            RoundsTablesTeams::class,
        ]);
    }
}
