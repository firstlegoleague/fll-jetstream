<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoundsTablesTeams extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if(env('APP_ENV', "prod") == "local" || env('APP_ENV', 'prod') == "staging") {

            $tournament = \App\Models\Tournament\Tournament::all()->first();

            $round = new \App\Models\Tournament\Round();
            $round->tournament_uuid = $tournament->uuid;
            $round->name = "Round 1";
            $round->score_hidden = false;
            $round->is_practice = false;
            $round->priority = 1;
            $round->save();

            $round = new \App\Models\Tournament\Round();
            $round->tournament_uuid = $tournament->uuid;
            $round->name = "Round 2";
            $round->score_hidden = false;
            $round->is_practice = false;
            $round->priority = 2;
            $round->save();

            $round = new \App\Models\Tournament\Round();
            $round->tournament_uuid = $tournament->uuid;
            $round->name = "Round 3";
            $round->score_hidden = true;
            $round->is_practice = false;
            $round->priority = 3;
            $round->save();

            $team = new \App\Models\Tournament\Team();
            $team->tournament_uuid = $tournament->uuid;
            $team->id = 1000;
            $team->affiliate = "The lonely four bits";
            $team->name = "Nibble";
            $team->save();

            $team = new \App\Models\Tournament\Team();
            $team->tournament_uuid = $tournament->uuid;
            $team->id = 1001;
            $team->affiliate = "NSA";
            $team->name = "Flowerbed";
            $team->save();




        }
    }
}
