<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class developmentUsers extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if(env('APP_ENV', "prod") == "local" || env('APP_ENV', 'prod') == "staging"){
            $userThijs = new \App\Models\User();
            $userThijs->name = "Thijs Tops";
            $userThijs->email = "thijs@flltools.com";
            $userThijs->password = bcrypt(env("DEVEL_PASS_THIJS", "password"));
            $userThijs->save();

            $userJamie = new \App\Models\User();
            $userJamie->name = "Jamie Trip";
            $userJamie->email = "jamie@flltools.com";
            $userJamie->password = bcrypt(env("DEVEL_PASS_JAMIE", "password"));
            $userJamie->save();
        }

    }
}
