<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GameSuperPowered extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superPowered = new \App\Models\Challenge\ScoreSheet();

        $superPowered->name = "SuperPowered";
        $superPowered->season = "2022 - 2023";
        $superPowered->save();

        $m00 = new \App\Models\Challenge\Mission\Mission();
        $m00->score_sheet_uuid = $superPowered->uuid;
        $m00->short_name = "M00";
        $m00->long_name = 'Inspectie Uitrusting';
        $m00->description = 'Past alle uitrusting in het vak?';
        $m00->requirements = '{}';
        $m00->updates = '{}';
        $m00->priority = 10;
        $m00->save();

        $m00_1 = new \App\Models\Challenge\Mission\Objective\RadioObjective();
        $m00_1->mission_uuid = $m00->uuid;
        $m00_1->name = '';
        $m00_1->statement = '';
        $m00_1->priority = 10;
        $m00_1->save();

        $m00_1_yes = new \App\Models\Challenge\Mission\Objective\RadioObjectiveOption();
        $m00_1_yes->radio_objective_uuid = $m00_1->uuid;
        $m00_1_yes->value = 'Ja';
        $m00_1_yes->points = 20;
        $m00_1_yes->bold = false;
        $m00_1_yes->color = "primary";
        $m00_1_yes->priority = 10;
        $m00_1_yes->save();

        $m00_1_no = new \App\Models\Challenge\Mission\Objective\RadioObjectiveOption();
        $m00_1_no->radio_objective_uuid = $m00_1->uuid;
        $m00_1_no->value = 'Nee';
        $m00_1_no->points = 0;
        $m00_1_no->bold = false;
        $m00_1_no->color = "primary";
        $m00_1_no->priority = 20;
        $m00_1_no->save();

        $m00_1->initial_option_uuid = $m00_1_yes->uuid;
        $m00_1->save();



    }
}
