<?php

namespace Database\Seeders;

use App\Models\Tournament\Team;
use App\Models\Tournament\Tournament;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Bouncer;

class RoleAndAbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    static public function run(): void
    {
        $superAdmin = Bouncer::role()->firstOrCreate([
            'name' => 'superAdmin',
            'title' => 'Super Administrator',
        ]);

        $admin = Bouncer::role()->firstOrCreate([
            'title' => 'Administrator',
            'name' => 'admin',
        ]);

        $secretariat = Bouncer::role()->firstOrCreate([
            'title' => 'Secretariat',
            'name' => 'secretariat',
        ]);

        $headRef = Bouncer::role()->firstOrCreate([
            'title' => 'Head Referee',
            'name' => 'headReferee',
        ]);

        $referee = Bouncer::role()->firstOrCreate([
            'title' => 'Referee',
            'name' => 'referee',
        ]);

        $headJudge = Bouncer::role()->firstOrCreate([
            'title' => 'Head Judge',
            'name' => 'headJudge',
        ]);

        $judge = Bouncer::role()->firstOrCreate([
            'title' => 'Judge',
            'name' => 'judge',
        ]);

        $coach = Bouncer::role()->firstOrCreate([
            'title' => 'Coach',
            'name' => 'coach',
        ]);

        $user = Bouncer::role()->firstOrCreate([
            'title' => 'User',
            'name' => 'user',
        ]);

        $guest = Bouncer::role()->firstOrCreate([
            'title' => 'Guest',
            'name' => 'guest',
        ]);

        // Guests
        foreach ([$guest, $user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("team.view"); }
        foreach ([$guest, $user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("score.view.public"); }
        foreach ([$guest, $user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("scoreboard.view"); }
        foreach ([$guest, $user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("schedule.view"); }
        foreach ([$guest, $user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("tournament.view"); }
        foreach ([$guest, $user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("table.view"); }
        foreach ([$guest, $user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("round.view"); }

        // Allow users and up
        foreach ([$user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("notification.subscribe"); }
        foreach ([$user, $coach, $judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("notification.unsubscribe"); }

        // Allow coaches to edit their own team and manage their own media
        Bouncer::allow($coach)->to("teams.edit.own");
        Bouncer::allow($role)->to("schedule.view");
        Bouncer::allow($role)->to("tournament.view");
        Bouncer::allow($role)->to("table.view");
        Bouncer::allow($role)->to("round.view");
        Bouncer::allow($coach)->to("media.judge.create");
        Bouncer::allow($coach)->to("media.judge.view.own");
        Bouncer::allow($coach)->to("media.judge.edit.own");
        Bouncer::allow($coach)->to("media.judge.delete.own");


        // Allow all volunteers to clear the cache
        foreach ([$judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("cache.flush"); }
        foreach ([$judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("remark.create"); }
        foreach ([$judge, $headJudge, $referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("remark.view.own"); }

        // Allow referees to create scores
        foreach ([$referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("scoresheet.view"); }
        foreach ([$referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("result.create"); }
        foreach ([$referee, $headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("result.view"); }

        // Allow head referee to edit and delete scores
        foreach ([$headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("result.edit"); }
        foreach ([$headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("result.delete"); }
        foreach ([$headRef, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("result.comments.view"); }


        // Allow judges to create judge results and view the media assigned

        foreach ([$judge, $headJudge, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("judge.view.own"); }
        foreach ([$judge, $headJudge, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("judge.create"); }
        foreach ([$judge, $headJudge, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("cache.edit.own"); }
        foreach ([$judge, $headJudge, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("media.judge.view.assigned"); }


        // Allow the head judge to edit and view all the results.
        foreach ([$headJudge, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("judge.edit"); }
        foreach ([$headJudge, $secretariat, $admin] as $role) { Bouncer::allow($role)->to("judge.view"); }

        // Give the game secretariat some more power
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("team.edit"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("team.create"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("team.delete"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("schedule.create"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("schedule.edit"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("schedule.delete"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("media.all.view"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("judge.delete"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("user.edit"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("user.view"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("round.create"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("round.edit"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("round.delete"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("table.create"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("table.edit"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("table.delete"); }
        foreach ([$secretariat, $admin] as $role) { Bouncer::allow($role)->to("remark.view"); }

        // Give the admin more power
        Bouncer::allow($admin)->to("tournament.create");
        Bouncer::allow($admin)->to("tournament.edit");
        Bouncer::allow($admin)->to("tournament.delete");
        Bouncer::allow($admin)->to("scoresheet.edit");
        Bouncer::allow($admin)->to("scoresheet.create");
        Bouncer::allow($admin)->to("scoresheet.delete");
        Bouncer::allow($admin)->to("media.view");
        Bouncer::allow($admin)->to("media.delete");
        Bouncer::allow($admin)->to("media.edit");
        Bouncer::allow($admin)->to("media.update");
        Bouncer::allow($admin)->to("user.create");
        Bouncer::allow($admin)->to("user.delete");
        Bouncer::allow($admin)->to("user.assignRole");
        Bouncer::allow($admin)->to("livestream.create");
        Bouncer::allow($admin)->to("livestream.view");
        Bouncer::allow($admin)->to("livestream.delete");
        Bouncer::allow($admin)->to("setting.view");
        Bouncer::allow($admin)->to("setting.edit");
        Bouncer::allow($admin)->to("remark.edit");
        Bouncer::allow($admin)->to("remark.delete");


        // Allow superAdmin to do anything
        Bouncer::allow($superAdmin)->everything();
        Bouncer::allow($superAdmin)->toOwnEverything();

        if(env('APP_ENV', "prod") == "local" || env('APP_ENV', 'prod') == "staging"){
            $userThijs = \App\Models\User::all()->where("email","thijs@flltools.com")->first();
            $userThijs->assign($superAdmin);
            $userThijs->assign($admin);
            $userThijs->save();

            $userJamie = \App\Models\User::all()->where("email","jamie@flltools.com")->first();
            $userJamie->assign($superAdmin);
            $userJamie->assign($admin);
            $userJamie->save();

        }
    }
}
