<?php

namespace Database\Seeders;

use App\Models\Challenge\Mission\Mission;
use App\Models\Challenge\Mission\Objective\RadioObjective;
use App\Models\Challenge\Mission\Objective\RadioObjectiveOption;
use App\Models\Challenge\Mission\Objective\SpinnerObjective;
use App\Models\Challenge\ScoreSheet;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class Game_Submerged extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $submerged = new ScoreSheet();
        $submerged->uuid = Uuid::fromBytes("Submerged-Season");
        $submerged->name = "SUBMERGED";
        $submerged->season = "2024 - 2025";
        $submerged->save();

        $M00 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM00-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "EIB",
            'long_name' => "scoresheet.submerged.M00_long_name",
            'description' => "scoresheet.submerged.M00_description",
            'requirements' => "scoresheet.submerged.M00_requirements",
            'updates' => "scoresheet.submerged.M00_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/EIB.png",
            'priority' => 10,
        ]);
        $M00_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM00_1RO"),
            'mission_uuid' => $M00->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M00_1_desc',
            'priority' => 10
        ]);
        $M00_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM00_1-Y"),
            'radio_objective_uuid' => $M00_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M00_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM00_1-N"),
            'radio_objective_uuid' => $M00_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);


        $M01 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM01-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M01",
            'long_name' => "scoresheet.submerged.M01_long_name",
            'description' => "scoresheet.submerged.M01_description",
            'requirements' => "scoresheet.submerged.M01_requirements",
            'updates' => "scoresheet.submerged.M01_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M01.png",
            'no_equipment' => true,
            'priority' => 20,
        ]);
        $M01_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_1RO"),
            'mission_uuid' => $M01->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M01_1_desc',
            'priority' => 10
        ]);
        $M01_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_1-Y"),
            'radio_objective_uuid' => $M01_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M01_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_1-N"),
            'radio_objective_uuid' => $M01_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M01_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_2RO"),
            'mission_uuid' => $M01->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M01_2_desc',
            'priority' => 20,
        ]);
        $M01_2_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_2-Y"),
            'radio_objective_uuid' => $M01_2->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
            'available_condition' => "'/mission_results/{$M01->uuid}/mission_objective_results/{$M01_1->uuid}/result' == '{$M01_1_yes->uuid}'"
        ]);
        $M01_2_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_2-N"),
            'radio_objective_uuid' => $M01_2->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
            'available_condition' => "'/mission_results/{$M01->uuid}/mission_objective_results/{$M01_1->uuid}/result' == '{$M01_1_yes->uuid}'"
        ]);
        $M01_3 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_3RO"),
            'mission_uuid' => $M01->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M01_3_desc',
            'priority' => 30
        ]);
        $M01_3_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_3-Y"),
            'radio_objective_uuid' => $M01_3->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M01_3_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM01_3-N"),
            'radio_objective_uuid' => $M01_3->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M02 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM02-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M02",
            'long_name' => "scoresheet.submerged.M02_long_name",
            'description' => "scoresheet.submerged.M02_description",
            'requirements' => "scoresheet.submerged.M02_requirements",
            'updates' => "scoresheet.submerged.M02_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M02.png",
            'priority' => 30,
        ]);
        $M02_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM02_1RO"),
            'mission_uuid' => $M02->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M02_1_desc',
            'priority' => 10
        ]);
        $M02_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM02_1-Y"),
            'radio_objective_uuid' => $M02_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M02_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM02_1-N"),
            'radio_objective_uuid' => $M02_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M02_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM02_2RO"),
            'mission_uuid' => $M02->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M02_2_desc',
            'priority' => 20,
        ]);
        $M02_2_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM02_2-Y"),
            'radio_objective_uuid' => $M02_2->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
        ]);
        $M02_2_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM02_2-N"),
            'radio_objective_uuid' => $M02_2->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
        ]);

        $M03 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM03-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M03",
            'long_name' => "scoresheet.submerged.M03_long_name",
            'description' => "scoresheet.submerged.M03_description",
            'requirements' => "scoresheet.submerged.M03_requirements",
            'updates' => "scoresheet.submerged.M03_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M03.png",
            'no_equipment' => true,
            'priority' => 40,
        ]);
        $M03_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM03_1RO"),
            'mission_uuid' => $M03->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M03_1_desc',
            'priority' => 10
        ]);
        $M03_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM03_1-Y"),
            'radio_objective_uuid' => $M03_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M03_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM03_1-N"),
            'radio_objective_uuid' => $M03_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M03_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM03_2RO"),
            'mission_uuid' => $M03->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M03_2_desc',
            'priority' => 10
        ]);
        $M03_2_0 = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM03_2-0"),
            'radio_objective_uuid' => $M03_2->uuid,
            'value' => '0',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M03_2_1 = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM03_2-1"),
            'radio_objective_uuid' => $M03_2->uuid,
            'value' => '1',
            'points' => 5,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M03_2_2 = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM03_2-2"),
            'radio_objective_uuid' => $M03_2->uuid,
            'value' => '2',
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 30
        ]);
        $M03_2_3 = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM03_2-3"),
            'radio_objective_uuid' => $M03_2->uuid,
            'value' => '3',
            'points' => 15,
            'bold' => false,
            'color' => "primary",
            'priority' => 40
        ]);

        $M04 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM04-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M04",
            'long_name' => "scoresheet.submerged.M04_long_name",
            'description' => "scoresheet.submerged.M04_description",
            'requirements' => "scoresheet.submerged.M04_requirements",
            'updates' => "scoresheet.submerged.M04_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M04.png",
            'priority' => 50,
        ]);
        $M04_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM04_1RO"),
            'mission_uuid' => $M04->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M04_1_desc',
            'priority' => 10
        ]);
        $M04_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM04_1-Y"),
            'radio_objective_uuid' => $M04_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M04_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM04_1-N"),
            'radio_objective_uuid' => $M04_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M04_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM04_2RO"),
            'mission_uuid' => $M04->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M04_2_desc',
            'priority' => 20,
        ]);
        $M04_2_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM04_2-Y"),
            'radio_objective_uuid' => $M04_2->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
        ]);
        $M04_2_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM04_2-N"),
            'radio_objective_uuid' => $M04_2->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
        ]);

        $M05 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM05-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M05",
            'long_name' => "scoresheet.submerged.M05_long_name",
            'description' => "scoresheet.submerged.M05_description",
            'requirements' => "scoresheet.submerged.M05_requirements",
            'updates' => "scoresheet.submerged.M05_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M05.png",
            'priority' => 60,
        ]);
        $M05_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM05_1RO"),
            'mission_uuid' => $M05->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M05_1_desc',
            'priority' => 10
        ]);
        $M05_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM05_1-Y"),
            'radio_objective_uuid' => $M05_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 30,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M05_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM05_1-N"),
            'radio_objective_uuid' => $M05_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M06 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM06-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M06",
            'long_name' => "scoresheet.submerged.M06_long_name",
            'description' => "scoresheet.submerged.M06_description",
            'requirements' => "scoresheet.submerged.M06_requirements",
            'updates' => "scoresheet.submerged.M06_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M06.png",
            'no_equipment' => true,
            'priority' => 70,
        ]);
        $M06_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM06_1RO"),
            'mission_uuid' => $M06->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M06_1_desc',
            'priority' => 10
        ]);
        $M06_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM06_1-Y"),
            'radio_objective_uuid' => $M06_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M06_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM06_1-N"),
            'radio_objective_uuid' => $M06_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M07 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM07-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M07",
            'long_name' => "scoresheet.submerged.M07_long_name",
            'description' => "scoresheet.submerged.M07_description",
            'requirements' => "scoresheet.submerged.M07_requirements",
            'updates' => "scoresheet.submerged.M07_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M07.png",
            'no_equipment' => true,
            'priority' => 80,
        ]);
        $M07_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM07_1RO"),
            'mission_uuid' => $M07->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M07_1_desc',
            'priority' => 10
        ]);
        $M07_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM07_1-Y"),
            'radio_objective_uuid' => $M07_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M07_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM07_1-N"),
            'radio_objective_uuid' => $M07_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M08 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM08-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M08",
            'long_name' => "scoresheet.submerged.M08_long_name",
            'description' => "scoresheet.submerged.M08_description",
            'requirements' => "scoresheet.submerged.M08_requirements",
            'updates' => "scoresheet.submerged.M08_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M08.png",
            'no_equipment' => true,
            'priority' => 90,
        ]);
        $M08_1 = SpinnerObjective::create([
            'mission_uuid' => $M08->uuid,
            'name' => "",
            'statement' => "scoresheet.submerged.M08_1_desc",
            'min'=> 0,
            'max' => 4,
            'points' => '10',
            'priority' => 10
        ]);

        $M09 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM09-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M09",
            'long_name' => "scoresheet.submerged.M09_long_name",
            'description' => "scoresheet.submerged.M09_description",
            'requirements' => "scoresheet.submerged.M09_requirements",
            'updates' => "scoresheet.submerged.M09_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M09.png",
            'priority' => 100,
        ]);
        $M09_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM09_1RO"),
            'mission_uuid' => $M09->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M09_1_desc',
            'priority' => 10
        ]);
        $M09_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM09_1-Y"),
            'radio_objective_uuid' => $M09_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M09_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM09_1-N"),
            'radio_objective_uuid' => $M09_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M09_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM09_2RO"),
            'mission_uuid' => $M09->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M09_2_desc',
            'priority' => 20,
        ]);
        $M09_2_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM09_2-Y"),
            'radio_objective_uuid' => $M09_2->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
        ]);
        $M09_2_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM09_2-N"),
            'radio_objective_uuid' => $M09_2->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
        ]);

        $M10 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM10-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M10",
            'long_name' => "scoresheet.submerged.M10_long_name",
            'description' => "scoresheet.submerged.M10_description",
            'requirements' => "scoresheet.submerged.M10_requirements",
            'updates' => "scoresheet.submerged.M10_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M10.png",
            'no_equipment' => true,
            'priority' => 110,
        ]);
        $M10_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM10_1RO"),
            'mission_uuid' => $M10->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M10_1_desc',
            'priority' => 10
        ]);
        $M10_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM10_1-Y"),
            'radio_objective_uuid' => $M10_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 30,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M10_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM10_1-N"),
            'radio_objective_uuid' => $M10_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M10_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM10_2RO"),
            'mission_uuid' => $M10->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M10_2_desc',
            'priority' => 20,
        ]);
        $M10_2_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM10_2-Y"),
            'radio_objective_uuid' => $M10_2->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
        ]);
        $M10_2_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM10_2-N"),
            'radio_objective_uuid' => $M10_2->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
        ]);

        $M11 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM11-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M11",
            'long_name' => "scoresheet.submerged.M11_long_name",
            'description' => "scoresheet.submerged.M11_description",
            'requirements' => "scoresheet.submerged.M11_requirements",
            'updates' => "scoresheet.submerged.M11_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M11.png",
            'priority' => 120,
        ]);
        $M11_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM11_1RO"),
            'mission_uuid' => $M11->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M11_1_desc',
            'priority' => 10
        ]);
        $M11_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM11_1-Y"),
            'radio_objective_uuid' => $M11_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M11_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM11_1-N"),
            'radio_objective_uuid' => $M11_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M11_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM11_2RO"),
            'mission_uuid' => $M11->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M11_2_desc',
            'priority' => 20,
        ]);
        $M11_2_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM11_2-Y"),
            'radio_objective_uuid' => $M11_2->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
            'available_condition' => "'/mission_results/{$M11->uuid}/mission_objective_results/{$M11_1->uuid}/result' == '{$M11_1_yes->uuid}'"
        ]);
        $M11_2_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM11_2-N"),
            'radio_objective_uuid' => $M11_2->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
            'available_condition' => "'/mission_results/{$M11->uuid}/mission_objective_results/{$M11_1->uuid}/result' == '{$M11_1_yes->uuid}'"
        ]);

        $M12 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM12-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M12",
            'long_name' => "scoresheet.submerged.M12_long_name",
            'description' => "scoresheet.submerged.M12_description",
            'requirements' => "scoresheet.submerged.M12_requirements",
            'updates' => "scoresheet.submerged.M12_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M12.png",
            'no_equipment' => true,
            'priority' => 130,
        ]);
        $M12_1 = SpinnerObjective::create([
            'mission_uuid' => $M12->uuid,
            'name' => "",
            'statement' => "scoresheet.submerged.M12_1_desc",
            'min'=> 0,
            'max' => 5,
            'points' => '10',
            'priority' => 10
        ]);

        $M13 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM13-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M13",
            'long_name' => "scoresheet.submerged.M13_long_name",
            'description' => "scoresheet.submerged.M13_description",
            'requirements' => "scoresheet.submerged.M13_requirements",
            'updates' => "scoresheet.submerged.M13_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M13.png",
            'priority' => 140,
        ]);
        $M13_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM13_1RO"),
            'mission_uuid' => $M13->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M13_1_desc',
            'priority' => 10
        ]);
        $M13_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM13_1-Y"),
            'radio_objective_uuid' => $M13_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M13_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM13_1-N"),
            'radio_objective_uuid' => $M13_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M14 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM14-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M14",
            'long_name' => "scoresheet.submerged.M14_long_name",
            'description' => "scoresheet.submerged.M14_description",
            'requirements' => "scoresheet.submerged.M14_requirements",
            'updates' => "scoresheet.submerged.M14_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M14.png",
            'priority' => 150,
        ]);
        $M14_1 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_1RO"),
            'mission_uuid' => $M14->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M14_1_desc',
            'priority' => 10
        ]);
        $M14_1_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_1-Y"),
            'radio_objective_uuid' => $M14_1->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 5,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M14_1_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_1-N"),
            'radio_objective_uuid' => $M14_1->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M14_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_2RO"),
            'mission_uuid' => $M14->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M14_2_desc',
            'priority' => 20,
        ]);
        $M14_2_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_2-Y"),
            'radio_objective_uuid' => $M14_2->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10,
        ]);
        $M14_2_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_2-N"),
            'radio_objective_uuid' => $M14_2->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20,
        ]);
        $M14_3 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_3RO"),
            'mission_uuid' => $M14->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M14_3_desc',
            'priority' => 30
        ]);
        $M14_3_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_3-Y"),
            'radio_objective_uuid' => $M14_3->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M14_3_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_3-N"),
            'radio_objective_uuid' => $M14_3->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M14_4 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_4RO"),
            'mission_uuid' => $M14->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M14_4_desc',
            'priority' => 40
        ]);
        $M14_4_0 = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_4-0"),
            'radio_objective_uuid' => $M14_4->uuid,
            'value' => '0',
            'points' => 00,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M14_4_1 = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_4-1"),
            'radio_objective_uuid' => $M14_4->uuid,
            'value' => '1',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M14_4_2 = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM14_4-2"),
            'radio_objective_uuid' => $M14_4->uuid,
            'value' => '2',
            'points' => 30,
            'bold' => false,
            'color' => "primary",
            'priority' => 30
        ]);

        $M15 = Mission::create([
            'uuid' => Uuid::fromBytes("SubmergedM15-Mis"),
            "score_sheet_uuid" => $submerged->uuid,
            'short_name' => "M15",
            'long_name' => "scoresheet.submerged.M15_long_name",
            'description' => "scoresheet.submerged.M15_description",
            'requirements' => "scoresheet.submerged.M15_requirements",
            'updates' => "scoresheet.submerged.M15_updates",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/M15.png",
            'no_equipment' => true,
            'priority' => 150,
        ]);
        $M15_1 = SpinnerObjective::create([
            'mission_uuid' => $M15->uuid,
            'name' => "",
            'statement' => "scoresheet.submerged.M15_1_desc",
            'min'=> 0,
            'max' => 6,
            'points' => '5',
            'priority' => 10
        ]);
        $M15_2 = RadioObjective::create([
            'uuid' => Uuid::fromBytes("SubmergedM15_2RO"),
            'mission_uuid' => $M15->uuid,
            'name' => "",
            'statement' => 'scoresheet.submerged.M15_2_desc',
            'priority' => 20
        ]);
        $M15_2_yes = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM15_2-Y"),
            'radio_objective_uuid' => $M15_2->uuid,
            'value' => 'scoresheet.general.yes',
            'points' => 20,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M15_2_no = RadioObjectiveOption::create([
            'uuid' => Uuid::fromBytes("SubmergedM15_2-N"),
            'radio_objective_uuid' => $M15_2->uuid,
            'value' => 'scoresheet.general.no',
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);

        $M16 = Mission::create([
            'score_sheet_uuid' => $submerged->uuid,
            'short_name' => 'PT',
            'long_name' => "scoresheet.general.gp_long_name",
            'description' => "",
            'requirements' => "",
            'updates' => "",
            'img' => "https://cdn.flltools.com/missionModels/Submerged/PT.png",
            'priority' => 170
        ]);
        $M16_1 = RadioObjective::create([
            'mission_uuid' => $M16->uuid,
            'name' => "",
            'statement' => "scoresheet.general.gp_desc",
            'priority' => 10
        ]);
        $M16_1_0 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "0",
            'points' => 0,
            'bold' => false,
            'color' => "primary",
            'priority' => 10
        ]);
        $M16_1_1 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "1",
            'points' => 10,
            'bold' => false,
            'color' => "primary",
            'priority' => 20
        ]);
        $M16_1_2 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "2",
            'points' => 15,
            'bold' => false,
            'color' => "primary",
            'priority' => 30
        ]);
        $M16_1_3 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "3",
            'points' => 25,
            'bold' => false,
            'color' => "primary",
            'priority' => 40
        ]);
        $M16_1_4 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "4",
            'points' => 35,
            'bold' => false,
            'color' => "primary",
            'priority' => 50
        ]);
        $M16_1_5 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "5",
            'points' => 50,
            'bold' => false,
            'color' => "primary",
            'priority' => 60
        ]);
        $M16_1_6 = RadioObjectiveOption::create([
            'radio_objective_uuid' => $M16_1->uuid,
            'value' => "6",
            'points' => 50,
            'bold' => false,
            'color' => "primary",
            'priority' => 70
        ]);


    }
}
