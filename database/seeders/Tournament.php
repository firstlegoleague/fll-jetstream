<?php

namespace Database\Seeders;

use App\Models\Schedule\Schedule;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Tournament extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        if(env('APP_ENV', "prod") == "local" || env('APP_ENV', 'prod') == "staging") {
            $tournament = new \App\Models\Tournament\Tournament();
            $tournament->name = "FLL@The Lego House";
            $tournament->score_sheet_uuid = \App\Models\Challenge\ScoreSheet::all()->first()->uuid;
            $tournament->active = true;
            $tournament->location_name = "The Lego House";
            $tournament->city = "Billund";
            $tournament->address = "Ole Kirks Plads 1";
            $tournament->zip_code = "7190";
            $tournament->geolocation = "null";
            $tournament->start_time = "2024-02-10 09:00:00";
            $tournament->end_time = "2024-02-10 16:30:00";
            $tournament->match_length = 7;
            $tournament->jury_length = 35;
            $tournament->simultaneous_matches = 4;
            $tournament->save();

            $schedule = Schedule::create([
                'tournament_uuid' => $tournament->uuid,
                'start_time' => $tournament->start_time,
                'end_time' => $tournament->end_time,
            ]);
        }
    }
}
