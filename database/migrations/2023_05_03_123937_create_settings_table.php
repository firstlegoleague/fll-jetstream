<?php

use App\Models\Tournament\Tournament;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->string("key");
            $table->string('name');
            $table->string('value')->nullable();
            $table->enum("type", ["BOOLEAN", "TEXT", "NUMBER"]);
            $table->string("category")->default("others");
            $table->integer("priority")->nullable();
            $table->string('ability_edit')->nullable();
            $table->string('ability_view')->nullable();
            $table->foreignIdFor(Tournament::class)->nullable();
            $table->timestamps();

            $table->foreign('tournament_uuid')
            ->references('uuid')
            ->on('tournaments')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('settings');
    }
};
