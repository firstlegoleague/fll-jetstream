<?php

use App\Models\Tournament\Tournament;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->unsignedBigInteger('id')->unique()->index();
            $table->foreignIdFor(Tournament::class);
            $table->string('affiliate');
            $table->string('name');
            $table->text('research_desc')->nullable();
            $table->text('robot_desc')->nullable();
            $table->string('yell')->nullable();
            $table->string('website_url')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('snapchat_url')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tournament_uuid')
                ->references('uuid')
                ->on('tournaments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('teams');
    }
};
