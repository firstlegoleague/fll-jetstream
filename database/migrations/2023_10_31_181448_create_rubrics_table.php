<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rubrics', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Tournament\Team::class)->nullable();
            $table->foreignIdFor(\App\Models\Tournament\Tournament::class)->nullable();
            $table->boolean("scanned_i")->default(false);
            $table->boolean("scanned_r")->default(false);
            $table->boolean("scanned_c")->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('team_uuid')
                ->references('uuid')
                ->on('teams')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tournament_uuid')
                ->references('uuid')
                ->on('tournaments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rubrics');
    }
};
