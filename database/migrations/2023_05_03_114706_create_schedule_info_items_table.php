<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schedule_info_items', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->string('type');
            $table->string('text')->nullable();
            $table->boolean('show_end_time');
            $table->enum('category', ['TABLE', 'JURY', 'ALL']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schedule_info_items');
    }
};
