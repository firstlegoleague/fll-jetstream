<?php

use App\Models\Tournament\Tournament;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Tournament::class);
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tournament_uuid')
                ->references('uuid')
                ->on('tournaments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schedules');
    }
};
