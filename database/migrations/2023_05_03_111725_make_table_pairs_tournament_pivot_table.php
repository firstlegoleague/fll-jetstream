<?php

use App\Models\TablePair;
use App\Models\Tournament\Tournament;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('table_pairs_tournaments', function (Blueprint $table) {
            $table->foreignIdFor(TablePair::class);
            $table->foreignIdFor(Tournament::class);
            $table->timestamps();


            $table->foreign('table_pair_uuid')
                ->references('uuid')
                ->on('table_pairs')
                ->onDelete('cascade')
                ->onUpdate('cascade');


            $table->foreign('tournament_uuid')
                ->references('uuid')
                ->on('tournaments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_pairs_tournaments');
    }
};
