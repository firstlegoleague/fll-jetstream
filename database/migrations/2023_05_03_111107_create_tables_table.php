<?php

use App\Models\TablePair;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tables', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(TablePair::class);
            $table->integer('number');
            $table->string('color');
            $table->string('color_code');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('table_pair_uuid')
                ->references('uuid')
                ->on('table_pairs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tables');
    }
};
