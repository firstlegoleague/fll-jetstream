<?php

use App\Models\Challenge\ScoreSheet;
use App\Models\Schedule\Items\ScheduleTableItem;
use App\Models\Tournament\Round;
use App\Models\Tournament\Team;
use App\Models\Table;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('referee_results', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->foreignIdFor(User::class);
            $table->foreignIdFor(Team::class);
            $table->foreignIdFor(ScoreSheet::class);
            $table->foreignIdFor(Round::class);
            $table->foreignIdFor(Table::class)->nullable()->default(null);
            $table->foreignIdFor(ScheduleTableItem::class)->nullable();
            $table->boolean('valid');
            $table->longText('comments')->nullable();
            $table->text('signature')->nullable();
            $table->integer('core_values_score')->nullable();
            $table->integer('score')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('team_uuid')
                ->references('uuid')
                ->on('teams')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('score_sheet_uuid')
                ->references('uuid')
                ->on('score_sheets')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('table_uuid')
                ->references('uuid')
                ->on('tables')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('round_uuid')
                ->references('uuid')
                ->on('rounds')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('schedule_table_item_uuid')
                ->references('uuid')
                ->on('schedule_table_items')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('referee_results');
    }
};
