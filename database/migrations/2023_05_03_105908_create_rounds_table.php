<?php

use App\Models\Tournament\Tournament;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rounds', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Tournament::class);
            $table->string("name");
            $table->boolean("score_hidden")->default(false);
            $table->boolean("is_practice")->default(false);
            $table->integer("priority");
            $table->timestamps();

            $table->foreign('tournament_uuid')
                ->references('uuid')
                ->on('tournaments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rounds');
    }
};
