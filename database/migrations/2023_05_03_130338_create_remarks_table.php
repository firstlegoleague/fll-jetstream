<?php

use App\Models\Tournament\Team;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('remarks', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Team::class);
            $table->foreignIdFor(User::class);
            $table->longText("value");
            $table->enum("category", ["TABLE", "JURY", "COACH", "TEAM", "PARENTS", "OTHERS"]);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('team_uuid')
                ->references('uuid')
                ->on('teams')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('remarks');
    }
};
