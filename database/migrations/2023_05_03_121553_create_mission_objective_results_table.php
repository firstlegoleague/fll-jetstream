<?php

use App\Models\Challenge\Mission\MissionResult;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mission_objective_results', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(MissionResult::class);
            // $table->uuidMorphs('mission_objective');
            $table->string('mission_objective_type');
            $table->uuid('mission_objective_id');
            $table->index(['mission_objective_type', 'mission_objective_id'], 'mission_objective_morph_index');
            $table->string('result')->nullable(); // TODO Function that checks nullable. Make it only nullable when available == false
            $table->boolean('available')->default(true);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('mission_result_uuid')
                ->references('uuid')
                ->on('mission_results')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mission_objective_results');
    }
};
