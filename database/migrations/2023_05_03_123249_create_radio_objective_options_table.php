<?php

use App\Models\Challenge\Mission\Objective\RadioObjective;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('radio_objective_options', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(RadioObjective::class);
            $table->string('value');
            $table->integer('points');
            $table->boolean('bold')->default(false);
            $table->string('color')->nullable();
            $table->integer('priority');
            $table->text('available_condition')->nullable();
            $table->json('available_condition_ast')->nullable();
            $table->timestamps();

            $table->foreign('radio_objective_uuid')
                ->references('uuid')
                ->on('radio_objectives')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('radio_objective_options');
    }
};
