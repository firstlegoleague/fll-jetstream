<?php

use App\Models\Schedule\Schedule;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schedule_items', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Schedule::class);
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->uuidMorphs('item');
            $table->boolean("isCancelled")->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('schedule_uuid')
                ->references('uuid')
                ->on('schedules')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schedule_items');
    }
};
