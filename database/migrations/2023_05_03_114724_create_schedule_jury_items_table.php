<?php

use App\Models\Tournament\Jury;
use App\Models\Tournament\Team;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schedule_jury_items', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Team::class);
            $table->foreignIdFor(Jury::class);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('jury_uuid')
                ->references('uuid')
                ->on('juries')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('team_uuid')
                ->references('uuid')
                ->on('teams')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schedule_jury_items');
    }
};
