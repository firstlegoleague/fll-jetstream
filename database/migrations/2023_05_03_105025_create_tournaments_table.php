<?php

use App\Models\Challenge\ScoreSheet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(ScoreSheet::class);
            $table->string('name');
            $table->boolean('active');
            $table->string("location_name");
            $table->string('address');
            $table->string('city');
            $table->string('zip_code');
            $table->string('geolocation');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('match_length');
            $table->integer('jury_length');
            $table->integer('simultaneous_matches');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tournaments');
    }
};
