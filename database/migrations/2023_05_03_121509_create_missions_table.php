<?php

use App\Models\Challenge\ScoreSheet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(ScoreSheet::class);
            $table->string('short_name');
            $table->string('long_name');      # TODO: Check if JSON is good
            $table->string('description');
            $table->string('requirements')->nullable();
            $table->string('updates')->nullable();
            $table->string('img')->default("/img/fll_horz.png"); # A default image needs to be done
            $table->integer('priority');
            $table->boolean("no_equipment")->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('score_sheet_uuid')
                ->references('uuid')
                ->on('score_sheets')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('missions');
    }
};
