<?php

use App\Models\Tournament\PracticeTable;
use App\Models\Tournament\Team;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('practice_sessions', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(PracticeTable::class);
            $table->foreignIdFor(Team::class);
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->timestamps();

            $table->foreign('practice_table_uuid')
                ->references('uuid')
                ->on('practice_tables')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('team_uuid')
                ->references('uuid')
                ->on('teams')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('practice_sessions');
    }
};
