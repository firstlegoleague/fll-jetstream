<?php

use App\Models\Tournament\Tournament;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('practice_tables', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Tournament::class);
            $table->integer('number');
            $table->string('color');
            $table->string('color_code');
            $table->string('practice_length');
            $table->dateTime('practice_open_time');
            $table->dateTime('practice_close_time');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tournament_uuid')
                ->references('uuid')
                ->on('tournaments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('practice_tables');
    }
};
