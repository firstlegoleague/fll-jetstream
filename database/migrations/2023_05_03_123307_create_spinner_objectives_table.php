<?php

use App\Models\Challenge\Mission\Mission;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('spinner_objectives', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Mission::class);
            $table->string('name');
            $table->string('statement');
            $table->integer('points');
            $table->integer('min');
            $table->integer('max');
            $table->integer('initial_value')->nullable();
            $table->integer('priority')->default(0);
            $table->timestamps();

            $table->foreign('mission_uuid')
                ->references('uuid')
                ->on('missions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('spinner_objectives');
    }
};
