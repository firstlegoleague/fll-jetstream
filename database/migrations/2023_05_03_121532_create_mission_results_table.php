<?php

use App\Models\Challenge\Mission\Mission;
use App\Models\Challenge\RefereeResult;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mission_results', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Mission::class);
            $table->foreignIdFor(RefereeResult::class);
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('mission_uuid')
                ->references('uuid')
                ->on('missions')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('referee_result_uuid')
                ->references('uuid')
                ->on('referee_results')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mission_results');
    }
};
