<?php

use App\Models\Table;
use App\Models\Tournament\Round;
use App\Models\Tournament\Team;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schedule_table_items', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignIdFor(Team::class);
            $table->foreignIdFor(Table::class);
            $table->foreignIdFor(Round::class);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('table_uuid')
                ->references('uuid')
                ->on('tables')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('team_uuid')
                ->references('uuid')
                ->on('teams')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('round_uuid')
                ->references('uuid')
                ->on('rounds')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schedule_table_items');
    }
};
