FROM existenz/webstack:8.2

HEALTHCHECK CMD curl --fail http://localhost:80/healthcheck || exit 1

COPY --chown=php:nginx ./ /www
COPY --chown=php:nginx ./.env.example /www/.env

RUN find /www -type d -exec chmod -R 555 {} \; \
    && find /www -type f -exec chmod -R 444 {} \; \
    && apk -U --no-cache add \
    git \
    nodejs \
    npm \
    screen \
    php82 \
    php82-ctype \
    php82-bcmath \
    php82-dom \
    php82-fileinfo \
    php82-gd \
    php82-iconv \
    php82-json \
    php82-mbstring \
    php82-openssl \
    php82-pdo \
    php82-pdo_mysql \
    php82-phar \
    php82-session \
    php82-simplexml \
    php82-tokenizer \
    bind-tools \
    php82-xml \
    php82-xmlreader \
    php82-xmlwriter \
    php82-zip \
    php82-curl \
    openjdk17-jdk \
    bash

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /www

RUN chmod 777 /www/storage/ -R

#RUN ["ln", "-s", "/usr/bin/php82", "/usr/bin/php"]
RUN ["/usr/bin/composer", "config", "--global", "disable-tls", "true"]
RUN ["/usr/bin/composer", "install"]

## Increase the php memory to 512MB, which is hopefully enough
RUN cp /www/docker/php.ini /etc/php82/conf.d/php.ini

RUN chmod 777 /www/ -R

# Frontend
RUN npm install
RUN npm run build

RUN chown php:nginx -R /www/

## Adding various things to the init that should run on all the websites
#RUN echo "crond -b" >> /init
#RUN echo "cd /www; php artisan queue:work &" >> /init
