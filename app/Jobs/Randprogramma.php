<?php

namespace App\Jobs;

use App\Models\Tournament\Team;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class Randprogramma implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Log::debug("Randprogramma generated started");

        $data = [
            'teams' => [],
        ];

        $teams = Team::query()
            ->whereNull('deleted_at')
            ->get();

        foreach ($teams as $team) {
            $data['teams'][] = [
                'team_id' => $team->id,
                'team_name' => $team->name,
                'team_affiliate' => $team->affiliate,
            ];
        }

        Pdf::loadView('pdf.randprogramma', $data)->save("public/generated/randprogramma.pdf");
        Log::debug("Done with Randprogramma");
    }
}
