<?php

namespace App\Jobs;

use App\Models\Tournament\Jury;
use App\Models\Tournament\Tournament;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Log;

class JudgeListGenerator implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 300;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Log::debug("Starting with the JudgeListGenerator");
        $jurySession = \App\Models\Schedule\Items\ScheduleJuryItem::with(["scheduleItem", "team", "jury"])->get();
        $juries = Jury::all()->sortBy("number");
        $data = [
            'jury' => [],
        ];
        foreach ($juries as $jury){
            $events = $jurySession->where("jury_uuid", $jury->uuid)->sortBy("scheduleItem.start_time");
            foreach ($events as $event){
                $data["jury"][$jury->number][] = [
                    'team_id' => $event->team->id,
                    'team_name' => $event->team->name,
                    'team_id_name' => $event->team->id_name,
                    'start_time' => $event->scheduleItem->start_time,
                    'end_time' => $event->scheduleItem->end_time,
                    'jury_name' => $jury->name,
                ];
            }
        }

        Pdf::loadView('pdf.judgeList', $data)->save("public/generated/judgeList.pdf");
        Log::debug("Done with JudgeListGenerator");
        return;

    }
}
