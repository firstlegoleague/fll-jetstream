<?php

namespace App\Jobs;

use App\Models\Tournament\Tournament;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Picqer\Barcode\BarcodeGeneratorPNG;

class JudgeFormGenerator implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Log::debug("Starting with JudgeFormGenerator");

        $tournament = Tournament::with('teams')->first();

        $data = [
            'teams' => [],
        ];

        foreach ($tournament->teams as $team) {
            $rubric = new \App\Models\Tournament\Rubrics();
            $rubric->team_uuid = $team->uuid;
            $rubric->tournament_uuid = $tournament->uuid;
            $rubric->save();

            $data['teams'][] = [
                'team_id' => str_pad($team->id, 3, "0", STR_PAD_LEFT),
                'team_name' => $team->name,
                'barcodes' => $this->generateBarcodes($team->id),
            ];
        }

        $data['teams'][] = [
            'team_id' => '*',
            'team_name' => '',
            'barcodes' => $this->generateBarcodes('*', $tournament->uuid),
        ];

        Pdf::loadView('pdf.rubrics', $data)->save("public/generated/judgeForms.pdf");

        Log::debug("Done with JudgeFormGenerator");
    }


    public function generateBarcodes($id)
    {
        $generator = new BarcodeGeneratorPNG();
        $barcodes = [];

        $barcodes['innovation'] = base64_encode($generator->getBarcode($id . 'I', $generator::TYPE_CODE_128));
        $barcodes['innovation_str'] = $id . 'I';
        $barcodes['robot'] = base64_encode($generator->getBarcode($id . 'R', $generator::TYPE_CODE_128));
        $barcodes['robot_str'] = $id . 'R';
        $barcodes['cv'] = base64_encode($generator->getBarcode($id . 'F', $generator::TYPE_CODE_128));
        $barcodes['cv_str'] = $id . 'F';

        return $barcodes;
    }
}
