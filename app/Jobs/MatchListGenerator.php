<?php

namespace App\Jobs;

use App\Models\Table;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class MatchListGenerator implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Log::debug("MatchList generated started");
        $tableSession = \App\Models\Schedule\Items\ScheduleTableItem::with(["scheduleItem", "team", "table", "round"])->get();
        $tables = Table::all()->sortBy("number");
        $data = [
            'tables' => [],
        ];

//        dd($tableSession[0]);

        foreach ($tables as $table){
            $events = $tableSession->where("table_uuid", $table->uuid)->sortBy("scheduleItem.start_time");

            foreach ($events as $event){
                $data["tables"][$table->uuid][] = [
                    'team_id' => $event->team->id,
                    'team_name' => $event->team->name,
                    'team_id_name' => $event->team->id_name,
                    'start_time' => $event->scheduleItem->start_time,
                    'end_time' => $event->scheduleItem->end_time,
                    'table_name' => $table->number,
                    'round_name' => $event->round->name,
                ];
            }
        }

        Pdf::loadView('pdf.matchList', $data)->save("public/generated/matchList.pdf");
        Log::debug("Done with MatchListGenerator");
        return;
    }
}
