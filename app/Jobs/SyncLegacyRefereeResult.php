<?php

namespace App\Jobs;

use App\Models\Challenge\RefereeResult;
use App\Models\Tournament\Tournament;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Client\RequestException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;


class SyncLegacyRefereeResult implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 20;

    private string $uuid;

    /**
     * Create a new job instance.
     */
    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Execute the job.
     * @throws RequestException
     */
    public function handle(): void
    {
        Log::info("Starting with Legacy RefereeResult sync");

        if (!config('app.sync_legacy_backend_url')) {
            return;
        }



        $refereeResult = RefereeResult::query()->withTrashed()->findOrFail($this->uuid);

        Log::info("Validated with Legacy RefereeResult sync");

        $response = Http::post(config('app.sync_legacy_backend_url'), [
            'action' => $refereeResult->trashed() ? "DELETE" : "UPDATE", // CREATE / UPDATE, DELETE
            'team_id' => $refereeResult->team->id,
            'uuid' => $refereeResult->uuid,
            'score' => $refereeResult->score,
            'practice' => $refereeResult->round->is_practice,
            'round_hidden' => $refereeResult->round->score_hidden,
            'updated_at' => $refereeResult->updated_at,
            'created_at' => $refereeResult->created_at,
        ]);

        $response->throw();
    }
}
