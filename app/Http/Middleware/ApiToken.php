<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Check if there is an registered API key
        $apitoken = \App\Models\ApiToken::query()
            ->where('uuid', $request->api_token)
            ->first();

//        dd($request->api_token, $apitoken, $request);

        if($apitoken == null){
            return response()->json(["error"=>"Invalid API Token", "code"=>401], 401);
        }

        if($apitoken->disabled == true){
            return response()->json(["error"=>"Your API code has been disabled", "code"=>401], 401);
        }

        return $next($request);
    }
}
