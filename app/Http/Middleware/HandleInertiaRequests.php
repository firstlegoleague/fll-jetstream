<?php

namespace App\Http\Middleware;

use App\Models\Tournament\Tournament;
use Illuminate\Http\Request;
use Inertia\Middleware;
use App\Models\Setting;
use Auth;
use Ramsey\Uuid\Uuid;
use Silber\Bouncer\Bouncer;


class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request): ?string
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request): array
    {

        $snackbar = [];
        if (session()->get('snackbar')) {
            $snackbar = [
                'uuid' => Uuid::uuid4(),
                'message' => session()->get('snackbar'),
                'color' => session()->get('snackbarColor'),
                'timeout' => session()->get('snackbarTimeout'),
            ];
        }

//      If the user is authenticated
        $user = Auth::user();
        if($user != null){
            return array_merge(parent::share($request), [
                //TODO: Implement something that only the settings that the user has access to get send! (Might be more then you expect, read access then)
                'settings' => [
                    'general' => Setting::getCategoryGlobal("General"),
                    'modules' => Setting::getCategoryGlobal("Modules"),
                ],
                'abilities' => $user->getAbilities(),
                'snackbar' => $snackbar,
            ]);
        }

//      If the user isn't authenticated
        return array_merge(parent::share($request), [
            'settings' => [
                'general' => Setting::getCategoryGlobal("General"),
                'modules' => Setting::getCategoryGlobal("Modules"),
            ],
            'snackbar' => $snackbar,
        ]);
    }
}
