<?php

namespace App\Http\Controllers;

use App\Models\Tournament\Remark;
use App\Models\Tournament\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class RemarkController extends Controller
{
    public function index()
    {
        if(!Auth::user()->can('remark.view')){
            abort(401);
        }

        return Inertia::render('Admin/Remarks/Index', [
            'remarks' => Remark::all()->loadMissing("team", "user"),
        ]);
    }


    public function show(Request $request, $uuid){

        $remark = Remark::query()
            ->where('uuid', $uuid)
            ->first();


        if(!Auth::user()->can('remark.view')){
            if(!(Auth::user()->can('remark.view.own') && $remark->user_id == Auth::user()->id)){
                abort(401);
            }
        }

        return Inertia::render('Admin/Remarks/Edit', [
            'remark' => $remark,
            'teams' => Team::all(),
        ]);
    }

    public function update(Request $request, $uuid){

        $remark = Remark::query()
            ->where('uuid', $uuid)
            ->first();

        if(!Auth::user()->can('remark.view')){
            if(!(Auth::user()->can('remark.view.own') && $remark->user_id == Auth::user()->id)){
                abort(401);
            }
        }

        $remark->value = $request->get("value");
        $remark->team_uuid = $request->get("team");
        $remark->category = $request->get("category");
        $remark->save();
    }


    public function store(Request $request){
        if(!Auth::user()->can('remark.create')){
                abort(401);
        }

        $remark = new Remark();
        $remark->user_id = Auth::user()->id;
        $remark->value = $request->get("value");
        $remark->team_uuid = $request->get("team");
        $remark->category = $request->get("category");
        $remark->save();
    }

    public function new(Request $request){
        return Inertia::render('Admin/Remarks/Edit', [
            'remark' => new Remark(),
            'teams' => Team::all(),
        ]);
    }
}
