<?php

namespace App\Http\Controllers;

use App\Models\Schedule\Items\ScheduleJuryItem;
use App\Models\Schedule\Items\ScheduleTableItem;
use App\Models\Schedule\Schedule;
use App\Models\Schedule\ScheduleItem;
use App\Models\Table;
use App\Models\Tournament\Jury;
use App\Models\Tournament\Round;
use App\Models\Tournament\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Str;

class ScheduleController extends Controller
{

    // TODO: @jamie, kan jij hier keer een rewrite van doen. Dit werkt wel, maar kan waarschijnlijk effiencter.
    // Al weet ik niet hoe.

    public function index(){

        $date = Carbon::now()->format('Y-m-d');
        $resources = [];
        $events = [];
        $allResourceIds = [];
        $tableResourceIds = [];
        $juryResourceIds = [];

        $tables = Table::query()
            ->orderBy("number")
            ->get();

        foreach ($tables as $table){
            $slice = null;
            $slice["title"] = "Table ".$table->number;
            $slice["id"] =  "table".$table->number;
            $slice["key"] = $table->number;
            array_push($allResourceIds, "table".$table->number);
            array_push($tableResourceIds, "table".$table->number);
            array_push($resources, $slice);
        }

        $juries = Jury::query()
            ->orderBy("number")
            ->get();

        foreach ($juries as $jury){
            $slice = null;
            $slice["title"]=  $jury->name;
            $slice["id"] =  "judge".$jury->number;
            array_push($allResourceIds, "judge".$jury->number);
            array_push($juryResourceIds, "judge".$jury->number);
            array_push($resources, $slice);
        }

        foreach (\App\Models\Schedule\ScheduleItem::all() as $s_item ){
            $slice = null;

            switch ($s_item->item_type){
                case "App\Models\Schedule\Items\ScheduleInfoItem":
                    $info_item = \App\Models\Schedule\Items\ScheduleInfoItem::all()->where("uuid", $s_item->item_id)->first();

                    $slice["title"] = $info_item->text;
                    $slice["groupId"] = $info_item->uuid;
                    $slice["start"] = $date . "T" . $s_item->start_time;
                    $slice["end"] = $date . "T" . $s_item->end_time;

                    if($info_item->category == "ALL") {
                        $slice["resourceIds"] = $allResourceIds;
                    } else if($info_item->category == "JURY"){
                        $slice["resourceIds"] = $juryResourceIds;
                    } else if($info_item->category == "MATCH"){
                        $slice["resourceIds"] = $tableResourceIds;
                    }
                    break;

                case "App\Models\Schedule\Items\ScheduleJuryItem":
                    $info_item = \App\Models\Schedule\Items\ScheduleJuryItem::all()->where("uuid", $s_item->item_id)->first();
                    $slice["title"] = \App\Models\Tournament\Team::all()->where("uuid", $info_item->team_uuid)->first()->name . " | " . \App\Models\Tournament\Jury::all()->where("uuid", $info_item->jury_uuid)->first()->name;
                    $slice["description"] = $slice["title"];
                    $slice["groupId"] = $info_item->uuid;
                    $slice["start"] = $date . "T" . $s_item->start_time;
                    $slice["end"] = $date . "T" . $s_item->end_time;
                    $slice["resourceId"] = "judge".Jury::where("uuid", $info_item->jury_uuid)->first()->number;
                    break;

                case "App\Models\Schedule\Items\ScheduleTableItem":
                    $info_item = \App\Models\Schedule\Items\ScheduleTableItem::all()->where("uuid", $s_item->item_id)->first();
                    $slice["title"] = \App\Models\Tournament\Team::all()->where("uuid", $info_item->team_uuid)->first()->id_name . " | " . \App\Models\Table::all()->where("uuid", $info_item->table_uuid)->first()->name;
                    $slice["groupId"] = $info_item->uuid;
                    $slice["description"] = $slice["title"];
                    $slice["start"] = $date . "T" . $s_item->start_time;
                    $slice["end"] = $date . "T" . $s_item->end_time;
                    $slice["resourceId"] = "table".Table::where("uuid",$info_item->table_uuid)->first()->number;
                    break;
            }

            array_push($events, $slice);
        }

//        dd($events, $resources);

        return Inertia::render('Admin/schedule/index', [
            'resources' => $resources,
            'events' => $events,
            'teams' => \App\Models\Tournament\Team::all(),
            'juries' => \App\Models\Tournament\Jury::all(),
            'tables' => \App\Models\Table::all(),
            'rounds' => \App\Models\Tournament\Round::all(),
        ]);
    }

    public function store(Request $request){
        $item_type = null;

        if($request->get("item_type") === "info"){
            $item = new \App\Models\Schedule\Items\ScheduleInfoItem();
            $item->type = $request->get("type");
            $item->text = $request->get("text");
            $item->show_end_time = $request->get("show_end_time");
            $item->category = $request->get("category");
            $item->save();

            $item_type = "App\Models\Schedule\Items\ScheduleInfoItem";

        } else if($request->get("item_type") === "judge") {
            $item = new \App\Models\Schedule\Items\ScheduleJuryItem();
            $item->team_uuid = $request->get("team_uuid");
            $item->jury_uuid = $request->get("jury_uuid");
            $item->save();

            $item_type = "App\Models\Schedule\Items\ScheduleJuryItem";

        } else if($request->get("item_type") === "match") {
            $item = new \App\Models\Schedule\Items\ScheduleTableItem();
            $item->team_uuid = $request->get("team_uuid");
            $item->table_uuid = $request->get("table_uuid");
            $item->round_uuid = $request->get("round_uuid");
            $item->save();

            $item_type = "App\Models\Schedule\Items\ScheduleTableItem";
        }

        $schedule = new \App\Models\Schedule\ScheduleItem();
        $schedule->schedule_uuid = \App\Models\Schedule\Schedule::all()->first()->uuid;
        $schedule->start_time = Carbon::createFromTimeString($request->get("start_time"));
        $schedule->end_time = Carbon::createFromTimeString($request->get("end_time"));
        $schedule->item_type = $item_type;
        $schedule->item_id = $item->uuid;
        $schedule->isCancelled = 0;
        $schedule->save();

        return to_route('schedule.index');

    }

    public function headReferee(Request $request){
        $sortedByTime = ScheduleTableItem::with(["scheduleItem", "team", "table", "round"])
            ->get()
            ->sortBy(fn($item) => $item->scheduleItem->start_time ?? '9999-12-31 23:59:59');

        // Group by start time
        $tableSession = $sortedByTime->groupBy(fn($item) => $item->scheduleItem->start_time ?? 'unknown');

        // Get all unique table numbers, sorted
        $tableNumbers = $sortedByTime->pluck('table.number')->unique()->sort()->values();

        // Format data for Vue frontend
        $schedule = [];

        foreach ($tableSession as $startTime => $items) {
            // Sort items within the time group by table number
            $sortedItems = $items->sortBy(fn($item) => $item->table->number ?? PHP_INT_MAX);

            // Prepare row with empty values
            $row = array_fill_keys($tableNumbers->toArray(), "");

            // Fill row with team names
            foreach ($sortedItems as $item) {
                $tableNumber = $item->table->number ?? 'N/A';
                $row[$tableNumber] = $item->team->id_name;
            }

            $schedule[] = [
                'time' => $startTime,
                'tables' => $row
            ];
        }

        return Inertia::render('Admin/schedule/headReferee', [
            'tables' => $tableNumbers,
            'schedule' => $schedule
        ]);
    }
}
