<?php

namespace App\Http\Controllers;

use App\Exports\ResultsExport;
use App\Jobs\ExportTeamTableIndicatorGenerator;
use App\Jobs\JudgeListGenerator;
use App\Jobs\MatchListGenerator;
use App\Jobs\Randprogramma;
use App\Models\Table;
use App\Models\Tournament\Jury;
use App\Models\Tournament\Round;
use App\Models\Tournament\Team;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use App\Jobs\JudgeFormGenerator;

class ExportController extends Controller
{
    //
    public function index(){
        return Inertia::render('Admin/Export/Index', [
            'tournaments' => \App\Models\Tournament\Tournament::all(),
        ]);
    }

    public function generate(){
        ExportTeamTableIndicatorGenerator::dispatch();
        Randprogramma::dispatch();

        JudgeListGenerator::dispatch();
        MatchListGenerator::dispatch();

        JudgeFormGenerator::dispatch();
        return to_route('export.index');
    }

    public function judgelist()
    {
        $jurySession = \App\Models\Schedule\Items\ScheduleJuryItem::with(["scheduleItem", "team", "jury"])->get();
        $juries = Jury::all()->sortBy("number");
        $data = [];
        foreach ($juries as $jury){
            $events = $jurySession->where("jury_uuid", $jury->uuid)->sortBy("scheduleItem.start_time");
            foreach ($events as $event){
                $data[$jury->number][] = [
                    'team_id' => $event->team->id,
                    'team_name' => $event->team->name,
                    'team_id_name' => $event->team->id_name,
                    'start_time' => $event->scheduleItem->start_time,
                    'end_time' => $event->scheduleItem->end_time,
                    'jury_name' => $jury->name,
                ];
            }
        }
        return view('pdf.judgeList', compact("data"));

    }

    public function matchlist()
    {
        $tableSession = \App\Models\Schedule\Items\ScheduleTableItem::with(["scheduleItem", "team", "table", "round"])->get();
        $tables = Table::all()->sortBy("number");
        $data = [];

//        dd($tableSession[0]);

        foreach ($tables as $table){
            $events = $tableSession->where("table_uuid", $table->uuid)->sortBy("scheduleItem.start_time");

            foreach ($events as $event){
                $data[$table->uuid][] = [
                    'team_id' => $event->team->id,
                    'team_name' => $event->team->name,
                    'team_id_name' => $event->team->id_name,
                    'start_time' => $event->scheduleItem->start_time,
                    'end_time' => $event->scheduleItem->end_time,
                    'table_name' => $table->number,
                    'round_name' => $event->round->name,
                ];
            }
        }
        return view('pdf.matchList', compact("data"));

    }

    public function matchlistreferee()
    {
        $tableSession = \App\Models\Schedule\Items\ScheduleTableItem::with(["scheduleItem", "team", "table", "round"])->get();
        $tables = Table::all()->sortBy("number");
        $data = [];

//        dd($tableSession[0]);

        foreach ($tables as $table){
            $events = $tableSession->where("table_uuid", $table->uuid)->sortBy("scheduleItem.start_time");
            $i = 0;
            foreach ($events as $event){

                $data[$table->number][] = [
                    'loop' => $i,
                    'team_id' => $event->team->id,
                    'team_name' => $event->team->name,
                    'team_id_name' => $event->team->id_name,
                    'start_time' => $event->scheduleItem->start_time,
                    'end_time' => $event->scheduleItem->end_time,
                    'table_name' => $table->number,
                    'round_name' => $event->round->name,
                ];
                $i++;
            }
        }

//        dd($data);

        return view('pdf.matchListReferee', compact("data"));

    }

    public function refereeResultExport()
    {
        return Excel::download(new ResultsExport, 'export.xlsx');

    }



}
