<?php

namespace App\Http\Controllers;

use App\Exceptions\DeletionBlockedByRelationException;
use App\Models\Schedule\Schedule;
use App\Models\Tournament\Team;
use App\Models\Tournament\Tournament;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth;

class TournamentsController extends Controller
{
    public function index(Request $request) {

        if(!Auth::user()->can('tournament.view')){
            abort(401);
        }

        return Inertia::render('Admin/Tournament/index',[
            "tournaments" => \App\Models\Tournament\Tournament::all()->loadMissing("scoreSheet"),
            "scoreSheets" => \App\Models\Challenge\ScoreSheet::all(),
        ]);
    }
    public function store(Request $request) {

        if(!Auth::user()->can('tournament.create')){
            abort(401);
        }

        $request->validate([
            'name' => ['required', 'string', 'max:40'],
            'scoreSheet' => ['required', 'string', 'exists:score_sheets,uuid'],
            'locationName' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'city'=> ['required', 'string', 'max:255'],
            'zipCode' => ['required', 'string', 'max:12'],
            'matchLength' => ['required', 'integer'],
            'juryLength' => ['required', 'integer'],
            'simultaneousMatches' =>['required', 'integer'],
            'startTime' => ['required', 'date'],
            'endTime' => ['required', 'date'],
        ]);

        $tournament = Tournament::create([
            'name' => $request->input("name"),
            'score_sheet_uuid' => $request->input('scoreSheet'),
            'active' => $request->input("active"),
            'location_name' => $request->input("locationName"),
            'address' => $request->input("address"),
            'city' => $request->input("city"),
            'zip_code' => $request->input("zipCode"),
            'geolocation' => "",
            'start_time' => $request->input("startTime"),
            'end_time' => $request->input("endTime"),
            'match_length' => $request->input("matchLength"),
            'jury_length' => $request->input("juryLength"),
            'simultaneous_matches' => $request->input("simultaneousMatches"),
        ]);

        $schedule = Schedule::create([
            'tournament_uuid' => $tournament->uuid,
            'start_time' => $tournament->start_time,
            'end_time' => $tournament->end_time,
        ]);

        return to_route('tournaments.index');
    }

    public function update(Request $request, $uuid) {

        if(!Auth::user()->can('tournament.edit')){
            abort(401);
        }

        $request->validate([
            'name' => ['required', 'string', 'max:40'],
            'scoreSheet' => ['required', 'string', 'exists:score_sheets,uuid'],
            'locationName' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'city'=> ['required', 'string', 'max:255'],
            'zipCode' => ['required', 'string', 'max:12'],
            'matchLength' => ['required', 'integer'],
            'juryLength' => ['required', 'integer'],
            'simultaneousMatches' =>['required', 'integer'],
            'startTime' => ['required', 'date'],
            'endTime' => ['required', 'date'],
        ]);

//        $tournament = \App\Models\Tournament\Tournament::all()->where("uuid", $uuid)->first();

        $tournament = Tournament::query()
            ->where('uuid', $uuid)
            ->first();

        $tournament->name = $request->input("name");
        $tournament->score_sheet_uuid = $request->input('scoreSheet');
        $tournament->active = $request->input("active");
        $tournament->location_name = $request->input("locationName");
        $tournament->address = $request->input("address");
        $tournament->city = $request->input("city");
        $tournament->zip_code = $request->input("zipCode");
        $tournament->geolocation = "";
        $tournament->start_time = $request->input("startTime");
        $tournament->end_time = $request->input("endTime");
        $tournament->match_length = $request->input("matchLength");
        $tournament->jury_length = $request->input("juryLength");
        $tournament->simultaneous_matches = $request->input("simultaneousMatches");

        $tournament->save();

        return to_route('tournaments.index');
    }

    public function destroy(Request $request, string $uuid)
    {
        if(!Auth::user()->can('tournament.delete')){
            abort(401);
        }

        $tournament = Tournament::find($uuid);

        if (!$tournament) {
            return redirect()->back()->with([
                'snackbar' => 'This tournament does not exist.'
            ]);
        }

        try {
            $tournament->safeDelete();
        } catch (DeletionBlockedByRelationException $e) {
            return redirect()->back()->with([
                'snackbar' => 'Tournament `' . $tournament->name . '` could not be deleted because it has relational dependencies.',
                'snackbarColor' => 'red-darken-2'
            ]);
        }

        return redirect()->back()->with([
            'snackbar' => 'Tournament `' . $tournament->name . '` has been deleted.',
            'snackbarColor' => 'red-darken-2'
        ]);
    }
}
