<?php

namespace App\Http\Controllers;

use App\Models\Tournament\Tournament;
use Illuminate\Http\Request;
use Auth;

class WizardController extends Controller
{
    //

    static function storeTournament(Request $request){

        if(!Auth::user()->can('tournament.create')){
            abort(401);
        }

        $request->validate([
            'name' => ['required', 'string', 'max:40'],
            'scoreSheet' => ['required', 'string', 'exists:score_sheets,uuid'],
            'locationName' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'city'=> ['required', 'string', 'max:255'],
            'zipCode' => ['required', 'string', 'max:12'],
            'matchLength' => ['required', 'integer'],
            'juryLength' => ['required', 'integer'],
            'simultaneousMatches' =>['required', 'integer'],
            'startTime' => ['required', 'date'],
            'endTime' => ['required', 'date'],
        ]);

        $tournament = Tournament::create([
            'name' => $request->input("name"),
            'score_sheet_uuid' => $request->input('scoreSheet'),
            'active' => $request->input("active"),
            'location_name' => $request->input("locationName"),
            'address' => $request->input("address"),
            'city' => $request->input("city"),
            'zip_code' => $request->input("zipCode"),
            'geolocation' => "",
            'start_time' => $request->input("startTime"),
            'end_time' => $request->input("endTime"),
            'match_length' => $request->input("matchLength"),
            'jury_length' => $request->input("juryLength"),
            'simultaneous_matches' => $request->input("simultaneousMatches"),
        ]);

        return response("", 200);
    }
}
