<?php

namespace App\Http\Controllers;

use App\Exceptions\DeletionBlockedByRelationException;
use App\Models\Table;
use App\Models\TablePair;
use BaconQrCode\Common\ReedSolomonCodec;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth;

class TableController extends Controller
{

    public function index()
    {
        if(!Auth::user()->can('table.view')){
            abort(401);
        }

        return Inertia::render('Admin/Tables/Index', [
            'tables' => \App\Models\Table::with("TablePair")->get(),
        ]);
    }

    function store(Request $request) {
        // TODO Might want to give tables more flexibility:
        // TODO Allow for table priority for the ordering and then a name for how it is presented to the users
        // TODO Change tablepair number to priority because it only serves for ordering

        // TODO Link tablepair to tournaments

        if(!Auth::user()->can('table.create')){
            abort(401);
        }

        $request->validate([
            't1_number' => ['required', 'numeric'],
            't1_color' => ['required', 'string'],
            't1_color_code' => ['required', 'string'],
            't2_number' => ['required', 'numeric'],
            't2_color' => ['required', 'string'],
            't2_color_code' => ['required', 'string'],
        ]);

        // TODO Fix changeability of table pair numbers
        $tablePair = \App\Models\TablePair::create([
            'number' => \App\Models\TablePair::all()->max('number') + 1,
        ]);

        $tablePair->save();

        $table1 = Table::create([
            'number' => $request->input('t1_number'),
            'color' => $request->input('t1_color'),
            'color_code' => $request->input('t1_color_code'),
            'table_pair_uuid' => $tablePair->uuid,
        ]);

        $table1->save();

        $table2 = Table::create([
            'number' => $request->input('t2_number'),
            'color' => $request->input('t2_color'),
            'color_code' => $request->input('t2_color_code'),
            'table_pair_uuid' => $tablePair->uuid,
        ]);

        $table2->save();

        return to_route('tables.index');
    }

    public function update(Request $request, $uuid){

        if(!Auth::user()->can('table.edit')){
            abort(401);
        }

        $request->validate([
            'number' => ['required', 'numeric'],
            'color' => ['required', 'string'],
            'color_code' => ['required', 'string'],
        ]);

        $table = Table::find($uuid);
        $table->number = $request->input("number");
        $table->color = $request->input("color");
        $table->color_code = $request->input("color_code");

        $table->save();

        return redirect()->back()->with([
            'snackbar' => 'Table `' . $table->number . '` has been updated.',
            'snackbarColor' => 'success'
        ]);

    }

    public function destroy(Request $request, $uuid){
        if(!Auth::user()->can('table.delete')){
            abort(401);
        }

        $table1 = Table::find($uuid);

        if(!$table1){
            // TODO Translate this error
            return redirect()->back()->with([
                'snackbar' => 'This table does not exist.'
            ]);
        }

        $tablePair = TablePair::find($table1->table_pair_uuid);

        $tables = $tablePair->Tables()->get();

        foreach ($tables as $table){
            if($table->scheduleTableItems()->count() !== 0){
                return redirect()->back()->with([
                    'snackbar' => 'Table `' . $table->number . '` could not be deleted because it has relational dependencies.',
                    'snackbarColor' => 'red-darken-2'
                ]);
            }
        }

        foreach ($tables as $table){
            try {
                $table->safeDelete();
            } catch (DeletionBlockedByRelationException $e) {
                return redirect()->back()->with([
                    'snackbar' => 'Table `' . $table->number . '` could not be deleted because it has relational dependencies.',
                    'snackbarColor' => 'red-darken-2'
                ]);
            }
        }

        $tablePair->delete();

        return redirect()->back()->with([
            'snackbar' => 'Tablepair `' . $tablePair->number . '` has been deleted.',
            'snackbarColor' => 'red-darken-2'
        ]);


    }
}
