<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    // Path:        /users/
    // Method:      GET
    // Description: Returns the users as a JSON object
    // Returns
        // 200      Success
        // 400      Bad request
        // 401      Unauthorized request
        // 403      You don't have permissions to do this
    public function getAllUsers(){
        // TODO: Check Authentication
        return User::all();
    }

    // Path:        /users/{id}
    // Method:      GET
    // Description: Returns the single user as a JSON object
    // Parameters
        // id       The ID of the user you want to query
    // Returns
        // 200      Success
        // 400      Bad request
        // 401      Unauthorized request
        // 403      You don't have permissions to do this

    public function getUser($apikey, $id){
        // TODO: Check Authentication
        return User::query()->findOrFail($id);
    }

    // Path:        /users/
    // Method:      POST
    // Description: Creates a user for the FLLTools
    // Parameters:
        // name     The name of the new user
        // email    The email of the new user
        // password The password of the new user (in plain text)
        // roles    An array of the various roles the new user get
    // Returns:
        // 200      Success
        // 400      Bad request
        // 401      Unauthorized request
        // 403      You don't have permissions to do this

    public function createUser(Request $request){
        // TODO: Check Authentication
        $user = User::create([
            'name' => $request->input("name"),
            'email' => $request->input("email"),
            'password' => bcrypt($request->input("password")),
        ]);

        $user->save();

        $user->assign($request->input("roles"));
        $user->save();

        return "{'msg': 'success', 'id':".$user->id."}";
    }

    // Path:        /users/{id}
    // Method:      DELETE
    // Description: Returns the single user as a JSON object
    // Parameters
        // id       The ID of the user you want to query
    // Returns
        // 200      Success
        // 400      Bad request
        // 401      Unauthorized request
        // 403      You don't have permissions to do this

    public function deleteUser($apikey, $id){
        // TODO: Check Authentication
        return User::query()->findOrFail($id)->delete();
    }

    // Path:        /users/{id}/roles
    // Method:      PUT
    // Description: Assign a role to a user
    // Parameters
        // id       The ID of the user you want to query
        // role     The role that is added to the user
    // Returns
        // 200      Success
        // 400      Bad request
        // 401      Unauthorized request
        // 403      You don't have permissions to do this

    public function addRoleToUser($apikey, $id, Request $request){
        $user = User::query()->findOrFail($id);
        $user->assign($request->input("roles"));
        $user->save();
        return "{'msg': 'success', 'user':".$user."}";
    }

    // Path:        /users/{id}/roles
    // Method:      DELETE
    // Description: Removes a role from a user
    // Parameters
        // id       The ID of the user you want to query
        // role     The role that is removed from the user
    // Returns
        // 200      Success
        // 400      Bad request
        // 401      Unauthorized request
        // 403      You don't have permissions to do this

    public function removeRoleToUser($apikey, $id, Request $request){
        $user = User::query()->findOrFail($id);
        $user->retract($request->input("roles"));
        $user->save();
        return "{'msg': 'success', 'user':".$user."}";
    }

}
