<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Tournament\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TournamentsController extends Controller
{
    //
    static public function createTournament(Request $request, $apikey){

        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();

        if($user->can('tournament.create')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'scoresheet_uuid' => ['required', 'uuid', 'exists:scoresheet,uuid'],
            'active' => ['required', 'boolean'],
            'location_name' => ['required', 'string'],
            'address' => ['required', 'string'],
            'city' => ['required', 'string'],
            'zip_code' => ['required', 'string'],
            'geolocation' => ['required', 'string'],
            'start_time' => ['required'],
            'end_time' => ['required'],
            'match_length' => ['required', 'integer'],
            'jury_length' => ['required', 'integer'],
            'simultaneous_matches' => ['required', 'integer']
        ]);

        if ($validator->fails()) {
            return response()->json( $validator->errors(), 400);
        }

        $tournament = Tournament::create([
           'scoresheet_uuid' => $request->input('scoresheet_uuid'),
           'name' => $request->input('name'),
           'active' => $request->input('active'),
           'location_name' => $request->input('location_name'),
           'address' => $request->input('address'),
           'city' => $request->input('city'),
           'zip_code' => $request->input('zip_code'),
           'geolocation' => $request->input('geolocation'),
           'start_time' => $request->input('start_time'),
           'end_time' => $request->input('end_time'),
           'match_length' => $request->input('match_length'),
           'jury_length' => $request->input('jury_length'),
           'simultaneous_matches' => $request->input('simultaneous_matches'),
        ]);

        return response()->json(["msg"=>"succes", "code"=>200, "tournament_uuid"=>$tournament->uuid], 200);

    }

    static public function getTournament(Request $request, $apikey, $tournament_uuid){
        // TODO: Check abilities
        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();
        if($user->can('tournament.view')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }
        return \App\Models\Tournament\Tournament::all()->where("uuid",$tournament_uuid)->first();

    }

    static public function editTournament(Request $request, $apikey, $tournament_uuid){
        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();
        if($user->can('tournament.edit')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }

        $validator = Validator::make($request->all(), [
            'uuid' => ['required', 'numeric', 'exists:tournaments,id'],
            'name' => ['required', 'string'],
            'scoresheet_uuid' => ['required', 'uuid', 'exists:scoresheet,uuid'],
            'active' => ['required', 'boolean'],
            'location_name' => ['required', 'string'],
            'address' => ['required', 'string'],
            'city' => ['required', 'string'],
            'zip_code' => ['required', 'string'],
            'geolocation' => ['required', 'string'],
            'start_time' => ['required'],
            'end_time' => ['required'],
            'match_length' => ['required', 'integer'],
            'jury_length' => ['required', 'integer'],
            'simultaneous_matches' => ['required', 'integer']
        ]);

        if ($validator->fails()) {
            return response()->json( $validator->errors(), 400);
        }

        $tournament = \App\Models\Tournament\Tournament::all()->where('uuid', $tournament_uuid)->first();

        // TODO: Actually save the info.

    }

    static public function deleteTournament(Request $request, $apikey, $tournament_uuid){

        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();
        if($user->can('tournament.delete')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }

        $tournament = \App\Models\Tournament\Tournament::all()->where("uuid", $tournament_uuid)->first();

        if($tournament == null){
            return response()->json(["msg"=>"This tournament doesn't exsist", "code"=>400], 400);
        }

        $tournament->delete();

        return response()->json(["msg"=>"succes", "code"=>200, "tournament_uuid"=>$tournament->uuid], 200);



    }
}
