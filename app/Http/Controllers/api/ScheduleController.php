<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    // Path:        /schedule/
    // Method:      GET
    // Description: Returns the schedule as a JSON object
    // Returns
    // 200      Success
    // 400      Bad request
    // 401      Unauthorized request
    // 403      You don't have permissions to do this
    public function getSchedule(){

    }


    // Path:        /schedule/upcoming
    // Method:      GET
    // Description: Returns the schedule as a JSON object
    // Parameters:
    // page     The page with the results
    // amount   The amount of events returned per request. Default 15, max 50.
    // Returns:
    // 200      Success
    // 400      Bad request
    // 401      Unauthorized request
    // 40?      Malformed request
    public function scheduleUpcoming(){

    }
}
