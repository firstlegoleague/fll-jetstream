<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LocaleController extends Controller
{
    public function setLocale(Request $request)
    {
        // TODO Add locale validation
        $request->validate([
            'language' => ['required', 'in:en,nl,be,it,enza,fr'],
        ]);
        $user = $request->user();
        if ($user) {
            $user->locale = $request->input('language');
            $user->save();
        }
        return new \Illuminate\Http\JsonResponse([
            'status' => 'OK'
        ], 200);
    }
}
