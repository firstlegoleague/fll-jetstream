<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Tournament\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TeamsController extends Controller
{
    static public function listTeams(Request $request, $apikey){
        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();
        if($user->can('team.view')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }
        return \App\Models\Tournament\Team::all()->whereNull("deleted_at");
    }

    static public function createTeam(Request $request, $apikey){
        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();
        if($user->can('team.create')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }

        // Validator, since request will redirect to a normal webpage, and not give a API return
        $validator = Validator::make($request->all(), [
            'id' => ['required', 'numeric', 'unique:teams,id'],
            'name' => ['required', 'string'],
            'tournament_uuid' => ['required', 'uuid', 'exists:tournaments,uuid'],
            'affiliate' => ['required', 'string'],
        ]);

        // If it fails, return a json object
        if ($validator->fails()) {
            return response()->json( $validator->errors(), 400);
        }

        $team = Team::create([
            'id' => $request->input("id"),
            'tournament_uuid' => $request->input("tournament_uuid"),
            'name' => $request->input("name"),
            'affiliate' => $request->input("affiliate"),
        ]);

        // Return the newly created team UUID
        return response()->json(["msg"=>"succes", "code"=>200, "team_uuid"=>$team->uuid], 200);
    }

    static public function getTeam(Request $request, $apikey){
        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();
        if($user->can('team.view')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }

        return \App\Models\Tournament\Team::all()->where("uuid", $request->team_uuid)->first();
    }

    static public function editTeam(Request $request, $apikey){

        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();
        if($user->can('team.edit')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }

        // Validator, since request will redirect to a normal webpage, and not give a API return
        $validator = Validator::make($request->all(), [
            'id' => ['required', 'numeric', 'unique:teams,id'],
            'name' => ['required', 'string'],
            'tournament_uuid' => ['required', 'uuid', 'exists:tournaments,uuid'],
            'affiliate' => ['required', 'string'],
        ]);

        // If it fails, return a json object
        if ($validator->fails()) {
            return response()->json( $validator->errors(), 400);
        }

        // TODO: Change, something we can do a nice way?
        $team = \App\Models\Tournament\Team::all()->where("uuid", $request->team_uuid)->first();

        // TODO: Edit the team

        return response()->json(["msg"=>"succes", "code"=>200, "team_uuid"=>$team->uuid], 200);
    }

    static public function deleteTeam(Request $request, $apikey){
        $user = \App\Models\ApiToken::all()->where("uuid", $apikey)->first()->getUser();
        if($user->can('team.delete')){
            return response()->json(["msg"=>"Not permissions", "code"=>401], 401);
        }

        $team = \App\Models\Tournament\Team::all()->where("uuid", $request->team_uuid)->first();

        if($team == null){
            return response()->json(["msg"=>"This team doesn't exsist", "code"=>400], 400);
        }

        $team->delete();

        return response()->json(["msg"=>"succes", "code"=>200, "team_uuid"=>$team->uuid], 200);

    }


}
