<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TeapotController extends Controller
{
    static public function teapot(){
        return response()->json(["msg"=>"I'm a teapot! But Thijs might prefer Ice Tea!", "code"=>418], 418);
    }
}
