<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class authentikController extends Controller
{
    public function login(){
        return Socialite::driver('authentik')->redirect();
    }

    public function callback(Request $request){
        $userAuthentik = Socialite::driver('authentik')->user();

        $user = User::query()
            ->where('email', $userAuthentik->email)->first();

        if($user == null){
            $newUser = new User();
            $newUser->name = $userAuthentik->name;
            $newUser->email = $userAuthentik->email;
            $newUser->password = Hash::make($userAuthentik->sub);
            $newUser->save();
            $user = $newUser;
        }

        $hasRole = 0;
        if(in_array("FLLTools_admin", $userAuthentik->groups)){
            $user->assign("admin");
            $hasRole = 1;
        }

        if(in_array("FLLTools_secretariat", $userAuthentik->groups)){
            $user->assign("secretariat");
            $hasRole = 1;
        }

        if(in_array("FLLTools_headReferee", $userAuthentik->groups)){
            $user->assign("headReferee");
            $hasRole = 1;
        }

        if(in_array("FLLTools_referee", $userAuthentik->groups)){
            $user->assign("referee");
            $hasRole = 1;
        }

        if(in_array("FLLTools_headJudge", $userAuthentik->groups)){
            $user->assign("headJudge");
            $hasRole = 1;
        }

        if(in_array("FLLTools_judge", $userAuthentik->groups)){
            $user->assign("judge");
            $hasRole = 1;
        }

        if(in_array("FLLTools_coach", $userAuthentik->groups)){
            $user->assign("coach");
            $hasRole = 1;
        }

        if(in_array("FLLTools_rp", $userAuthentik->groups)){
            $hasRole = 1;
        }

        $user->save();


        Auth::login($user);

        if($hasRole == 0){
            $user->assign("user");
            return redirect('/');
        }

        return redirect('/dashboard');
    }
}
