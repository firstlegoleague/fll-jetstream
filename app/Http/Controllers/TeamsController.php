<?php

namespace App\Http\Controllers;

use App\Exceptions\DeletionBlockedByRelationException;
use App\Imports\TeamsImport;
use App\Models\Tournament\Team;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        if(!Auth::user()->can('team.view')){
            abort(401);
        }

        return Inertia::render('Admin/Teams/index', [
            'teams' => \App\Models\Tournament\Team::all(),
            'tournaments' => \App\Models\Tournament\Tournament::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if(!Auth::user()->can('team.create')){
            abort(401);
        }

        $request->validate([
            'team_id' => ['required', 'numeric', 'unique:teams,id'],
            'name' => ['required', 'string'],
            'tournament' => ['required', 'uuid', 'exists:tournaments,uuid'],
            'affiliate' => ['required', 'string'],
        ]);

        $info = $request->all();

        $team = new \App\Models\Tournament\Team();

        $team->id = $info['team_id'];
        $team->name = $info['name'];
        $team->affiliate = $info['affiliate'];

        if($request->filled('tournament')) {
            $team->tournament_uuid = $request->input('tournament');
        }

        // TODO Implement advanced team management

        if($request->filled('yell')) {
            $team->yell = $request->input('yell');
        }

        if($request->filled('robot_desc')) {
            $team->robot_desc = $request->input('robot_desc');
        }

        if($request->filled('research_desc')) {
            $team->research_desc = $request->input('research_desc');
        }

        if($request->filled('research_desc')) {
            $team->research_desc = $request->input('research_desc');
        }

        if($request->filled('website_url')) {
            $team->website_url = $request->input('website_url');
        }

        if($request->filled('facebook_url')) {
            $team->facebook_url = $request->input('facebook_url');
        }

        if($request->filled('instagram_url')) {
            $team->instagram_url = $request->input('instagram_url');
        }

        if($request->filled('twitter_url')) {
            $team->twitter_url = $request->input('twitter_url');
        }

        if($request->filled('snapchat_url')) {
            $team->snapchat_url = $request->input('snapchat_url');
        }

        $team->save();

        return to_route('teams.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Team $team, $team_uuid)
    {

        if(!Auth::user()->can('team.view')){
            abort(401);
        }

        $team = Team::query()
            ->where('uuid', $team_uuid)
            ->first();

        return Inertia::render('Admin/Teams/advIndex', [
            'team' => $team,
            'tournaments' => \App\Models\Tournament\Tournament::all(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $uuid)
    {
        if(!Auth::user()->can('team.edit')){
            abort(401);
        }

        $request->validate([
            'team_id' => ['required', 'numeric', 'exists:teams,id'],
            'name' => ['required', 'string'],
            'tournament' => ['required', 'uuid', 'exists:tournaments,uuid'],
            'affiliate' => ['required', 'string'],
        ]);

        $team = Team::query()
            ->where('uuid', $uuid)
            ->first();

        if($request->filled('team_id')) {
            $team->id = $request->input('team_id');
        }

        if($request->filled('name')) {
            $team->name = $request->input('name');
        }

        if($request->filled('affiliate')) {
            $team->affiliate = $request->input('affiliate');
        }

        if($request->filled('tournament')) {
            $team->tournament_uuid = $request->input('tournament');
        }

        // TODO Implement advanced team management

        if($request->filled('yell')) {
            $team->yell = $request->input('yell');
        }

        if($request->filled('robot_desc')) {
            $team->robot_desc = $request->input('robot_desc');
        }

        if($request->filled('research_desc')) {
            $team->research_desc = $request->input('research_desc');
        }

        if($request->filled('research_desc')) {
            $team->research_desc = $request->input('research_desc');
        }

        if($request->filled('website_url')) {
            $team->website_url = $request->input('website_url');
        }

        if($request->filled('facebook_url')) {
            $team->facebook_url = $request->input('facebook_url');
        }

        if($request->filled('instagram_url')) {
            $team->instagram_url = $request->input('instagram_url');
        }

        if($request->filled('twitter_url')) {
            $team->twitter_url = $request->input('twitter_url');
        }

        if($request->filled('snapchat_url')) {
            $team->snapchat_url = $request->input('snapchat_url');
        }

        $team->save();

        return;
    }


    public function uploadList(Request $request){

//        if(!Auth::user()->can('team.create')){
//            abort(401);
//        }

        $request->validate([
            'tournament' => ['required', 'uuid', 'exists:tournaments,uuid'],
        ]);

        Excel::import(new TeamsImport, $request->file->store());

        return to_route("teams.index");

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, string $uuid)
    {
        if(!Auth::user()->can('team.delete')){
            abort(401);
        }

        $team = Team::find($uuid);

        if (!$team) {
            return redirect()->back()->with([
                'snackbar' => 'This team does not exist.'
            ]);
        }

//        Delete all the schedule items. The long way round...
        $table = \App\Models\Schedule\Items\ScheduleTableItem::query()
            ->where("team_uuid", $uuid)
            ->get();

        foreach ($table as $item) {
            $scheduleItem = $item->scheduleItem;
            $scheduleItem->delete();
            $item->delete();
        }

        $jury = \App\Models\Schedule\Items\ScheduleJuryItem::query()
            ->where("team_uuid", $uuid)
            ->get();

        foreach ($jury as $item) {
            $scheduleItem = $item->scheduleItem;
            $scheduleItem->delete();
            $item->delete();
        }


        try {
            $team->safeDelete();
        } catch (DeletionBlockedByRelationException $e) {
            return redirect()->back()->with([
                'snackbar' => 'Team `' . $team->name . '` could not be deleted because it has relational dependencies.',
                'snackbarColor' => 'red-darken-2'
            ]);
        }

        return redirect()->back()->with([
            'snackbar' => 'Team `' . $team->name . '` has been deleted.',
            'snackbarColor' => 'red-darken-2'
        ]);
    }
}
