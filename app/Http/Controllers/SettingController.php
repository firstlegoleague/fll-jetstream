<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SettingController extends Controller
{
    //

    public function store(Request $request){

        if(!Auth::user()->can('setting.edit')){
            abort(401);
        }

        $requestSettings = $request->all()['settings'];
        $allSettings = \App\Models\Setting::all();

        foreach ($allSettings as $setting){
            if(key_exists($setting->uuid, $requestSettings)){
                $setting->value = $requestSettings[$setting->uuid];
                $setting->save();
            }
        }
    }
}
