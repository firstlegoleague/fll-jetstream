<?php

namespace App\Http\Controllers;

use App\Models\Tournament\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Barryvdh\DomPDF\Facade\Pdf;

class RubricsController extends Controller
{
    //

    public function index(Request $request, $uuid){
        $file = public_path()."/generated/judgeForms.pdf";
//        return Storage::download("/public/generated/judgeForms.pdf");
        return Redirect::to("/generated/judgeForms.pdf");
    }
//    public function index(Request $request, $uuid)
//    {
//        $tournament = Tournament::with('teams')->where("uuid", $uuid)->first();
//
//        $data = [
//            'teams' => [],
//        ];
//
////        $teams = \App\Models\Tournament\Team::all();
//
//        foreach ($tournament->teams as $team) {
//            $rubric = new \App\Models\Tournament\Rubrics();
//            $rubric->team_uuid = $team->uuid;
//            $rubric->tournament_uuid = $tournament->uuid;
//            $rubric->save();
//
//            $data['teams'][] = [
//                'team_id' => $team->id,
//                'team_name' => $team->name,
//                'barcodes' => $this->generateBarcodes($rubric->id),
//            ];
//        }
//
//        $data['teams'][] = [
//            'team_id' => '*',
//            'team_name' => '',
//            'barcodes' => $this->generateBarcodes('*', $tournament->uuid),
//        ];
//
//        return Pdf::loadView('pdf.rubrics', $data)->stream();
//
//        set_time_limit(0);
//        return view('pdf.rubrics', compact("data"));
//
//    }


    public function generateBarcodes($id)
    {
        $generator = new BarcodeGeneratorPNG();
        $barcodes = [];

        $barcodes['innovation'] = base64_encode($generator->getBarcode($id . 'I', $generator::TYPE_CODE_128));
        $barcodes['innovation_str'] = $id . 'I';
        $barcodes['robot'] = base64_encode($generator->getBarcode($id . 'R', $generator::TYPE_CODE_128));
        $barcodes['robot_str'] = $id . 'R';
        $barcodes['cv'] = base64_encode($generator->getBarcode($id . 'C', $generator::TYPE_CODE_128));
        $barcodes['cv_str'] = $id . 'C';

        return $barcodes;
    }
}
