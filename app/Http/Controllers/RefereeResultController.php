<?php

namespace App\Http\Controllers;

use App\Actions\RefereeResult\StoreRefereeResult;
use App\Exports\ResultsExport;
use App\Jobs\SyncLegacyRefereeResult;
use App\Models\Challenge\Mission\Mission;
use App\Models\Challenge\RefereeResult;
use App\Models\Challenge\ScoreSheet;
use App\Models\Table;
use App\Models\Tournament\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class RefereeResultController extends Controller
{

    public function index()
    {
        // TODO Permissions

        $refereeResults = RefereeResult::query()
            ->select([
                'referee_results.uuid as uuid',
                'referee_results.score as score',
                'referee_results.created_at as created_at',
                'teams.id as team_id',
                'teams.name as team_name',
                'rounds.name as round_name',
                'score_sheets.name as score_sheet_name'
            ])
            ->join('teams', 'referee_results.team_uuid', 'teams.uuid')
            ->join('rounds', 'referee_results.round_uuid', 'rounds.uuid')
            ->join('score_sheets', 'referee_results.score_sheet_uuid', 'score_sheets.uuid')
            ->get();

        return Inertia::render('Admin/RefereeResults/Index', [
            'refereeResults' => $refereeResults,
        ]);
    }

    public function create() {
        // TODO Permissions

        return Inertia::render('Admin/RefereeResults/Create',[
            'scoreSheet' => \App\Models\Challenge\ScoreSheet::with(['missions.radioObjectives', 'missions.spinnerObjectives'])->first(),
            'teams' => \App\Models\Tournament\Team::query()->orderBy('id')->get(),
            'tables' => \App\Models\Table::orderBy('number')->get(),
        ]);
    }

    public function store(Request $request) {
        // TODO Permissions

        (new StoreRefereeResult($request))->process();

        return to_route('referee_results.index');
    }

    public function show(Request $request, string $uuid) {
        // TODO Permissions

        $refereeResult = \App\Models\Challenge\RefereeResult::query()
            ->where('uuid', $uuid)
            ->with([
                'team',
                'round',
                'table',
                'scoreSheet.missions.spinnerObjectives',
                'scoreSheet.missions.radioObjectives.radioObjectiveOptions',
                'missionResults.missionObjectiveResults.spinnerObjective',
                'missionResults.missionObjectiveResults.radioObjective.radioObjectiveOptions'
            ])
            ->firstOrFail();

        return Inertia::render('Admin/RefereeResults/Editor', [
            'editorEnabled' => false,
            'refereeResult' => $refereeResult->getClientSideCompatibleDataArray(),
            'teams' => \App\Models\Tournament\Team::query()->orderBy('id')->get(), // TODO tournaments etc
            'tables' => \App\Models\Table::orderBy('number')->get(), // TODO Tournaments etc
        ]);
    }

    public function edit(Request $request, string $uuid) {
        // TODO Permissions

        $refereeResult = \App\Models\Challenge\RefereeResult::query()
            ->where('uuid', $uuid)
            ->with([
                'team',
                'round',
                'table',
                'scoreSheet.missions.spinnerObjectives',
                'scoreSheet.missions.radioObjectives.radioObjectiveOptions',
                'missionResults.missionObjectiveResults.spinnerObjective',
                'missionResults.missionObjectiveResults.radioObjective.radioObjectiveOptions'
            ])
            ->firstOrFail();

        return Inertia::render('Admin/RefereeResults/Editor', [
            'editorEnabled' => true,
            'refereeResult' => $refereeResult->getClientSideCompatibleDataArray(),
            'teams' => \App\Models\Tournament\Team::query()->orderBy('id')->get(), // TODO tournaments etc
            'tables' => \App\Models\Table::orderBy('number')->get(), // TODO Tournaments etc
        ]);
    }

    public function update(Request $request, string $uuid) {
        // TODO Permissions

        return (new StoreRefereeResult($request))->update();
    }

    public function destroy(Request $request, string $uuid) {
        // TODO Permissions

        $result = RefereeResult::query()
            ->where('uuid', $uuid)
            ->firstOrFail();

        $result->delete();

        // TODO Legacy sync
        if (config('app.sync_legacy_backend_url')) {
            SyncLegacyRefereeResult::dispatch($uuid);
        }

        return to_route('referee_results.index');
    }

    public function export(){
        // TODO Permissions
        return "200";
        //return Excel::download(new ResultsExport, 'export.xlsx');
    }
}
