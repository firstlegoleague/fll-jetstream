<?php

namespace App\Http\Controllers;

use App\Exceptions\DeletionBlockedByRelationException;
use App\Models\Tournament\Jury;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class JuryController extends Controller
{
    //
    public function index()
    {
        if(!Auth::user()->can('judge.view')){
            abort(401);
        }

        return Inertia::render('Admin/Juries/Index', [
            'juries' => \App\Models\Tournament\Jury::all(),
            'tournaments' => \App\Models\Tournament\Tournament::all(),
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'number' => ['required'],
            'name' => ['required', 'string'],
            'location' => ['required', 'string'],
            'tournament_uuid' => ['required', 'uuid', 'exists:tournaments,uuid'],
        ]);

        $jury = Jury::create([
            'number' => $request->input('number'),
            'name' => $request->input('name'),
            'location' => $request->input('location'),
            'tournament_uuid' => $request->input('tournament_uuid'),
        ]);

        $jury->save();

        return to_route('jury.index');
    }

    public function update(Request $request, $uuid){
        $request->validate([
            'number' => ['required'],
            'name' => ['required', 'string'],
            'location' => ['required', 'string'],
            'tournament_uuid' => ['required', 'uuid', 'exists:tournaments,uuid'],
        ]);

        $jury = Jury::all()->where("uuid", $uuid)->first();

        $jury->number = $request->input('number');
        $jury->name = $request->input('name');
        $jury->location = $request->input('location');
        $jury->tournament_uuid = $request->input('tournament_uuid');

        $jury->save();

        return to_route('jury.index');
    }



    public function delete(Request $request, $uuid){
        if(!Auth::user()->can('round.delete')){
            abort(401);
        }

        $jury = Jury::findOrFail($uuid);

        if(!$jury){
            return redirect()->back()->with([
                'snackbar' => 'This jury does not exist.'
            ]);
        }

        try {
            $jury->safeDelete();
        } catch (DeletionBlockedByRelationException $e){
            return redirect()->back()->with([
                'snackbar' => 'Jury `' . $jury->name . '` could not be deleted because it has relational dependencies.',
                'snackbarColor' => 'red-darken-2'
            ]);
        }

        return redirect()->back()->with([
            'snackbar' => 'Jury `' . $jury->name . '` has been deleted.',
            'snackbarColor' => 'red-darken-2'
        ]);
    }
}
