<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Inertia\Inertia;
use \App\Models\User;
use App\Models\Tournament\Team;
use Auth;
use Silber\Bouncer\BouncerFacade as Bouncer;

class UserController extends Controller
{
    //
    public function index()
    {

        if(!Auth::user()->can('user.view')){
            abort(401);
        }

        return Inertia::render('Admin/Users/Index', [
            'users' => User::with('roles')->get(),
            'roles' => Role::all(),
        ]);
    }

    public function show($uuid){

        if(!Auth::user()->can('user.view')){
            abort(401);
        }

//        $user = User::all()->where("id", $uuid)->first();

        $user = User::query()
            ->where('id', $uuid)
            ->first();

        return Inertia::render('Admin/Users/Edit', [
            'user' => $user,
            'userRoles' => $user->getRoles(),
            'roles' => Role::all(),
            'teams' => Team::all(),
        ]);

    }

    public function store(Request $request){

        if(!Auth::user()->can('user.create')){
            abort(401);
        }

        // TODO: Password confirm check

        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'unique:users,email'],
            'current_team_id' => [ 'exists:teams,uuid']
        ]);

        $user = \App\Models\User::create([
            'name' => $request->get("name"),
            'email' => $request->get("email"),
            'locale' => $request->get("locale"),
            'password' => bcrypt($request->get("password"))
        ]);

        if(Auth::user()->can('user.assignRole')){
            Bouncer::sync($user)->roles($request->get("roles"));
        }

        $user->save();
        return;
    }

    public function update(Request $request, $uuid){

        if(!Auth::user()->can('user.edit')){
            abort(401);
        }

        // TODO: Password confirm check

        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string'],
        ]);

        $user = User::query()
            ->where('id', $uuid)
            ->first();

        $user->name = $request->get("name");
        $user->email = $request->get("email");
        $user->locale = $request->get("locale");

        if($request->filled('password')) {
            $user->password = bcrypt($request->get("password"));
        }

        if(Auth::user()->can('user.assignRole')){
            Bouncer::sync($user)->roles($request->get("roles"));
        }

        $user->save();

        return;

    }
}
