<?php

namespace App\Http\Controllers;

use App\Models\Challenge\RefereeResult;
use App\Models\Schedule\ScheduleItem;
use App\Models\Tournament\Team;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Contracts\Database\Eloquent\Builder;

class PublicTeamController extends Controller
{
    public function show(Request $request, $id){

//        $team = Team::query()
//            ->where("id", $id)
//            ->get()->first();
//
//        $results = RefereeResult::query()
//            ->where("team_uuid", $team->uuid)
//            ->whereNull("deleted_at")
//            ->with(["team", "referee", "round", "table"])
//            ->get();

        $team = Team::query()
            ->where("id", $id)
            ->with([
                'refereeResults',
                'refereeResults.team',
                'refereeResults.referee',
                'refereeResults.round',
                'refereeResults.table'
            ])
            ->firstOrFail();

        $info = \App\Models\Schedule\Items\ScheduleInfoItem::with(["scheduleItem"])->get();

        // TODO Fix this later, now it is fixed in the front-end
        $table = \App\Models\Schedule\Items\ScheduleTableItem::with(["scheduleItem", "team" => function (Builder $query) use ($id) {
            $query->where("id", "like",  $id);
        }, "table", "round"])->get();

        $jury = \App\Models\Schedule\Items\ScheduleJuryItem::with(["scheduleItem", "team" => function (Builder $query) use ($id) {
            $query->where("id", "like",  $id);
        }, "jury"])->get();


        $schedule = $table->concat($jury)->concat($info);
        $schedule = $schedule->sortBy("scheduleItem.start_time", SORT_NATURAL)->values();

        return  Inertia::render('Public/Team/index',[
            'team' => $team,
            'schedule' => $schedule,
        ]);
    }
}
