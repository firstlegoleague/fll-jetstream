<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class TablePairController extends Controller
{
    function create(Request $request) {



        if(!Auth::user()->can('table.create')){
            abort(401);
        }

        $request->validate([
            'number' => ['required', 'numeric'],
        ]);

        $info = $request->all();

        $tablePair = new \App\Models\TablePair();

        $tablePair->number = $info['number'];

        $tablePair->save();

        // TODO: Implement the tables paired to the tournaments themself

        return to_route('tables.index');
    }
}
