<?php

namespace App\Http\Controllers;

use App\Exceptions\DeletionBlockedByRelationException;
use App\Models\Tournament\Round;
use App\Models\Tournament\Team;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use Auth;
use Silber\Bouncer\Bouncer;

class RoundController extends Controller
{

    public function index()
    {
        if(!Auth::user()->can('round.view')){
            abort(401);
        }

        return Inertia::render('Admin/Rounds/Index', [
            'tournaments' => \App\Models\Tournament\Tournament::all(),
            'rounds' => \App\Models\Tournament\Round::all(),
        ]);
    }

    public function store(Request $request){
        if(!Auth::user()->can('round.create')){
            abort(401);
        }

        $request->validate([
            'name' => ['required', 'string'],
            'is_practice' => ['required', 'boolean'],
            'score_hidden' => ['required', 'boolean'],
            'tournament' => ['required', 'uuid', 'exists:tournaments,uuid'],
            'priority' => ['required', 'numeric'],
        ]);

        $round = Round::create([
            'name' => $request->input('name'),
            'is_practice' => $request->input('is_practice'),
            'score_hidden' => $request->input('score_hidden'),
            'tournament_uuid' => $request->input('tournament'),
            'priority' => $request->input('priority'),
        ]);

        $round->save();

        return to_route('rounds.index');

    }

    function create(Request $request) {

        if(!Auth::user()->can('round.create')){
            abort(401);
        }

        $request->validate([
            'name' => ['required', 'string'],
        ]);

        $info = $request->all();

        $round = new \App\Models\Tournament\Round();

        $round->name = $info['name'];

        // TODO: Check if this is correct, but is hard without the interface present
        $round->score_hidden = $info['score_hidden'];
        $round->is_practice = $info['is_practice'];

        $round->save();

        return to_route('rounds.index');
    }

    function edit(Round $round){

    }

    public function update(Request $request, $uuid){

        if(!Auth::user()->can('round.edit')){
            abort(401);
        }

        $request->validate([
            'name' => ['required', 'string'],
            'is_practice' => ['required', 'boolean'],
            'score_hidden' => ['required', 'boolean'],
            'tournament' => ['required', 'uuid', 'exists:tournaments,uuid'],
            'priority' => ['required', 'numeric'],
        ]);

        $round = Round::findOrFail($uuid);
        $round->name = $request->input("name");
        $round->score_hidden = $request->input("score_hidden");
        $round->is_practice = $request->input("is_practice");
        $round->tournament_uuid = $request->input('tournament');
        $round->priority = $request->input('priority');
        $round->save();

        \App\Events\NewRefereeResult::dispatch("New Result");
        
        return redirect()->back()->with([
            'snackbar' => 'Round `' . $round->name . '` has been updated.',
            'snackbarColor' => 'success'
        ]);
    }

    public function destroy(Request $request, string $uuid){

        if(!Auth::user()->can('round.delete')){
            abort(401);
        }

        $round = Round::findOrFail($uuid);

        if(!$round){
            // TODO Translate this error
            return redirect()->back()->with([
                'snackbar' => 'This round does not exist.'
            ]);
        }

        try {
            $round->safeDelete();
        } catch (DeletionBlockedByRelationException $e) {
            return redirect()->back()->with([
                'snackbar' => 'Round `' . $round->name . '` could not be deleted because it has relational dependencies.',
                'snackbarColor' => 'red-darken-2'
            ]);
        }

        return redirect()->back()->with([
            'snackbar' => 'Round `' . $round->name . '` has been deleted.',
            'snackbarColor' => 'red-darken-2'
        ]);
    }

    public function getRoundForTeam(Request $request, string $uuid) {
        if(!Auth::user()->can('round.view') || !Auth::user()->can('team.view')){
            abort(401);
        }

        $team = Team::findOrFail($uuid);

        // TODO Test this with more teams and results in the DB
        $refereeResults = DB::table('referee_results')
            ->select(['team_uuid', 'round_uuid', DB::raw('COUNT(*) as referee_results_count')])
            ->where('team_uuid', $team->uuid)
            ->whereNull(['deleted_at'])
            ->groupBy(['team_uuid', 'round_uuid']);



        $rounds = Round::where('tournament_uuid', $team->tournament_uuid)
            ->leftJoinSub($refereeResults, 'referee_results_counts', function (JoinClause $join) {
                $join->on('rounds.uuid', '=', 'referee_results_counts.round_uuid');
            })
            ->orderBy('priority')
            ->get()->toArray();

        $rounds = Round::hydrate($rounds);

        return new JsonResponse($rounds, 200);
    }
}
