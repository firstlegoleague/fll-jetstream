<?php

namespace App\Http\Controllers;

use App\Events\EndClock;
use App\Events\StartClock;
use App\Events\ResetClock;
use App\Events\PauseClock;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;

class TimerController extends Controller
{
    public function timer_start(){
        $this->handleClock("start");
    }

    public function timer_pause(){
        $this->handleClock("pause");
    }

    public function timer_reset(){
        $this->handleClock("reset");
    }

    public function timer_end(){
        $this->handleClock("end");
    }

    public function api_start(Request $request, $apikey){
        // TODO: Authentication
        $this->handleClock("start");
        return response('{"code":200,"msg":"Timer started"}', 200)->header('Content-Type', 'application/json');
    }

    public function api_end(Request $request, $apikey){
        // TODO: Authentication
        $this->handleClock("end");
        return response('{"code":200,"msg":"Timer stopped"}', 200)->header('Content-Type', 'application/json');
    }

    public function api_reset(Request $request, $apikey){
        // TODO: Authentication
        $this->handleClock("reset");
        return response('{"code":200,"msg":"Timer reset"}', 200)->header('Content-Type', 'application/json');
    }

    public function button(Request $request, $apikey){
        // TODO: Authentication
        $this->handleClock("toggle");
    }

    public function button_web(Request $request){
        // TODO: Authentication
        $this->handleClock("toggle");
    }

    public function handleClock($command){
        $lock = Cache::lock('global_clock_toggle', 156);

        if ($command == 'start') {
            $lock->forceRelease();
            $lock->get();
            StartClock::dispatch();
        } else if ($command == 'end') {
            $lock->forceRelease();
            EndClock::dispatch();
        } else if ($command == 'toggle') {
            if($lock->get()) {
                StartClock::dispatch();
            } else {
                $lock->forceRelease();
                EndClock::dispatch();
            }
        }

    }
}
