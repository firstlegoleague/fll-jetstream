<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationManagerController extends Controller
{
    public function subscribe(Request $request)
    {
        $request->validate([
            'pushSubscription.endpoint' => ['required', 'string'],
            'pushSubscription.keys.p256dh' => ['required', 'string'],
            'pushSubscription.keys.auth' => ['required', 'string'],
            'encoding' => ['required', 'string'],
        ]);

        $user = $request->user();

        $subscription = $user->updatePushSubscription(
            $request->input('pushSubscription.endpoint'),
            $request->input('pushSubscription.keys.p256dh'),
            $request->input('pushSubscription.keys.auth'),
            $request->input('encoding'),
        );

        return to_route($request->input('route'));
    }

}
