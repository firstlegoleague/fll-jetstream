<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Tournament\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Benchmark;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class ScoreboardController extends Controller
{
    //

    public function index(){
        $query = /** @lang MySQL */
            <<<SQL
SELECT RANK() over place as place,
       teams.id as team_id,
       team_uuid,
       teams.name as team_name,
       max_score,
       sort_key
FROM (
    SELECT referee_results.team_uuid,
           MAX(score) as max_score, -- Select max score
           -- TODO Note the hardcoded 4 below, this limits scores to a max of 9999
           GROUP_CONCAT(LPAD(score, 4, '0') ORDER BY score DESC SEPARATOR '') as sort_key -- Concat the sorted scores as strings
    FROM referee_results
    JOIN rounds ON referee_results.round_uuid = rounds.uuid
    WHERE rounds.is_practice = 0 AND rounds.score_hidden = 0 AND referee_results.deleted_at is NULL -- Select only scores which are not practice and are not hidden. Do not include delted scores
    GROUP BY referee_results.team_uuid -- Group the scores per team
) AS raw_data
JOIN teams ON teams.uuid = team_uuid
WINDOW place AS (ORDER BY sort_key desc); -- Sort by the sort key
SQL;

        $data = DB::select($query);

        return Inertia::render('scoreboard/index', [
            'ranking' => $data,
        ]);
    }

    public function privateScoreboard(){
        $query = /** @lang MySQL */
            <<<SQL
SELECT RANK() over place as place,
       teams.id as team_id,
       team_uuid,
       teams.name as team_name,
       max_score,
       sort_key
FROM (
    SELECT referee_results.team_uuid,
           MAX(score) as max_score, -- Select max score
           -- TODO Note the hardcoded 4 below, this limits scores to a max of 9999
           GROUP_CONCAT(LPAD(score, 4, '0') ORDER BY score DESC SEPARATOR '') as sort_key -- Concat the sorted scores as strings
    FROM referee_results
    JOIN rounds ON referee_results.round_uuid = rounds.uuid
    WHERE rounds.is_practice = 0 AND referee_results.deleted_at is NULL -- Select only scores which are not practice and are not hidden. Do not include delted scores
    GROUP BY referee_results.team_uuid -- Group the scores per team
) AS raw_data
JOIN teams ON teams.uuid = team_uuid
WINDOW place AS (ORDER BY sort_key desc); -- Sort by the sort key
SQL;

        $data = DB::select($query);

//        dd("test", $data);

        return Inertia::render('Admin/Scoreboard/Index', [
            'ranking' => $data,
        ]);
    }

}
