<?php

namespace App\Imports;

use App\Models\Tournament\Team;
use Maatwebsite\Excel\Concerns\ToModel;

class TeamsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        $team = new Team([
            'id' => intval($row[0], 10),
            'name' => $row[1],
            'affiliate' => $row[2],
            'tournament_uuid' => \App\Models\Tournament\Tournament::all()->first()->uuid,
        ]);

        $team->save();
    }
}
