<?php

namespace App\Exports;

use App\Models\Challenge\RefereeResult;
use App\Models\Tournament\Round;
use App\Models\Tournament\Team;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class ResultsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('exports.refereeResults', [
            'teams' => Team::all(),
            'rounds' => Round::all()->where("is_practice", 0)->sortBy("priority"),
        ]);
    }
}
