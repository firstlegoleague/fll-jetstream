<?php

namespace App\Utils\ScoreSheet\ConditionalExpression;

use Illuminate\Support\Facades\Process;

class BooleanExpressionParser
{
    /**
     * Parse a boolean expression into a json ast.
     *
     * @param string $expr The expression to be parsed.
     * @return array JSON AST.
     * @throws \Exception Throws if the expression is invalid.
     */
    public static function parseExpression(string $expr): array
    {
        $arg = escapeshellarg($expr);
        $result = Process::path(__DIR__)->run("node ./resources/parser.js $arg");

        $data = json_decode($result->output(), true);

        if ($data["status"] !== "OK") {
            $dump = print_r($data, true);
            throw new \Exception("Failed to parse expression: $dump");
        }

        return $data["data"];
    }
}
