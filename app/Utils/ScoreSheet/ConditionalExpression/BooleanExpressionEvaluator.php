<?php

namespace App\Utils\ScoreSheet\ConditionalExpression;

use Rs\Json\Pointer;
use Rs\Json\Pointer\InvalidJsonException;
use Rs\Json\Pointer\NonexistentValueReferencedException;
use Rs\Json\Pointer\NonWalkableJsonException;

class BooleanExpressionEvaluator
{
    /**
     * Evaluate the AST against the given context.
     *
     * @param $ast array Abstract Syntax Tree as array. Should be generated from the grammar.js.
     * @param $context string An object with context as json.
     * @return bool The result of the boolean expression (ast) given the context.
     * @throws InvalidJsonException
     * @throws NonWalkableJsonException
     * @throws NonexistentValueReferencedException
     */
    public function evaluate(array $ast, string $context): bool
    {
        $context = new Pointer($context);
        return $this->evaluateNode($ast, $context);
    }

    /**
     * Evaluate a node in the AST.
     *
     * @param $ast array Abstract Syntax Tree as array. Should be generated from the grammar.js.
     * @param $context Pointer An object with context as json.
     * @return bool The result of the boolean expression (ast) given the context.
     * @throws InvalidJsonException
     * @throws NonWalkableJsonException
     * @throws NonexistentValueReferencedException
     */
    private function evaluateNode(array $ast, Pointer $context): bool
    {
        $operator = $ast["operator"];
        $left = $ast["left"];
        $right = $ast["right"];
        return $this->$operator($left, $right, $context);
    }

    private function and($left, $right, $context): bool
    {
        return $this->evaluateNode($left, $context) && $this->evaluateNode($right, $context);
    }

    private function or($left, $right, $context): bool
    {
        return $this->evaluateNode($left, $context) || $this->evaluateNode($right, $context);
    }

    private function equals($selector, $value, $context): bool
    {
        $realValue = $this->lookup($context, $selector);
        return $realValue === $value;
    }

    private function notEquals($selector, $value, $context): bool
    {
        return !$this->equals($selector, $value, $context);
    }

    private function in($value, $selector, $context): bool
    {
        $realValue = $this->lookup($context, $selector);
        return in_array($value, $realValue);
    }

    private function notIn($value, $selector, $context): bool
    {
        return !$this->in($value, $selector, $context);
    }

    private function lookup($context, $selector)
    {
        return $context->get($selector);
    }
}
