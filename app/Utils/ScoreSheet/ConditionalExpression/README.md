## Boolean expression parsing and evaluation for conditional scoresheets

## Conditional scoring
Sometimes in the FLL scoring system, we require the use of conditional scoring. 
This usually applies to bonus points where the amount of points scored in a second objective 
vary based on the outcome of the first objective.

An example, Mission 02 of MASTERPIECE:
- Your theater's red flag is down and the active scene color is:
  - Not Down 
  - Blue 
  - Pink
  - Orange
- Select the color of the other side's flag:
  - Not Down 
  - Blue
  - Pink
  - Orange

In this case, the second objective only scores when the selected color is the same as the first color. 
This would also be solved by adding the conditional to the objective question, making the referee only select it
if the colors are the same, but conditional objectives options are way nicer.

We can now choose to add more objectives to the second one:
- Not Down (conditional: objective#1 !== null and objective#1 !== not down)
- Blue (conditional: objective#1 !== null and objective#1 !== blue), 0 pt
- Blue (conditional: objective#1 !== null and objective#1 === blue), 10 pt
- Pink (conditional: objective#1 !== null and objective#1 !== pink), 0 pt
- Pink (conditional: objective#1 !== null and objective#1 === pink), 10 pt
- Orange (conditional: objective#1 !== null and objective#1 !== orange), 0 pt
- Orange (conditional: objective#1 !== null and objective#1 === orange), 10 pt

This makes sure that the points scoring options are only shown when needed. 
Also, when none of the objectives options are shown, the objective will be hidden. 


## Implementation
We make use of peggyjs for the parsing and evaluation. A grammar is available in grammar.pegjs. 
The grammar should be converted to a new grammar.js when it is changed.

The parser.js is made to be used in conjunction with node for server-side parsing of expressions. 
We store the parsed expressions so that they don't have to be recalculated on every pageload / server load.

The BooleanExpressionParser is a helper for the nodejs/parser.js parsing.

The BooleanExpressionEvaluator accepts a parsed expression and context, and evaluates to true or false.
This is also available client side.

## Context 
```js
const context = {
    mission_results: {
        mission_uuid: {
            mission_objective_results: {
                objective_uuid: {
                    result: "result_uuid"
                }
            }
        }
    }
}
```

Example json_pointer:
```
/mission_results/7aa87572-1673-4242-99cf-898a261cbc7d/mission_objective_results/ff4136c5-dd83-4441-a137-c47e1022b1ce/result
```

### Sources:
- https://github.com/hashicorp/js-bexpr
- https://github.com/peggyjs/peggy
- https://github.com/manuelstofer/json-pointer
- https://github.com/raphaelstolt/php-jsonpointer


## Some more of Jamie's notes
- Currently, conditional results are only available on radio objectives
- When no radio objective options are available, the result defaults to null
- When the results input changes:
  - If the currently selected options is not available anymore, the result is set to:
    - Null if default is turned off
    - The default option if default is turned on and it is available
  - If the currently selected option is still available, the result is not changed
- Recursive conditional objective options are not supported, so a conditional option may not depend on another conditional option 

- Database wise:
  - The conditional expression is stored in the radio_objective_options table for editing and viewing purposes
  - The parsed conditional expression is stored in the radio_objective_options table and used for evaluation
