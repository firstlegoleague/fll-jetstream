const parser = require("./grammar.js");

const expression = process.argv.slice(2)[0];

const result = {
    status: null,
    data: null,
};

try {
    result.data = parser.parse(expression);
    result.status = "OK";
    return 0;
} catch (err) {
    result.data = {
        message: err.message,
        location: err.location.start,
    };
    result.status = "ERROR";
    return 1;
} finally {
    process.stdout.write(JSON.stringify(result));
}
