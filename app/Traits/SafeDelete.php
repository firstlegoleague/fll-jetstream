<?php

namespace App\Traits;

use App\Exceptions\DeletionBlockedByRelationException;

trait SafeDelete
{
    /**
     * Define the relationships to watch.
     */
    abstract public function getSafeRelationships(): array;

    /**
     * Delete the model but throw an exception if any of the relations defined in $safeDeleteRelations still have records.
     *
     * @throws DeletionBlockedByRelationException
     */
    public function safeDelete()
    {
        $hasRelation = false;
        foreach ($this->getSafeRelationships() as $relation) {
            try {
                $count = $this->$relation()->withTrashed()->count();
            } catch (\BadMethodCallException $e) {
                $count = $this->$relation()->count();
            }

            if ($count) {
                $hasRelation = true;
                break;
            }
        }

        if ($hasRelation) {
            throw new DeletionBlockedByRelationException();
        }

        return $this->delete();
    }
}
