<?php

namespace App\Models;

use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory, UuidPrimary;

    protected $guarded = [];

    // TODO: Write function to check the abilities

    protected function value(): Attribute
    {
        // NOTE! Set Type first then value to perform correct casting
        return Attribute::make(
            get: function ($value, $attributes) {
                return match ($attributes['type']) {
                    'BOOLEAN' => (boolean) $value,
                    'NUMBER' => (integer) $value,
                    default => $value,
                };
            },
            set: function ($value, $attributes) {
                return match ($attributes['type']) {
                    'BOOLEAN' => $value ? '1' : '0',
                    default => $value,
                };
            }
        );
    }

    static function getSetting(String $name){
        return \App\Models\Setting::where("name", $name)->first();
    }

    static function getSettingTournament(String $name, String $uuid){
        return \App\Models\Setting::where("name", $name)->where("tournament_uuid", $uuid)->first();
    }

    static function getAllTournament(String $uuid){
        return \App\Models\Setting::all()->where("tournament_uuid", $uuid);
    }

    static function getAllGlobal(){
        return \App\Models\Setting::all()->where("tournament_uuid", null);
    }

    static function getCategoryGlobal(String $cat){
        return \App\Models\Setting::where("tournament_uuid", null)->where("category", $cat)->orderBy('priority', 'asc')->get();
    }
}
