<?php

namespace App\Models;

use App\Models\Tournament\Tournament;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TablePair extends Model
{
    use HasFactory, UuidPrimary;

    public $cacheFor = 3600;

    protected $fillable = ['number'];

    protected static $flushCacheOnUpdate = true;

    public function tables()
    {
        return $this->hasMany(Table::class);
    }

    public function tournament(){
        return $this->belongsTo(Tournament::class);
    }
}
