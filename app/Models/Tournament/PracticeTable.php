<?php

namespace App\Models\Tournament;

use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PracticeTable extends Model
{
    use HasFactory, UuidPrimary;
}
