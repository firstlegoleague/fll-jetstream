<?php

namespace App\Models\Tournament;

use App\Models\Challenge\RefereeResult;
use App\Models\Challenge\ScoreSheet;
use App\Traits\SafeDelete;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Tournament extends Model
{
    use HasFactory, UuidPrimary, SafeDelete;

    // TODO: Event on create, to create the settings for the tourament with default settings



    public $cacheFor = 3600;

    protected static $flushCacheOnUpdate = true;
    protected $guarded = [];

    protected $casts = [
        'practice_round' => 'boolean',
        'last_round_hidden' => 'boolean',
        'start_time' => 'datetime',
        'end_time' => 'datetime',
    ];

    public function teams(){
        return $this->hasMany(Team::class);
    }

    public function scoreSheet(){
        return $this->belongsTo(ScoreSheet::class);
    }

    public function getScoreSheetName(){
        $this->loadMissing("ScoreSheet");
        return $this->scoreSheet->name;
    }

    public function refereeResults(){
        return $this->hasManyThrough(RefereeResult::class, Team::class);
    }

    public function lastRefereeResults(){
        return $this->refereeResults()->orderBy("created_at", "desc")->take(20);
    }

    // TODO Make this smarter / or cache the results
    static function tournamentNameArray(){
        $tournaments = \App\Models\Tournament\Tournament::all();
        $array = [];
        foreach ($tournaments as $tournament){
            array_push($array, $tournament->name);
        }
        return $array;
    }

    public function getSafeRelationships(): array
    {
        // TODO Add other relations
        return ['teams'];
    }

    public function rounds(): HasMany
    {
        return $this->hasMany(Round::class);
    }
}
