<?php

namespace App\Models\Tournament;

use App\Models\Challenge\RefereeResult;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\SafeDelete;

class Round extends Model
{
    use HasFactory, UuidPrimary, SafeDelete;

    public $cacheFor = 3600;

    protected $casts = [
        'is_practice' => 'boolean',
        'score_hidden' => 'boolean',
    ];

    /**
     * Invalidate the cache automatically
     * upon update in the database.
     *
     * @var bool
     */
    protected static $flushCacheOnUpdate = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function refereeResults(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(RefereeResult::class);
    }

    public function getSafeRelationships(): array
    {
        // TODO Add other relations
        return ['refereeResults'];
    }
}
