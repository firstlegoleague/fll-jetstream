<?php

namespace App\Models\Tournament;

use App\Models\Challenge\RefereeResult;
use App\Models\Schedule\Items\ScheduleJuryItem;
use App\Models\Schedule\Items\ScheduleTableItem;
use App\Models\Str;
use App\Models\Tournament\Round;
use App\Models\User;
use App\Traits\SafeDelete;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory, UuidPrimary, SafeDelete;

    /**
     * The time to keep this value in cache
     *
     */
    public $cacheFor = 120;

    /**
     * Invalidate the cache automatically
     * upon update in the database.
     *
     * @var bool
     */
    protected static $flushCacheOnUpdate = true;

    // There are currently no fields that needs to be kept hidden
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $with = ['users'];

    protected $appends = [
        'id_name'
    ];

    /**
     * Get the user that owns the team.
     */
    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withPivot('permission')
            ->as('association')
            ->withTimestamps();
    }

    // TODO: Implement the permission thingies

    protected function getUserId($user)
    {
        $id = null;

        if($user instanceof User) {
            $id = $user->id;
        } else {
            if(is_string($user)) {
                $id = intval($user);
            } else if(is_int($user)) {
                $id = $user;
            }
        }

        return $id;
    }

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    public function refereeResults()
    {
        return $this->hasMany(RefereeResult::class);
    }

    public function practiceSessions()
    {
        return $this->hasMany(PracticeSession::class);
    }

    public function getScoreByRound(Round $round){
        $result = RefereeResult::all()->where("team_uuid", $this->uuid)->where("round_uuid",$round->uuid)->first();
        if($result == null){
            return null;
        }
        return $result->score;
    }

    public function getGPByRound(Round $round){
        $result = RefereeResult::all()->where("team_uuid", $this->uuid)->where("round_uuid",$round->uuid)->first();
        if($result == null){
            return null;
        }
        return $result->core_values_score;
    }

    // Nice piece of code to get a weighted score for the scoreboard
    public function getCumulatedRefereeResultsAttribute()
    {
        $weight = 1;
        $weightScore = 0;
        foreach ($this->sortedRefereeResults as $result) {
            $weightScore += ($result->points * $weight);
            $weight = $weight / 1000;
        }

        return $weightScore;
    }

    public function getDisplayNameAttribute() {
        return "{$this->id} | {$this->name}";
    }

    // Get the highest result that's not a practice nor a hidden round
    public function getMaxPracticePointsAttribute() {
        $results = $this->refereeResults;
        $resultCount = 0;

        if(count($results) > 0) {
            $max = 0;
            foreach ($results as $result) {
                if ($result->round->is_practice && !$result->round->score_hidden) {
                    $resultCount++;
                    $points = $result->points;

                    if($max < $points) $max = $points;
                }
            }
        }
        if ($resultCount > 0) {
            return $max;
        } else {
            return null;
        }
    }

    public function getMaxScore() {
        $results = $this->refereeResults;
        $resultCount = 0;
        $max = 0;

        if(count($results) > 0) {
            foreach ($results as $result) {
                if (!$result->round->is_practice) {
                    $resultCount++;
                    $points = $result->points;

                    if($max < $points) $max = $points;
                }
            }
        }
        if ($resultCount > 0) {
            return $max;
        } else {
            return null;
        }
    }

    public function getSlugAttribute() {
        return Str::slug($this->id . ' ' . $this->name, '-');
    }

    public function getIdNameAttribute(): string
    {
        return str_pad($this->id, 3, "0", STR_PAD_LEFT). " | $this->name";
        //return "$this->id | $this->name";
    }

    public function getRefereeCoreValuesPointsAttribute()
    {
        $points = 0;
        $results = $this->refereeResults;
        foreach ($results as $result) {
            if ($result->core_values_score) {
                $points += $result->core_values_score;
            }
        }
        return $points;
    }

//    public function getTeamPhotoUrl($conversion = '', $seconds = 259200)
//    {
//        if($this->getFirstMedia('team-photo')) {
//            return $this->getFirstMedia('team-photo')->getProcessedUrl($conversion, $seconds);
//        } else return 'https://static.fllnn.tech/img/anonymous-user.jpg';
//    }
//
//    public function getRobotPhotoUrl($conversion = '', $seconds = 259200)
//    {
//        if($this->getFirstMedia('robot-photo')) {
//            return $this->getFirstMedia('robot-photo')->getProcessedUrl($conversion, $seconds);
//        } else return null;
//    }

    public function scheduleJuryItems(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ScheduleJuryItem::class);
    }

    public function scheduleTableItems(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ScheduleTableItem::class);
    }

    public function getSafeRelationships(): array
    {
        // TODO Add other relations
        return ['refereeResults'];
    }
}

