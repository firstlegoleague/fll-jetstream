<?php

namespace App\Models\Tournament;

use App\Models\Schedule\Items\ScheduleJuryItem;
use App\Traits\SafeDelete;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jury extends Model
{
    use HasFactory, UuidPrimary, SafeDelete;

    public $cacheFor = 3600;

    protected $guarded = [];


    public function scheduleJuryItems()
    {
        return $this->hasMany(ScheduleJuryItem::class);
    }

    public function tournament(){
        return $this->belongsTo(Tournament::class);
    }

    public function getSafeRelationships(): array
    {
        // TODO: Implement getSafeRelationships() method.
        // TODO: JAMIE!

        return [];
    }
}
