<?php

namespace App\Models\Schedule\Items;

use App\Models\Schedule\ScheduleItem;
use App\Models\Table;
use App\Models\Tournament\Round;
use App\Models\Tournament\Team;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class ScheduleTableItem extends Model
{
    use HasFactory, UuidPrimary;

    protected $guarded = [];

    public function scheduleItem()
    {
        return $this->morphOne(ScheduleItem::class, 'item');
    }

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

    public function round(): BelongsTo
    {
        return $this->belongsTo(Round::class);
    }

    public function table(): BelongsTo
    {
        return $this->belongsTo(Table::class);
    }
}
