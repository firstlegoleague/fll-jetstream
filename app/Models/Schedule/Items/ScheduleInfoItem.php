<?php

namespace App\Models\Schedule\Items;

use App\Models\Schedule\ScheduleItem;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class ScheduleInfoItem extends Model
{
    use HasFactory, UuidPrimary;

    protected $guarded = [];

    public function scheduleItem(): MorphOne
    {
        return $this->morphOne(ScheduleItem::class, 'item');
    }
}
