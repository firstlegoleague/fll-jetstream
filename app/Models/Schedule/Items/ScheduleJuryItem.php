<?php

namespace App\Models\Schedule\Items;

use App\Models\Schedule\ScheduleItem;
use App\Models\Tournament\Jury;
use App\Models\Tournament\Team;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class ScheduleJuryItem extends Model
{
    use HasFactory, UuidPrimary;

    protected $guarded = [];
    public function scheduleItem(): MorphOne
    {
        return $this->morphOne(ScheduleItem::class, 'item');
    }

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

    public function jury(): BelongsTo
    {
        return $this->belongsTo(Jury::class);
    }
}
