<?php

namespace App\Models\Schedule;

use App\Models\Tournament\Tournament;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory, UuidPrimary;

    public $cacheFor = 3600;
    protected static $flushCacheOnUpdate = true;
    protected $guarded = [];

    public function tournament(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Tournament::class);
    }

    public function scheduleItems(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ScheduleItem::class);
    }


}
