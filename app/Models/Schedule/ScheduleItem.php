<?php

namespace App\Models\Schedule;

use App\Casts\TimeCast;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleItem extends Model
{
    use HasFactory, UuidPrimary, SoftDeletes;

    public $cacheFor = 3600;

    protected static $flushCacheOnUpdate = true;

    protected $guarded = [];

    protected $casts = [
        'start_time' => TimeCast::class,
        'end_time' => TimeCast::class,
    ];

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    public function item(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

}
