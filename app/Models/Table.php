<?php

namespace App\Models;

use App\Models\Schedule\Items\ScheduleTableItem;
use App\Traits\SafeDelete;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    use HasFactory, UuidPrimary, safeDelete;

    protected $fillable = ['number', 'color', 'color_code', 'table_pair_uuid'];

    public $cacheFor = 3600;

    public function scheduleTableItems()
    {
        return $this->hasMany(ScheduleTableItem::class);
    }

    public function tablePair()
    {
        return $this->belongsTo(TablePair::class);
    }

    public function getSafeRelationships(): array
    {
        // TODO Add other relations
        return ['scheduleTableItems'];
    }
}
