<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApiToken extends Model
{
    use HasFactory;

    public function getUser(){
        return \App\Models\User::all()->where("id", $this->user_id)->first();
    }
}
