<?php

namespace App\Models\Challenge;

use App\Models\Challenge\Mission\Mission;
use App\Models\Tournament\Tournament;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScoreSheet extends Model
{
    use HasFactory, UuidPrimary;

    public $cacheFor = 3600;
    protected static $flushCacheOnUpdate = true;

    protected $casts = [
        'valid' => 'boolean',
        'initial_results_enabled' => 'boolean',
    ];

    public function missions() {
        return $this->hasMany(Mission::class);
    }

    public function refereeResults() {
        return $this->hasMany(RefereeResult::class);
    }

    public function tournaments() { //Unused probably
        return $this->hasMany(Tournament::class);
    }
}
