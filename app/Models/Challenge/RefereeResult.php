<?php

namespace App\Models\Challenge;

use App\Models\Challenge\Mission\MissionResult;
use App\Models\Challenge\Mission\Objective\MissionObjectiveResult;
use App\Models\Table;
use App\Models\Tournament\Team;
use App\Models\Tournament\Round;
use App\Models\User;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefereeResult extends Model
{
    use HasFactory, UuidPrimary, SoftDeletes;

    public $cacheFor = 3600;
    protected $touches = ['team'];

    protected $casts = [
        'valid' => 'boolean'
    ];

    protected static $flushCacheOnUpdate = true;

    protected $guarded = [];

    public function referee() {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function team() {
        return $this->belongsTo(Team::class);
    }

    public function table() {
        return $this->belongsTo(Table::class);
    }

    public function round() {
        return $this->belongsTo(Round::class);
    }

    public function scoreSheet() {
        return $this->belongsTo(ScoreSheet::class);
    }

    public function missionResults() {
        return $this->hasMany(MissionResult::class);
    }

    public function getClientSideCompatibleDataArray() {
        $refereeResultArray = $this->toArray();

        $refereeResultArray['points'] = 0;

        $refereeResultArray['mission_results'] = $this->missionResults->mapWithKeys(function (MissionResult $missionResult) {
            $missionResultArray = $missionResult->toArray();
            $missionResultArray['valid'] = true;
            $missionResultArray['mission_objective_results'] = $missionResult->missionObjectiveResults->mapWithKeys(function (MissionObjectiveResult $missionObjectiveResult) {
                $missionObjectiveResult->valid = true;
                $missionObjectiveResultArray = $missionObjectiveResult->toArray();
                $missionObjectiveResultArray['points'] = $missionObjectiveResult->points;
                return [
                    $missionObjectiveResult->mission_objective_id => $missionObjectiveResultArray,
                ];
            });

            return [
                $missionResult->mission_uuid => $missionResultArray
            ];
        });

        return $refereeResultArray;
    }

    public function getPointsAttribute() {
        $points = 0;
        foreach ($this->missionResults as $missionResult) {
            $points += $missionResult->points;
        }

        return $points;
    }

}
