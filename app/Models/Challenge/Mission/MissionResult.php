<?php

namespace App\Models\Challenge\Mission;

use App\Models\Challenge\Mission\Objective\MissionObjectiveResult;
use App\Models\Challenge\RefereeResult;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MissionResult extends Model
{
    use HasFactory, UuidPrimary, SoftDeletes;

    public $cacheFor = 3600;

    /**
     * Invalidate the cache automatically
     * upon update in the database.
     *
     * @var bool
     */
    protected static $flushCacheOnUpdate = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function refereeResult(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(RefereeResult::class);
    }

    public function mission(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Mission::class);
    }

    public function missionObjectiveResults(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(MissionObjectiveResult::class);
    }

    public function getPointsAttribute() {
        $points = 0;
        foreach($this->missionObjectiveResults as $missionObjectiveResult) {
            $points += $missionObjectiveResult->points;
        }
        return $points;
    }

}
