<?php

namespace App\Models\Challenge\Mission\Objective;

use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class BaseObjective extends Model
{
    use HasFactory, UuidPrimary;

    public $cacheFor = 3600;

    /**
     * Invalidate the cache automatically
     * upon update in the database.
     *
     * @var bool
     */
    protected static $flushCacheOnUpdate = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    protected $appends = ['type'];

    protected function type(): Attribute
    {
        return Attribute::make(
            get: fn () => get_class($this),
        );
    }

}
