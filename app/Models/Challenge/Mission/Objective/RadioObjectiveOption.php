<?php

namespace App\Models\Challenge\Mission\Objective;

use App\Traits\UuidPrimary;
use App\Utils\ScoreSheet\ConditionalExpression\BooleanExpressionParser;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RadioObjectiveOption extends Model
{
    use HasFactory, UuidPrimary;

    public $cacheFor = 3600;

    /**
     * Invalidate the cache automatically
     * upon update in the database.
     *
     * @var bool
     */
    protected static $flushCacheOnUpdate = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $casts = [
        'available_condition_ast' => 'array'
    ];

    public function radioObjective() {
        return $this->belongsTo(RadioObjective::class);
    }

    protected function availableCondition(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => $value,
            set: fn (string|null $value) => [
                'available_condition' => $value,
                'available_condition_ast' => $value === null ? null : json_encode(BooleanExpressionParser::parseExpression($value)),
            ],
        );
    }
}
