<?php

namespace App\Models\Challenge\Mission\Objective;

use App\Models\Challenge\Mission\MissionResult;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MissionObjectiveResult extends Model
{
    use HasFactory, UuidPrimary, SoftDeletes;

    public $cacheFor = 3600;

    protected static $flushCacheOnUpdate = true;

    protected $guarded = [];

    protected $with = ['radioObjective', 'spinnerObjective'];

    protected $casts = [
        'available' => 'bool'
    ];

    public function missionResult() {
        return $this->belongsTo(MissionResult::class);
    }


    public function __get($name)
    {
        switch ($name) {
            case 'objective':
                switch($this->mission_objective_type) {
                    case 'App\Models\Challenge\Mission\Objective\RadioObjective':
                        return $this->radioObjective;
                    case 'App\Models\Challenge\Mission\Objective\SpinnerObjective':
                        return $this->spinnerObjective;
                    default:
                        abort(500, 'Unknown mission_objective_type ');
                }

                break;

            default:
                return parent::__get($name);
        }
    }

    protected function result(): Attribute
    {
        return Attribute::make(
            get: function (string|null $value) {
                switch($this->mission_objective_type) {
                    case 'App\Models\Challenge\Mission\Objective\RadioObjective':
                        return $value;
                    case 'App\Models\Challenge\Mission\Objective\SpinnerObjective':
                        return (int) $value;
                    default:
                        abort(500, 'Unknown mission_objective_type ');
                }
            },
        );
    }

    public function radioObjective() {
        return $this->belongsTo(RadioObjective::class, 'mission_objective_id');
    }

    public function spinnerObjective() {
        return $this->belongsTo(SpinnerObjective::class, 'mission_objective_id');
    }

    public function getPointsAttribute()
    {
        if ($this->result === null) {
            return 0;
        }
        switch ($this->mission_objective_type) {
            case 'App\Models\Challenge\Mission\Objective\RadioObjective':
                $option = $this->objective->radioObjectiveOptions->where('uuid', $this->result)->first(); //RadioObjectiveOption::find($this->result);
                if($option === null) {
                    return 0;
                }
                return $option->points;
            case 'App\Models\Challenge\Mission\Objective\SpinnerObjective':
                return $this->objective->points * $this->result;
            default:
                abort(500, 'Unknown mission_objective_type');
        }
    }
}
