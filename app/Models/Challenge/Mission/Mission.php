<?php

namespace App\Models\Challenge\Mission;

use App\Models\Challenge\Mission\Objective\RadioObjective;
use App\Models\Challenge\Mission\Objective\SpinnerObjective;
use App\Models\Challenge\ScoreSheet;
use App\Models\Challenge\Mission\MissionResult;
use App\Traits\UuidPrimary;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Mission extends Model
{
    use HasFactory, UuidPrimary;

    public $cacheFor = 3600;

    protected static $flushCacheOnUpdate = true;

    protected $guarded = [];

    protected $with = ['radioObjectives', 'spinnerObjectives'];

    protected $appends = [
        'objectives'
    ];

    protected function objectives(): Attribute
    {
        return Attribute::make(
            get: function() {
                return (new Collection())
                    ->concat($this->radioObjectives)
                    ->concat($this->spinnerObjectives);
            }
        );
    }

    public function radioObjectives(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(RadioObjective::class);
    }

    public function spinnerObjectives(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(SpinnerObjective::class);
    }

    public function scoreSheet(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ScoreSheet::class);
    }

    public function missionResults(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(MissionResult::class);
    }
}
