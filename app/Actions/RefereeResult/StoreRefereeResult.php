<?php

namespace App\Actions\RefereeResult;

use App\Jobs\SyncLegacyRefereeResult;
use App\Models\Challenge\Mission\Mission;
use App\Models\Challenge\Mission\Objective\RadioObjective;
use App\Models\Challenge\Mission\Objective\SpinnerObjective;
use App\Models\Challenge\RefereeResult;
use App\Models\Challenge\ScoreSheet;
use App\Models\Table;
use App\Models\Tournament\Round;
use App\Models\Tournament\Team;
use App\Models\Tournament\Tournament;
use App\Utils\ScoreSheet\ConditionalExpression\BooleanExpressionEvaluator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Nette\Schema\ValidationException;

class StoreRefereeResult
{
    private Request $request;

    private Team $team;

    private Tournament $tournament;

    private Round $round;

    private Table $table;

    private ScoreSheet $scoreSheet;

    private array $results;

    private string $jsonResults;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function process() {
        $this->validateRefereeResultBase();
        $this->getAndVerifyBaseModels();

        if (RefereeResult::query()
            ->where('uuid', $this->request->input('uuid'))
            ->first() !== null) {
            return response("EXISTS", 201);
        }

        $this->validateAdditionalBase();
        $this->validateMissionResults();
        $this->storeRefereeResult();

        \App\Events\NewRefereeResult::dispatch("New Result");
        $this->syncWithLegacyBackend();

        return response("OK", 201);
    }

    public function update() {
        $this->validateRefereeResultBase();
        $this->getAndVerifyBaseModels();

        if (RefereeResult::query()
                ->where('uuid', $this->request->input('uuid'))
                ->first() === null) {
            // TODO Better response
            return response("Could not find result.", 404);
        }

        $this->validateAdditionalBase();
        $this->validateMissionResults();
        $this->updateRefereeResult();

        \App\Events\NewRefereeResult::dispatch("Updated Result");
        $this->syncWithLegacyBackend();

        return response("OK", 200);
    }

    private function syncWithLegacyBackend() {
        // TODO Legacy sync
        if (config('app.sync_legacy_backend_url')) {
            SyncLegacyRefereeResult::dispatch($this->request->input('uuid'));
        }
    }


    private function validateRefereeResultBase(): void
    {
        $this->request->validate([
            'uuid' => ['required', 'uuid'],
            'score_sheet_uuid' => ['required', 'uuid'],
            'team_uuid' => ['required', 'uuid'],
            'round_uuid' => ['required', 'uuid'],
            'table_uuid' => ['present', 'nullable', 'uuid'],
            'comments' => ['present', 'nullable', 'string'],
            'points' => ['required', 'int'],
            'mission_results' => ['required', 'array'],
            // validate mission results
            'mission_results.*.mission_uuid' => ['required', 'uuid', 'distinct:ignore_case'],
//            'mission_results.*.points' => ['required', 'int'],
            'mission_results.*.mission_objective_results' => ['required', 'array'],
            'mission_results.*.mission_objective_results.*.mission_objective_id' => ['required', 'uuid', 'distinct:ignore_case'],
            'mission_results.*.mission_objective_results.*.mission_objective_type' => ['required', 'string',
                Rule::in([
                    'App\Models\Challenge\Mission\Objective\RadioObjective',
                    'App\Models\Challenge\Mission\Objective\SpinnerObjective'
                ])],
            'mission_results.*.mission_objective_results.*.result' => ['nullable'],
//            'mission_results.*.mission_objective_results.*.points' => ['nullable', 'int'],
            'mission_results.*.mission_objective_results.*.available' => ['required', 'boolean'],
        ]);
    }

    private function getAndVerifyBaseModels(): void
    {
        // Verify team exists
        $this->team = Team::query()
            ->where('uuid', $this->request->input('team_uuid'))
            ->select(['uuid', 'tournament_uuid'])
            ->firstOrFail();

        // Get tournament of team
        $this->tournament = Tournament::query()
            ->where('uuid', $this->team->tournament_uuid)
            ->select(['uuid', 'score_sheet_uuid'])
            ->firstOrFail();

        // Verify round exists and belongs to the right tournament
        $this->round = Round::query()
            ->where('tournament_uuid', $this->tournament->uuid)
            ->where('uuid', $this->request->input('round_uuid'))
            ->firstOrFail(); // TODO Could change to count?

        // TODO No check is being done if there isn't already a score for this team and round, but we may want it that way to allow for double scores to be processed afterwards

        if ($this->request->input('table_uuid') !== null) {
            // If table is entered, verify that it exists and is coupled to the tournament
            $this->table = Table::query()
                ->where('tables.uuid', $this->request->input('table_uuid'))
//                ->join('table_pairs', 'tables.table_pair_uuid', '=', 'table_pairs.uuid')
//                ->join('table_pairs_tournaments', 'table_pairs.uuid', '=', 'table_pairs_tournaments.table_pair_uuid')
//                ->where('table_pairs_tournaments.tournament_uuid', $this->tournament->uuid)
                ->firstOrFail();
        }

        $this->scoreSheet = ScoreSheet::query()
            ->where('uuid', $this->request->input('score_sheet_uuid'))
            ->where('uuid', $this->tournament->score_sheet_uuid)
            ->with(['missions.radioObjectives', 'missions.radioObjectives.radioObjectiveOptions', 'missions.spinnerObjectives'])
            ->firstOrFail();
    }

    private function validateAdditionalBase(): void
    {
        $rules = [];

        if (true) { // TODO Check if gp is required
            $rules['gracious_professionalism'] = ['required', 'int', 'in:0,2,3,4'];
        }

        if (true) { // TODO Check if signature is required
            $rules['signature'] = ['required', 'string'];
        }

        $this->request->validate($rules);
    }

    private function validateMissionResults(): void
    {
        $this->results = $this->request->input('mission_results');
        $this->jsonResults = json_encode([
            'mission_results' => $this->results
        ]);

        foreach ($this->scoreSheet->missions as $mission) {
            $this->validateMissionResult($mission);
        }
    }

    private function validateMissionResult(Mission $mission): void
    {
        $missionValidationKey = 'mission_results.' . $mission->uuid;

        if (!array_key_exists($mission->uuid, $this->results)) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $missionValidationKey => 'Mission result is missing.'
            ]);
        }

        $missionResult = $this->results[$mission->uuid];

        if ($missionResult['mission_uuid'] != $mission->uuid) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $missionValidationKey . '.mission_uuid' => 'Array key / mission_uuid are not the same.'
            ]);
        }

        foreach ($mission->objectives as $objective) {
            $this->validateMissionObjectiveResult($missionResult, $mission, $objective);
        }
    }

    private function validateMissionObjectiveResult($missionResult, $mission, $objective): void
    {
        $objectiveValidationKey = 'mission_results.' . $mission->uuid . '.mission_objective_results.' . $objective->uuid;

        $objectiveResults = $missionResult['mission_objective_results'];

        if (!array_key_exists($objective->uuid, $objectiveResults)) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey => 'Mission objective result is missing.'
            ]);
        }

        $objectiveResult = $objectiveResults[$objective->uuid];

        if ($objectiveResult['mission_objective_id'] != $objective->uuid) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey . '.mission_objective_id' => 'Array key / mission_objective_id are not the same.'
            ]);
        }

        if ($objectiveResult['mission_objective_type'] != get_class($objective)) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey . '.mission_objective_type' => 'Mission objective type does not match objective type in database.'
            ]);
        }

        // TODO Sometime get rid of all these switch statements
        switch(get_class($objective)) {
            case 'App\Models\Challenge\Mission\Objective\RadioObjective':
                $this->validateMissionRadioObjectiveResult($missionResult, $mission, $objective, $objectiveResult, $objectiveValidationKey);
                break;
            case 'App\Models\Challenge\Mission\Objective\SpinnerObjective':
                $this->validateMissionSpinnerObjectiveResult($missionResult, $mission, $objective, $objectiveResult, $objectiveValidationKey);
                break;
            default:
                abort(500, 'Unknown mission_objective_type ');
        }
    }

    private function validateMissionRadioObjectiveResult($missionResult, $mission, RadioObjective $objective, $objectiveResult, $objectiveValidationKey): void
    {
        $res = $objectiveResult['result'];

        $evaluator = new BooleanExpressionEvaluator();

        // Calculate available options
        $options = [];
        foreach ($objective->radioObjectiveOptions as $radioObjectiveOption) {
            if ($radioObjectiveOption->available_condition === null ||
                $radioObjectiveOption->available_condition_ast === null ||
                $evaluator->evaluate($radioObjectiveOption->available_condition_ast, $this->jsonResults)) {
                $options[] = $radioObjectiveOption->uuid;
            }
        }

        $available = count($options) > 0;

        if ($available !== $objectiveResult['available']) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey . '.available' => 'Result available does not match backend calculation.'
            ]);
        }

        if ($available) {
            if (!is_string($res)) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    $objectiveValidationKey . '.result' => 'Result should be a string since it is available.'
                ]);
            }

            if (!in_array($res, $options)) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    $objectiveValidationKey . '.result' => 'Result is not available in options.'
                ]);
            }
        } else if ($res != null) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey . '.result' => 'Result should be null since it is not available.'
            ]);
        }
    }

    private function validateMissionSpinnerObjectiveResult($missionResult, $mission, SpinnerObjective $objective, $objectiveResult, $objectiveValidationKey): void
    {
        if (!$objectiveResult['available']) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey . '.available' => 'Spinnerobjective is always available.'
            ]);
        }

        $res = $objectiveResult['result'];

        if (!is_int($res)) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey . '.result' => 'Result should be an integer.'
            ]);
        }

        if ($res < $objective->min) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey . '.result' => 'Result not be smaller than ' . $objective->min . '.',
            ]);
        }

        if ($res > $objective->max) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $objectiveValidationKey . '.result' => 'Result not be greater than ' . $objective->max . '.',
            ]);
        }
    }

    private function storeRefereeResult(): void
    {
        DB::transaction(function () {
            $refereeResult = RefereeResult::create([
                'uuid' => $this->request->input('uuid'),
                'user_id' => $this->request->user()->id,
                'team_uuid' => $this->request->input('team_uuid'),
                'score_sheet_uuid' => $this->request->input('score_sheet_uuid'),
                'table_uuid' => $this->request->input('table_uuid'),
                'round_uuid' => $this->request->input('round_uuid'),
                'signature' => $this->request->input("signature"),
                'core_values_score' => $this->request->input("gracious_professionalism"),
                'comments' => $this->request->input("comments"),
                'valid' => true,
            ]);

            foreach ($this->scoreSheet->missions as $mission) {
                $this->storeMissionResult($refereeResult, $mission);
            }

            $refereeResult->loadMissing([
                'missionResults.missionObjectiveResults.radioObjective.radioObjectiveOptions',
                'missionResults.missionObjectiveResults.spinnerObjective'
            ]);

            if ($refereeResult->points != $this->request->input('points')) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'result' => 'Calculated score does not match supplied score.'
                ]);
            }

            // TODO Change this
            $refereeResult->score = $refereeResult->points;
            $refereeResult->save();
        });
    }

    private function storeMissionResult(RefereeResult $refereeResult, Mission $mission): void
    {
        $missionResult = $this->results[$mission->uuid];

        $dbMissionResult = $refereeResult->missionResults()->create([
            'mission_uuid' => $mission->uuid,
        ]);

        $objectives = $mission->objectives;

        $objectiveResults = $missionResult['mission_objective_results'];
        foreach ($objectives as $objective) {
            $objectiveResult = $objectiveResults[$objective->uuid];
            $dbMissionResult->missionObjectiveResults()->create([
                'mission_objective_type' => $objectiveResult['mission_objective_type'],
                'mission_objective_id' => $objectiveResult['mission_objective_id'],
                'result' => $objectiveResult['result'],
            ]);
        }
    }

    private function updateRefereeResult(): void
    {
        DB::transaction(function () {
            $refereeResult = RefereeResult::query()
                ->with('missionResults.missionObjectiveResults')
                ->where('uuid', $this->request->input('uuid'))
                ->firstOrFail();

            $refereeResult->update([
                'team_uuid' => $this->request->input('team_uuid'),
                'round_uuid' => $this->request->input('round_uuid'),
                'table_uuid' => $this->request->input('table_uuid'),
                'signature' => $this->request->input("signature"),
                'core_values_score' => $this->request->input("gracious_professionalism"),
                'comments' => $this->request->input("comments"),
                'valid' => true,
            ]);

            foreach ($this->scoreSheet->missions as $mission) {
                $this->updateMissionResult($refereeResult, $mission);
            }

            $refereeResult->loadMissing([
                'missionResults.missionObjectiveResults.radioObjective.radioObjectiveOptions',
                'missionResults.missionObjectiveResults.spinnerObjective'
            ]);

            if ($refereeResult->points != $this->request->input('points')) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'result' => 'Calculated score does not match supplied score.'
                ]);
            }

            // TODO Change this
            $refereeResult->score = $refereeResult->points;
            $refereeResult->save();
        });
    }

    private function updateMissionResult(RefereeResult $refereeResult, Mission $mission): void
    {
        $missionResult = $this->results[$mission->uuid];

        $dbMissionResult = $refereeResult->missionResults
            ->where('uuid', $missionResult['uuid'])
            ->where('mission_uuid', $missionResult['mission_uuid'])
            ->where('mission_uuid', $mission->uuid)
            ->firstOrFail();

        $objectives = $mission->objectives;

        $objectiveResults = $missionResult['mission_objective_results'];
        foreach ($objectives as $objective) {
            $objectiveResult = $objectiveResults[$objective->uuid];

            $dbObjectiveResult = $dbMissionResult->missionObjectiveResults
                ->where('uuid', $objectiveResult['uuid'])
                ->where('mission_objective_type', $objectiveResult['mission_objective_type'])
                ->where('mission_objective_id', $objectiveResult['mission_objective_id'])
                ->firstOrFail();

            $dbObjectiveResult->update([
                'result' => $objectiveResult['result']
            ]);
        }
    }
}
