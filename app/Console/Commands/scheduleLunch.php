<?php

namespace App\Console\Commands;

use App\Models\Schedule\Items\ScheduleInfoItem;
use App\Models\Schedule\Items\ScheduleJuryItem;
use App\Models\Schedule\Items\ScheduleTableItem;
use App\Models\Schedule\ScheduleItem;
use Carbon\Carbon;
use Illuminate\Console\Command;

class scheduleLunch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:addLunch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds the lunchbreak to the schedule';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $cutoffTime = Carbon::createFromTime(12, 17)->toTimeString();

        //Only table schedule items
        //$items = ScheduleItem::query()->whereTime('start_time', '>', $cutoffTime)->where("item_type", "App\Models\Schedule\Items\ScheduleTableItem")->get();

        // All items
        $items = ScheduleItem::query()->whereTime('start_time', '>', $cutoffTime)->get();
        $time = 30;

        foreach ($items as $item) {
            $item->start_time = Carbon::parse($item->start_time)->addMinutes($time);
            $item->end_time = Carbon::parse($item->end_time)->addMinutes($time);
            $item->save();
        }

        $this->info('Lunch has been added successfully');
    }
}
