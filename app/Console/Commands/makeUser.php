<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class makeUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user using the CLI of the FLLTools';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $name = $this->ask("What is the users name?");
        $email = $this->ask("What is the users email");
        $password = $this->secret("Please provide a password");
        $role = $this->anticipate('What is users role?', ['admin', 'headReferee', 'headJudge', 'referee', 'judge', 'coach']);

        $user = new \App\Models\User();
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();
        $user->assign($role);
        $user->save();

        $this->info("User created with ID: ".$user->id);

    }
}
