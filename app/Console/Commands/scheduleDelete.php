<?php

namespace App\Console\Commands;

use App\Models\Schedule\Items\ScheduleInfoItem;
use App\Models\Schedule\Items\ScheduleJuryItem;
use App\Models\Schedule\Items\ScheduleTableItem;
use App\Models\Schedule\ScheduleItem;
use Illuminate\Console\Command;

class scheduleDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete the existing schedule';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        ScheduleJuryItem::query()->delete();
        ScheduleItem::query()->delete();
        ScheduleTableItem::query()->delete();
        ScheduleInfoItem::query()->delete();

        $this->info('Schedule deleted successfully');
    }
}
