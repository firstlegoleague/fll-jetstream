<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class makeUserNoInteract extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:userNI {name} {email} {password} {--admin} {--referee} {--judge} {--head} {--coach}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user using the CLI of the FLLTools';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $name = $this->argument("name");
        $email = $this->argument("email");
        $password = $this->argument("password");
        $isAdmin = $this->option("admin");
        $isReferee = $this->option("referee");
        $isJudge = $this->option("judge");
        $isHead = $this->option("head");
        $isCoach = $this->option("coach");

        $user = new \App\Models\User();
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();

        if($isAdmin){
            $user->assign("admin");
        }
        if($isReferee){
            if($isHead)
                $user->assign("headReferee");
            else
                $user->assign("referee");
        }
        if($isJudge){
            if($isHead)
                $user->assign("headJudge");
            else
                $user->assign("referee");
        }
        if($isCoach){
            $user->assign("coach");
        }

        $user->save();

    }
}
