<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

//Route::get('/scoreboard', function () {
//    return Inertia::render('scoreboard/index');
//})->name('scoreboard.index');

Route::controller(\App\Http\Controllers\TimerController::class)->group(function () {
    Route::get('/start', 'api_start')->name('timer.api.start');
    Route::get("/pause", "api_reset")->name("timer.api.pause");
    Route::get('/end', 'api_end')->name('timer.api.end');
    Route::get('/toggle', 'api_toggle')->name('timer.api.toggle');

});
