<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

// GET /api/v1/{api_token}/teams
// DESC: Get a list of all the teams.
Route::get('/', function () {
    return \App\Models\Tournament\Team::all();
});

// POST /api/v1/{api_token}/teams
// Create a new team
Route::post('/', [\App\Http\Controllers\api\TeamsController::class, 'createTeam']);


Route::put("/{team_uuid}", [\App\Http\Controllers\api\TeamsController::class, 'editTeam']);


Route::delete("/{team_uuid}", [\App\Http\Controllers\api\TeamsController::class, 'deleteTeam']);
