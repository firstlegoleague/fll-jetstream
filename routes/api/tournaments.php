<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

// GET /api/v1/{api_token}/tournaments
// DESC: Get a list of all the tournaments.
Route::get('/', function () {
    return \App\Models\Tournament\Tournament::all();
});

Route::get("/{tournament_uuid}", [\App\Http\Controllers\api\TournamentsController::class, 'getTournament']);


// POST /api/v1/{api_token}/tournaments
// Create a new tournament
Route::post('/', [\App\Http\Controllers\api\TournamentsController::class, 'createTournament']);


Route::put("/{tournament_uuid}", [\App\Http\Controllers\api\TournamentsController::class, 'editTournament']);


Route::delete("/{tournament_uuid}", [\App\Http\Controllers\api\TournamentsController::class, 'deleteTournament']);
