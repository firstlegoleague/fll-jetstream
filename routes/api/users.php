<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

// GET /api/v1/{api_token}/users
// DESC: Get a list of all the teams.
Route::get('/', [\App\Http\Controllers\api\UsersController::class, 'getAllUsers']);
Route::get('/{user}', [\App\Http\Controllers\api\UsersController::class, 'getUser']);
