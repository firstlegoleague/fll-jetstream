<?php

use App\Http\Controllers\NotificationManagerController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/home')->group(__DIR__.'/web/public/homepage.php');
Route::prefix('/scoreboard')->group(__DIR__ . '/web/public/scoreboard.php');
Route::prefix('/team')->group(__DIR__.'/web/public/team.php');
Route::prefix('/timer')->group(__DIR__.'/web/public/timer.php');
Route::prefix('/livestream')->group(__DIR__.'/web/public/livestream.php');

Route::get('/', function () {
    return to_route('public.homepage.index');
});

Route::get('/loginold', function () {
    return Inertia::render('Auth/LoginOld', [ ]);
});

route::get("/healthcheck",function(){
    return view("healthcheck");
});

Route::get("/login/sso/authentik", [\App\Http\Controllers\authentikController::class, 'login']);
Route::get("/login/sso/authentik/callback", [\App\Http\Controllers\authentikController::class, 'callback']);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {

        $teamCount = \App\Models\Tournament\Team::query()->count();
        $resultCount = \App\Models\Challenge\RefereeResult::query()->count();
        $resultLatest = \App\Models\Challenge\RefereeResult::query()
            ->with("team", "round", "table")
            ->latest()
            ->first();
        $resultHighest = \App\Models\Challenge\RefereeResult::query()
            ->orderBy("score", "desc")
            ->with("team")
            ->first();

        return Inertia::render('Dashboard', compact("teamCount", "resultCount", "resultLatest", "resultHighest"));
    })->name('dashboard');

    Route::prefix('/judge')->group(__DIR__.'/web/judge.php');

    Route::prefix('/admin/settings')->group(__DIR__.'/web/admin/settings.php');
    Route::prefix('/admin/teams')->group(__DIR__.'/web/admin/teams.php');
    Route::prefix('/admin/rounds')->group(__DIR__.'/web/admin/rounds.php');
    Route::prefix('/admin/tables')->group(__DIR__.'/web/admin/tables.php');
    Route::prefix('/admin/referee_results')->group(__DIR__.'/web/admin/referee_results.php');
    Route::prefix('/admin/users')->group(__DIR__.'/web/admin/users.php');
    Route::prefix('/admin/remarks')->group(__DIR__.'/web/admin/remarks.php');
    Route::prefix('/admin/exports')->group(__DIR__.'/web/admin/exports.php');
    Route::prefix('/admin/judges')->group(__DIR__.'/web/admin/judges.php');
    Route::prefix('/admin/scoreboard')->group(__DIR__.'/web/admin/scoreboard.php');
    Route::prefix('/admin/timer_control')->group(__DIR__.'/web/admin/timer.php');

//  Change to /settings/tournaments?
    Route::prefix('/admin/tournaments')->group(__DIR__.'/web/admin/tournament.php');
    Route::prefix('/schedule')->group(__DIR__.'/web/schedule.php');
    Route::prefix('/error')->group(__DIR__.'/web/error.php');
    Route::prefix('/frontend_api')->group(__DIR__.'/web/frontend_api/frontend_api.php');
    Route::prefix('/wizard')->group(__DIR__.'/web/wizard.php');
});


Route::post('/notifications/subscribe', [NotificationManagerController::class, 'subscribe'])->name('push.subscribe');


