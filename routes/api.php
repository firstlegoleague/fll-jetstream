<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix("v1/{api_token}")
    ->middleware('api_token')
    ->group(function () {
    Route::prefix('/teams')->group(__DIR__.'/api/teams.php');
    Route::prefix('/users')->group(__DIR__.'/api/users.php');
    Route::prefix('/tournaments')->group(__DIR__.'/api/tournaments.php');
    Route::prefix('/timer')->group(__DIR__.'/api/timer.php');
});

// GET api/v1/teapot
// DESC: A little easter egg.
Route::get("/v1/teapot", [\App\Http\Controllers\api\TeapotController::class, 'teapot']);
