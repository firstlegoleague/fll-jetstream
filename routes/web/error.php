<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/404', function () {
    return Inertia::render('Errors/404');
});

Route::get('/401', function () {
    return Inertia::render('Errors/401');
});

Route::get('/403', function () {
    return Inertia::render('Errors/403');
});

Route::get('/500', function () {
    return Inertia::render('Errors/500');
});
