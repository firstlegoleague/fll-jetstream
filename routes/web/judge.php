<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/judge/robot', function () {
    return Inertia::render('judge/robot/index');
})->name('judge.robot.index');

