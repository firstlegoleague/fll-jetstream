<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

//Route::get('/dev', function () {
//    return Inertia::render('Public/Homepage/index');
//})->name('public.homepage.index');

//Route::get('/{id}', function () {
//    return  Inertia::render('Public/Team/index');
//})->name('public.team.index');

Route::controller(\App\Http\Controllers\PublicTeamController::class)->group(function () {

    Route::get('/', function () {
        return Inertia::render('Public/Teamlist/index', [
            'teams' => \App\Models\Tournament\Team::all(),
        ]);
    })->name('public.teamlist.index');

    Route::get('/{id}', 'show')->name('public.team.show');

});

Route::controller(\App\Http\Controllers\RefereeResultController::class)->group(function () {
    Route::get('/results/{uuid}', 'show')->name('public.referee_results.show');
});


