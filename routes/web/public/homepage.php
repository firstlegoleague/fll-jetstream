<?php

use App\Models\Table;
use App\Models\Tournament\Jury;
use App\Models\Tournament\Team;
use Carbon\Carbon;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;



Route::get('/', function () {
    return to_route('scoreboard.index');
})->name('public.homepage.index');

Route::get('/schedule', function () {

    $date = Carbon::now()->format('Y-m-d');
    $resources = [];
    $events = [];
    $allResourceIds = [];
    $tableResourceIds = [];
    $juryResourceIds = [];

    foreach (\App\Models\Table::all()->sortBy("number") as $table){
        $slice = null;
        $slice["title"] = "Tafel " . $table->number;
        $slice["id"] = "table" . str_pad($table->number, 2, "0", STR_PAD_LEFT);
        $slice["groupId"] = 1;
        $slice["key"] = $table->number;
        array_push($resources, $slice);
        array_push($allResourceIds, "table0".$table->number);
        array_push($tableResourceIds, "table0".$table->number);
    }

    foreach (\App\Models\Tournament\Jury::all()->sortBy("number") as $jury){
        $slice = null;
        $slice["title"]=  $jury->name;
        $slice["groupId"] = 2;
        $slice["id"] =  "judge".$jury->number;
        array_push($allResourceIds, "judge".$jury->number);
        array_push($juryResourceIds, "judge".$jury->number);
        array_push($resources, $slice);
    }

    foreach (\App\Models\Schedule\ScheduleItem::all() as $s_item ){
        $slice = null;

        switch ($s_item->item_type){
            case "App\Models\Schedule\Items\ScheduleInfoItem":
                $info_item = \App\Models\Schedule\Items\ScheduleInfoItem::all()->where("uuid", $s_item->item_id)->first();

                $slice["title"] = $info_item->text;
                $slice["groupId"] = $info_item->uuid;
                $slice["start"] = $date . "T" . $s_item->start_time;
                $slice["end"] = $date . "T" . $s_item->end_time;

                if($info_item->category == "ALL") {
                    $slice["resourceIds"] = $allResourceIds;
                } else if($info_item->category == "JURY"){
                    $slice["resourceIds"] = $juryResourceIds;
                } else if($info_item->category == "MATCH"){
                    $slice["resourceIds"] = $tableResourceIds;
                }
                break;

            case "App\Models\Schedule\Items\ScheduleJuryItem":
                $info_item = \App\Models\Schedule\Items\ScheduleJuryItem::all()->where("uuid", $s_item->item_id)->first();
                $slice["title"] = \App\Models\Tournament\Team::all()->where("uuid", $info_item->team_uuid)->first()->name . " | " . \App\Models\Tournament\Jury::all()->where("uuid", $info_item->jury_uuid)->first()->name;
                $slice["description"] = $slice["title"];
                $slice["groupId"] = $info_item->uuid;
                $slice["start"] = $date . "T" . $s_item->start_time;
                $slice["end"] = $date . "T" . $s_item->end_time;
                $slice["resourceId"] = "judge".Jury::where("uuid", $info_item->jury_uuid)->first()->number;
                break;

            case "App\Models\Schedule\Items\ScheduleTableItem":
                $info_item = \App\Models\Schedule\Items\ScheduleTableItem::all()->where("uuid", $s_item->item_id)->first();
                $slice["title"] = \App\Models\Tournament\Team::all()->where("uuid", $info_item->team_uuid)->first()->id_name . " | " . \App\Models\Table::all()->where("uuid", $info_item->table_uuid)->first()->name;
                $slice["groupId"] = $info_item->uuid;
                $slice["description"] = $slice["title"];
                $slice["start"] = $date . "T" . $s_item->start_time;
                $slice["end"] = $date . "T" . $s_item->end_time;
                $slice["resourceId"] = "table".str_pad(Table::where("uuid",$info_item->table_uuid)->first()->number, 2, "0", STR_PAD_LEFT); //"table".Table::where("uuid",$info_item->table_uuid)->first()->number;
                break;
        }

        array_push($events, $slice);
    }

//        dd($events, $resources);

    return Inertia::render('Public/schedule/index', [
        'resources' => $resources,
        'events' => $events,
        'teams' => \App\Models\Tournament\Team::all(),
        'juries' => \App\Models\Tournament\Jury::all(),
        'tables' => \App\Models\Table::all(),
        'rounds' => \App\Models\Tournament\Round::all(),
    ]);

//    return Inertia::render('Public/Schema/index', [
//        "schedule" => $schedule,
//    ]);

})->name('public.schedule.index');
