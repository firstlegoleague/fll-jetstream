<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return Inertia::render('Public/Timer');
})->name('public.timer');


Route::controller(\App\Http\Controllers\TimerController::class)->group(function () {
    Route::get('/S7mackyC4m4Jfk/button', 'button_web');
});
