<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

//Route::get('/scoreboard', function () {
//    return Inertia::render('scoreboard/index');
//})->name('scoreboard.index');

Route::controller(\App\Http\Controllers\ScoreboardController::class)->group(function () {
    Route::get('/', 'index')->name('scoreboard.index');
});
