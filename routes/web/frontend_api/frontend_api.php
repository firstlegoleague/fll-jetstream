<?php

use Illuminate\Support\Facades\Route;

Route::post('/locale', [\App\Http\Controllers\api\LocaleController::class, 'setLocale'])->name('frontend_api.locale');

Route::get('/rounds/team/{uuid}', [\App\Http\Controllers\RoundController::class, 'getRoundForTeam'])->name('frontend_api.get_rounds_for_team');
