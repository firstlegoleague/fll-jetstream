<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\RoundController::class)->group(function () {
    Route::get('/', 'index')->name('rounds.index');
    Route::get('/{uuid}', 'show')->name('rounds.show');
    Route::put("/{uuid}", 'update')->name('rounds.update');
    Route::post('/', 'store')->name('rounds.store');
    Route::delete('/{uuid}', 'destroy')->name('rounds.destroy');
});

