<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\JuryController::class)->group(function () {
    Route::get('/', 'index')->name('jury.index');
//    Route::get('/{uuid}', 'show')->name('teams.show');
    Route::post('/{uuid}', 'update')->name('jury.update');
    Route::post('/', 'store')->name('jury.store');
    Route::delete('/{uuid}', 'delete')->name('jury.destroy');

});
