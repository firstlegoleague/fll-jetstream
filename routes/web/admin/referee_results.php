<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\RefereeResultController::class)->group(function () {
    Route::get('/', 'index')->name('referee_results.index');
    Route::get('/create', 'create')->name('referee_results.create');
    Route::post('/', 'store')->name('referee_results.store');
    Route::get('/{uuid}', 'show')->name('referee_results.show');
    Route::get('/{uuid}/edit', 'edit')->name('referee_results.edit');
    Route::put('/{uuid}', 'update')->name('referee_results.update');
    Route::patch('/{uuid}', 'update');
    Route::delete('/{uuid}', 'destroy')->name('referee_results.destroy');
    Route::get('/export', 'export')->name('referee_results.export');
});
