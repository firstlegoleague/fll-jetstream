<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

//Route::get('/scoreboard', function () {
//    return Inertia::render('scoreboard/index');
//})->name('scoreboard.index');

Route::controller(\App\Http\Controllers\TimerController::class)->group(function () {
    Route::get('/start', 'timer_start')->name('timer.start');
    Route::get('/pause', 'timer_pause')->name('timer.pause');
    Route::get("/end", "timer_end")->name("timer.end");
});
