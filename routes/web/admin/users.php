<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\UserController::class)->group(function () {
    Route::get('/', 'index')->name('users.index');
    Route::get('/{uuid}', 'show')->name('users.show');
    Route::put("/{uuid}", 'update')->name('users.update');
    Route::post('/', 'store')->name('users.store');
    Route::delete('/{uuid}', 'destroy')->name('users.destroy');
});

