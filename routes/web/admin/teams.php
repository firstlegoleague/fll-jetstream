<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\TeamsController::class)->group(function () {
    Route::post('/uploadList', 'uploadList')->name('teams.uploadList');
    Route::get('/', 'index')->name('teams.index');
    Route::get('/{uuid}', 'show')->name('teams.show');
    Route::post('/{uuid}', 'update')->name('teams.update');
    Route::post('/', 'store')->name('teams.store');
    Route::delete('/{uuid}', 'destroy')->name('teams.destroy');

});

