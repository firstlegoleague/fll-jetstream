<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\RemarkController::class)->group(function () {
    Route::get('/', 'index')->name('remarks.index');
    Route::get('/new', 'new')->name('remarks.new');
    Route::get('/{uuid}', 'show')->name('remarks.show');
    Route::put("/{uuid}", 'update')->name('remarks.update');
    Route::post('/', 'store')->name('remarks.store');
    Route::delete('/{uuid}', 'destroy')->name('remarks.destroy');
});

