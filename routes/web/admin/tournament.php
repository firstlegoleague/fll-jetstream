<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\TournamentsController::class)->group(function () {
    Route::get('/', 'index')->name('tournaments.index');
    Route::post('/', 'store')->name('tournaments.store');
    Route::post('/{uuid}', 'update')->name('tournaments.update');
    Route::delete('/{uuid}', 'destroy')->name('tournaments.destroy');
});
