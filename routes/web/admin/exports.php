<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

//Route::get('/scoreboard', function () {
//    return Inertia::render('scoreboard/index');
//})->name('scoreboard.index');

Route::controller(\App\Http\Controllers\ExportController::class)->group(function () {
    Route::get('/', 'index')->name('export.index');
    Route::get('/judgelist', 'judgelist')->name('export.judgelist');
    Route::get('/matchlist', 'matchlist')->name('export.matchlist');
    Route::get('/tijn', 'matchlistreferee')->name('export.matchlistreferee');
    Route::get('/generate', 'generate')->name('export.generate');
    Route::get('/temp', 'refereeResultExport')->name('export.ojs_temp');
    Route::get("/refereeResults", "refereeResultExport")->name("export.referee_results");
});

Route::controller(\App\Http\Controllers\RubricsController::class)->group(function () {
    Route::get('/rubricDownload/{uuid}', 'index')->name("export.rubric");
});
