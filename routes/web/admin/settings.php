<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


Route::get('/', function () {
return Inertia::render('Admin/Settings/index');
})->name('settings.index');


Route::put("/store",
    [\App\Http\Controllers\SettingController::class, 'store'])
    ->name("setting.store");
