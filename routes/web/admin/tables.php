<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\TableController::class)->group(function () {
    Route::get('/', 'index')->name('tables.index');
    Route::get('/{uuid}', 'show')->name('tables.show');
    Route::put("/{uuid}", 'update')->name('tables.update');
    Route::post('/', 'store')->name('tables.store');
    Route::delete('/{uuid}', 'destroy')->name('tables.destroy');
});

