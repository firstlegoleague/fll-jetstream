<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::controller(\App\Http\Controllers\ScheduleController::class)->group(function () {
    Route::get('/index', 'index')->name('schedule.index');
    Route::get('/headReferee', 'headReferee')->name('schedule.headReferee');
    Route::post('/store', "store")->name('schedule.store');
});
