<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return Inertia::render('Wizard/Index',[
        'scoreSheets'=> \App\Models\Challenge\ScoreSheet::all()
    ]);
})->name('wizard.index');

Route::post('/tournament', ['\App\Http\Controllers\WizardController','storeTournament'])->name('wizard.storeTournament');
